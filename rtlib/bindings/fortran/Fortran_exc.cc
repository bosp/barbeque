#include "Fortran_exc.h"

#include <cstdio>
#include <bbque/utils/utility.h>

FortranWrapper::FortranWrapper(std::string const & name,
		std::string const & recipe,
		RTLIB_Services_t *rtlib) :
	BbqueEXC(name, recipe, rtlib) {
	logger->Debug("New FortranWrapper::FortranWrapper()");
}

RTLIB_ExitCode_t FortranWrapper::onSetup() {
	logger->Debug("FortranWrapper::onSetup()");
    return (RTLIB_ExitCode_t) Fortran_OnSetup();
}

RTLIB_ExitCode_t FortranWrapper::onConfigure(int8_t awm_id) {
	logger->Debug("FortranWrapper::onConfigure(): EXC [%s] => AWM [%02d]",
		exc_name.c_str(), awm_id);
	return (RTLIB_ExitCode_t) Fortran_OnConfigure();
}

RTLIB_ExitCode_t FortranWrapper::onRun() {
	RTLIB_WorkingModeParams_t const wmp = WorkingModeParams();
	logger->Debug("FortranWrapper::onRun()      : EXC [%s]  @ AWM [%02d]",
		exc_name.c_str(), wmp.awm_id);
	return (RTLIB_ExitCode_t) Fortran_OnRun();
}

RTLIB_ExitCode_t FortranWrapper::onMonitor() {
	RTLIB_WorkingModeParams_t const wmp = WorkingModeParams();
	logger->Debug("FortranWrapper::onMonitor()  : EXC [%s]  @ AWM [%02d] "
			"=> cycles [%d], CPS = %.2f",
		exc_name.c_str(), wmp.awm_id, Cycles(), GetCPS());
	return (RTLIB_ExitCode_t) Fortran_OnMonitor();
}

RTLIB_ExitCode_t FortranWrapper::onSuspend() {
	logger->Debug("FortranWrapper::onSuspend()  : suspension...");
	return RTLIB_OK;
}

RTLIB_ExitCode_t FortranWrapper::onRelease() {
	logger->Debug("FortranWrapper::onRelease()  : exit");
	return (RTLIB_ExitCode_t) Fortran_OnRelease();
}
