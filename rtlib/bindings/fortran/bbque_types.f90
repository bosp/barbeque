! Initialization
module RTLIB_ExitCode !For more information see barbeque/include/bbque/rtlib.h
    integer, parameter :: RTLIB_OK = 0
    integer, parameter :: RTLIB_ERROR = 1
    integer, parameter :: RTLIB_VERSION_MISMATCH = 2
    integer, parameter :: RTLIB_NO_WORKING_MODE = 3
    integer, parameter :: RTLIB_BBQUE_CHANNEL_SETUP_FAILED = 4
    integer, parameter :: RTLIB_BBQUE_CHANNEL_TEARDOWN_FAILED = 5
    integer, parameter :: RTLIB_BBQUE_CHANNEL_WRITE_FAILED = 6
    integer, parameter :: RTLIB_BBQUE_CHANNEL_READ_FAILED = 7
    integer, parameter :: RTLIB_BBQUE_CHANNEL_READ_TIMEOUT = 8
    integer, parameter :: RTLIB_BBQUE_CHANNEL_PROTOCOL_MISMATCH = 9
    integer, parameter :: RTLIB_BBQUE_CHANNEL_UNAVAILABLE = 10
    integer, parameter :: RTLIB_BBQUE_CHANNEL_TIMEOUT = 11
    integer, parameter :: RTLIB_BBQUE_UNREACHABLE = 12
    integer, parameter :: RTLIB_EXC_DUPLICATE = 13
    integer, parameter :: RTLIB_EXC_NOT_REGISTERED = 14
    integer, parameter :: RTLIB_EXC_REGISTRATION_FAILED = 15
    integer, parameter :: RTLIB_EXC_MISSING_RECIPE = 16
    integer, parameter :: RTLIB_EXC_UNREGISTRATION_FAILED = 17
    integer, parameter :: RTLIB_EXC_NOT_STARTED = 18
    integer, parameter :: RTLIB_EXC_ENABLE_FAILED = 19
    integer, parameter :: RTLIB_EXC_NOT_ENABLED = 20
    integer, parameter :: RTLIB_EXC_DISABLE_FAILED = 21
    integer, parameter :: RTLIB_EXC_GWM_FAILED = 22
    integer, parameter :: RTLIB_EXC_GWM_START = 23
    integer, parameter :: RTLIB_EXC_GWM_RECONF = 24
    integer, parameter :: RTLIB_EXC_GWM_MIGREC = 25
    integer, parameter :: RTLIB_EXC_GWM_MIGRATE = 26
    integer, parameter :: RTLIB_EXC_GWM_BLOCKED = 27
    integer, parameter :: RTLIB_EXC_SYNC_MODE = 28
    integer, parameter :: RTLIB_EXC_SYNCP_FAILED = 29
    integer, parameter :: RTLIB_EXC_WORKLOAD_NONE = 30
    integer, parameter :: RTLIB_EXC_CGROUP_NONE = 31
    integer, parameter :: RTLIB_EXIT_CODE_COUNT = 32
end module RTLIB_ExitCode

module RTLIB_ResourceType !For more information see barbeque/include/bbque/rtlib.h
    integer, parameter :: SYSTEM = 0
    integer, parameter :: CPU = 1
    integer, parameter :: PROC_NR = 2
    integer, parameter :: PROC_ELEMENT = 3
    integer, parameter :: MEMORY = 4
    integer, parameter :: GPU = 5
    integer, parameter :: ACCELERATOR = 6
end module RTLIB_ResourceType
