#include <cstdio>
#include <iostream>
#include <random>
#include <cstring>
#include <memory>

#include <libgen.h>

#include <boost/program_options/options_description.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/variables_map.hpp>

#include "Fortran_exc.h"
#include <bbque/utils/utility.h>
#include <bbque/utils/logging/logger.h>

// Setup logging
#undef  BBQUE_LOG_MODULE
#define BBQUE_LOG_MODULE "FortranWrapper"

namespace bu = bbque::utils;
namespace po = boost::program_options;

std::unique_ptr<bu::Logger> logger;

typedef std::shared_ptr<BbqueEXC> pBbqueEXC_t;
pBbqueEXC_t pexc;

RTLIB_Services_t *rtlib;

extern "C" int bbqfortranmain_(char *recipe_name, unsigned int len) {

	logger = bu::Logger::GetLogger("fortran");

	logger->Debug("STEP 0. Initializing RTLib, application...");

	if ( RTLIB_Init("FortranWrapper", &rtlib) != RTLIB_OK) {
		logger->Fatal("Unable to init RTLib (Did you start the BarbequeRTRM daemon?)");
		return RTLIB_ERROR;
	}

	assert(rtlib);
	std::string recipe;
	recipe.assign(recipe_name, len);

	logger->Debug("STEP 1. Registering EXC with recipe <%s>...", recipe_name);
	pexc = std::make_shared<FortranWrapper>("FortranWrapper", recipe, rtlib);
	if (!pexc->isRegistered()) {
		logger->Fatal("Registering failure.");
		return RTLIB_ERROR;
	}


	logger->Debug("STEP 2. Starting EXC control thread...");
	pexc->Start();


	logger->Debug("STEP 3. Waiting for EXC completion...");
	pexc->WaitCompletion();


	logger->Debug("STEP 4. Disabling EXC...");
	pexc = NULL;

	logger->Debug("===== FortranWrapper DONE! =====");
	return EXIT_SUCCESS;

}

extern "C" int bbqgetassignedresources_(int *r_type, int *r_amount) {
	return (int) pexc->GetAssignedResources( (RTLIB_ResourceType_t)*r_type, *r_amount);
}

extern "C" int bbqcycles_() {
	return (int) pexc->Cycles();
}

extern "C" float bbqgetcps_() {
	return (float) pexc->GetCPS();
}


