import argparse
import matplotlib.pyplot as plt
import numpy as np
import os
import re
import pandas as pd
import sys
from typing import List

def gen_table(app_name : str,data : pd.DataFrame, col_width : float = 3.0, row_height : float = 0.625, font_size : int = 14,
              header_color : str='#40466e', runs_color : str='#eff0f6', max_color : str ='#bfc2d9',
              row_colors : List[str]=['#f1f1f2', 'w'], edge_color : str='w')-> plt.Figure:
    """It plots pandas dataframe generating a table

    Args:
        data (pd.DataFrame): Data to be formatted
        col_width (float, optional): Column width. Defaults to 3.0.
        row_height (float, optional): Row height. Defaults to 0.625.
        font_size (int, optional): Font size. Defaults to 14.
        header_color (str, optional): Color for header background. Defaults to '#40466e'.
        runs_color (str, optional): Color for 'runs' column background. Defaults to '#eff0f6'.
        max_color (List[str], optional): Color for max runs 
        row background. Defaults to #bfc2d9.
        row_colors (List[str], optional): Color for rows background. Defaults to ['#f1f1f2', 'w'].
        edge_color (str, optional): Table edges color. Defaults to 'w'.

    Returns:
        plt.Figure: Figure containing a table representation of the dataframe
    """
    size = (np.array(data.shape[::-1]) + np.array([0, 1])) * np.array([col_width, row_height])
    data.style.format({'mean_epc[J]': '{:.2e}'})
    print(data)
    fig, ax = plt.subplots(figsize=size)
    fig.suptitle(f'{app_name} AWMs composition', size=30)
    ax.axis('off')
    mpl_table = ax.table(cellText=data.values, colLoc='center', rowLoc='center', cellLoc='center', bbox=[0, 0, 1, 1], colLabels=data.columns)
    mpl_table.auto_set_font_size(False)
    mpl_table.set_fontsize(font_size)

    max_idx=data['runs'].idxmax()+1
    last_column= data.shape[1]-1
    for k, cell in mpl_table._cells.items():
        cell.set_edgecolor(edge_color)
        if k[0] == 0:
            cell.set_text_props(weight='bold', color='w')
            cell.set_facecolor(header_color)
        elif k[1] == last_column-2:
                cell.set_facecolor(runs_color)
        elif k[1] >= last_column-1:
                cell.set_facecolor(runs_color)
                #workaround to force scientific notation on mean_epc 
                cell._text.set_text("%.2e" % float(cell._text.get_text()))
        else:
            cell.set_facecolor(row_colors[k[0]%len(row_colors) ])

        if k[0] == max_idx:
                cell.set_facecolor(max_color)
                cell.set_text_props(weight='bold')
    return fig

def parse_data(log_path : str,out_dir : str) -> None:
    """It parses the log file to extract information about awms used by enero policy.
    Results are then organized in a csv file and a table image is generated

    Args:
        log_path (str): Log file path
        out_dir (str): Output directory path
    """
    print(f'Input log:\t{log_path}')
    print(f'Output dir:\t{out_dir}')

    apps={}
    with open(log_path,'r') as f:
        for line in f.readlines():
            if ('ScheduleRequest' in line) and ('AWM' in line):
                #extract awm_id from log line 
                print(line)
                awm_id=int(re.search("([0-9]*):run", line).group(1))
                #extract app_name
                app_name=re.search("\[([0-9]*:[A-Z]*[0-9]*)\]", line).groups()[0]
                #print(f'APP:{app_name} {app_name in apps} {apps}')
                #extract resource id and quota from log line 
                awm_res=re.search("<(.*):([0-9]*)>",line)
                
                if not(app_name in apps):
                    apps[app_name]=[]
            
                if awm_res:
                    name,quota=awm_res.groups()
                    #print(f"AWM {awm_id} RES {name} QUOTA {quota}")
                    apps[app_name][awm_id][name]=quota
                else:
                    if awm_id >= len(apps[app_name]):
                        print(f"Adding AWM {awm_id}")
                        apps[app_name].append({'awm_id':awm_id,'runs':0})
                    apps[app_name][awm_id]['runs']=apps[app_name][awm_id]['runs']+1

    
    epc_df=pd.read_csv(f'{out_dir}/data_epc.txt',sep=' ',
                                usecols=[2, 4, 5,6],
                                names=['app','awm_id', 'mean_epc', 'mean_epc2' ],
                                index_col='awm_id').groupby(['app','awm_id']).mean().reset_index()
    

   # print(epc_df.groups())
    
    for (app_name,group) in epc_df.groupby('app'):
        
        
        awms=apps[app_name]
        #convert replace NaN with 0
        awm_df= pd.DataFrame(awms).fillna(0)
        pd.set_option('display.precision', 2)
        awm_df= pd.merge(awm_df,group,on='awm_id')
        
        
        #change columns order, 'runs' and 'mean_epc' in last position, remove app_name
        cols=awm_df.columns.tolist()
        awm_df=awm_df[[cols[0]]+cols[2:-3]+[cols[1]]+cols[-2:]]
        
        
        
        
        #using log name to have unique name for csv file
        log_name=os.path.basename(log_path).split('.')[0]
        
        gen_table(app_name,awm_df).savefig(os.path.join(out_dir,app_name+"-"+log_name+".png"))
        print (f'PNG image saved:{os.path.join(out_dir,log_name+".png")}')

        awm_df.set_index('awm_id',inplace=True)
        awm_df.to_csv(os.path.join(out_dir,app_name+"-"+log_name+".csv"))
        print (f'CSV file saved:{os.path.join(out_dir,app_name+"-"+log_name+".csv")}')
    
    
    
    

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input-log', help='The bbque.log ',
                        required=True)
    parser.add_argument('-o', '--output-directory', help='The output directory containing the csv file',
                        required=True)
    try:
        args = parser.parse_args()
    except argparse.ArgumentError:
        print('Error in the provided arguments')
        parser.print_help()
        sys.exit(1)
    arguments=vars(args)
    parse_data(arguments['input_log'],arguments['output_directory'])
