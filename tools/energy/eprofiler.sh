#!/bin/bash

logfile=$1
outdir=${logfile%.log}
file_energy=$outdir/data_energy.txt
file_epc=$outdir/data_epc.txt

echo "Input log file   : " $logfile
echo "Output directory : " $outdir
mkdir $outdir

if [ $# -eq 0 ]; then
	echo "$0 <bbque.log>"
	exit 1
fi

cat $logfile | grep StopResourceConsumption \
	| grep sys | sed s/value=//g | sed s/\<//g | sed s/\>//g \
	> $file_energy

cat $logfile | grep EpC | awk '{ print $1" "$2" "$8" "$11" "$14" "$15 }' \
	| sed s/'={id='/' '/g | sed s/', '/' '/g | sed s/'EpC='//g | sed s/'EpC2='//g \
	> $file_epc

python3 eplot.py -i $file_energy -a $file_epc -o $outdir
python3 awm_table.py -i $logfile -o $outdir
