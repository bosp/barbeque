import argparse
import datetime as dt
import os
from random import randint
import re
import sys


import matplotlib
import matplotlib.dates as mdates
import matplotlib.pyplot as plt


#Tkagg backend prevent matplotlib to steal window focus 
matplotlib.use('Tkagg')
res_set=set()
colors = []
for i in range(50):
    colors.append('#%06X' % randint(0, 0xFFFFFF))


class AppLogPlot:
    """
    AppLogPlot class offers a wrapper to plots and epc/awm data
    """
    def __init__(self,app_name : str) -> None:
        self.app_name=app_name
        self.__time_points=[]
        self.__epc_points=[]
        self.__epc2_points=[]
        self.__awm_ids=[]
        self.__epc_ax=None
        self.__awm_ax=None
        self.__awm_composition={}
        self.__ymax=800
        
        
    def set_ax(self,ax : matplotlib.axes.SubplotBase) -> None:
        self.__epc_ax,self.__awm_ax=ax
        
        
        
    def add_point(self, timestamp : str,awm_id : int,epc : int,epc2 : int) -> None:
        self.__time_points.append(dt.datetime.strptime(timestamp, '%H:%M:%S'))
        self.__awm_ids.append(awm_id)
        self.__epc_points.append(epc)
        self.__epc2_points.append(epc2)
        
    def add_awm(self,awm_id : int, res : re.Match) -> None:
        if not (awm_id in self.__awm_composition):
            self.__awm_composition[awm_id]={}
        
        if res:
            pe,quota=res.groups()
            quota=int(quota)
            if 'gpu' in pe:
                quota=100 if quota else 0
            self.__awm_composition[awm_id][pe]=quota
            
        
            
    def already_run(self):
        return len(self.__time_points)
        
    def plot(self) -> None:
        self.__epc_ax.clear()
        self.__awm_ax.clear()
        
    
        time_formatter = mdates.DateFormatter('%H:%M:%S')
        self.__epc_ax.xaxis.set_major_formatter(time_formatter)
        self.__epc_ax.plot(self.__time_points,self.__epc_points,
                    color='black',
                    marker='o',
                    linewidth=2,
                    label='EpC')
        self.__epc_ax.plot(self.__time_points,self.__epc2_points,
                    color='red',
                    marker='d',
                    linewidth=2,
                    label='EpC2')
        self.__epc_ax.legend()
        
        
        #plt.gcf().autofmt_xdate()
        self.__epc_ax.set_title(f'{self.app_name}',size=20)
        self.__epc_ax.set_ylabel('Energy [uJ]',size=20)
        
        
        for i, id in enumerate(self.__awm_ids):
            self.__epc_ax.annotate(id, (self.__time_points[i], self.__epc_points[i]))
            self.__epc_ax.annotate(id, (self.__time_points[i], self.__epc2_points[i]))
        
        self.__epc_ax.tick_params(axis='x', which='both', labelsize=14, labelbottom=True, labelrotation=45)
        
        
        last_awm=self.__awm_ids[-1]
        
        self.__awm_ax.set_title(f'AWM {last_awm} composition',size=20)
        self.__awm_ax.set_ylabel('Resource quota',size=20)
           
        if last_awm in self.__awm_composition:
            awm=self.__awm_composition[last_awm]
            awm_x=[x.split('.')[1] for x in res_set]
            awm_y=[ awm[x] if x in awm else 0 for x in res_set]
            if any(y > 800 for y in awm_y):
                self.__ymax=max(awm_y)
            
            self.__awm_ax.set_ylim(0,self.__ymax)
            bars=self.__awm_ax.bar(awm_x,awm_y,0.3,label=awm_x, color=colors)
            self.__awm_ax.tick_params(axis='x', which='both', labelsize=14, labelbottom=True, labelrotation=0)
           
            for p in self.__awm_ax.patches:
                self.__awm_ax.annotate(str(p.get_height()), (p.get_x() * 1.005, p.get_height() * 1.005))
    
                
        

def tail(file_path : str) -> str:
    with open(file_path) as f:
        f.seek(0,os.SEEK_END)
        while True:
            line = f.readline()
            if not line:
                # sleep is performed through plt.pause to allow matplotlib GUI refresh when
                # any log lines are available
                plt.pause(0.1)
                continue
            yield line

def RTepcViewer(log_path : str, plot_width : int , plot_height : int) -> None:
    apps={}
    axes=[]
    plt.ion()
    
    
    fig, ax= plt.subplots(nrows=1,ncols=1)
    fig.canvas.set_window_title('Ener0 - EpC Viewer')
    ax.annotate('Waiting for applications',xy=[0.5,0.5],ha='center')
    plt.axis('off')
    plt.show()
    
    plt.style.use("ggplot")
    for log_line in tail(log_path):
            if 'EpC' in log_line:
                timestamp,app_name,awm_id,epc,epc2=re.search('([0-9]+:[0-9]+:[0-9]+).*Init: ([0-9]*:[a-zA-Z]+[0-9]*).*id=([0-9]+).*EpC=([0-9]+) EpC2=([0-9]+)',log_line).groups()
                awm_id=int(awm_id)
                epc=int(epc)
                epc2=int(epc2)
                
                if not(app_name in apps):
                    apps[app_name]=AppLogPlot(app_name)
            
                apps[app_name].add_point(timestamp,awm_id,epc,epc2)
                if len(axes) != len(apps):
                    plt.close()
                    fig, axes = plt.subplots(nrows=len(apps),ncols=2,figsize=(plot_width,len(apps)*plot_height),sharex='col',gridspec_kw={'width_ratios':[2,1]})
                    fig.suptitle('Ener0 - EpC Viewer',size=30)
                    fig.canvas.set_window_title('Ener0 - EpC Viewer')
                    
                    for i,(_,app) in enumerate(apps.items()):
                        app.set_ax(axes[i])
                        
                    for _,app in apps.items():
                        if app.already_run():
                            app.plot()
                    fig.subplots_adjust(hspace=0.5)
                else:
                    apps[app_name].plot()
                    
                    
                #print(f'Timestamp:{timestamp}\tApp:{app_name}\tAWM:{awm_id}\tEPC:{epc}\tEPC2:{epc2}')
                
            elif 'ScheduleRequest' in log_line:
                app_name,awm_id=re.search('([0-9]+:[a-zA-Z]+[0-9]*).*\[ *([0-9]+):run',log_line).groups()
                print(app_name,awm_id)
                res=re.search("<(.*):([0-9]*)>",log_line)
                
                if not(app_name in apps):
                    apps[app_name]=AppLogPlot(app_name)
                    
                apps[app_name].add_awm(int(awm_id),res)
            elif 'UpdateResourcesAvailability' in log_line:
                res_name=re.search('<(.*)>',log_line).groups()[0]
                res_set.add(res_name)

                
                
                
    
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input-log-path', help='The bbque.log path', required=True)
    parser.add_argument('-f', '--fig-sizes', nargs='+', help='Plot sizes, default [15,8]', type=int, default=[15,8],required=False)
    try:
        args = parser.parse_args()
    except argparse.ArgumentError:
        print('Error in the provided arguments')
        parser.print_help()
        sys.exit(1)
    
    arguments = vars(args)
    log_path=arguments['input_log_path']
    plot_width,plot_height=arguments['fig_sizes']
    RTepcViewer(log_path,plot_width,plot_height)
    
