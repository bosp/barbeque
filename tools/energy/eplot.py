#!/usr/bin/python3

import argparse
import sys
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import pandas as pd
from pandas.plotting import register_matplotlib_converters

colors_ = {
    'gpu0': '#00AA00',
    'gpu1': '#009900',
    'gpu2': '#007700',
    'gpu3': '#005500',
    'cpu0': '#CC0000',
    'cpu1': '#CC9900',
    'cpu2': '#AA5500',
    'cpu3': '#994400'
}


def load_energy_info_lines(filename):
    fd = open(filename, "r")
    content = fd.read()
    energy_line = content.split(']')
    for i in range(1, len(energy_line)):
        print("Line: " + energy_line[i])


def load_energy_consumption_data(filename, data_filter):
    """
    Extract the energy consumption data from the log file and build the DataFrame objects
    :param filename: the BarbequeRTRM log file
    :param data_filter:
    :return: the DataFrame object
    """
    energy_logs = pd.read_csv(filename, sep=' ',
                              usecols=[0, 1, 18, 19],
                              names=['date', 'time', 'resource', 'energy'],
                              parse_dates=[[0, 1]],
                              infer_datetime_format=True)
    print(energy_logs)
    if len(data_filter) > 0:
        data_filtered_energy_logs = energy_logs.loc[lambda d: d['date_time'] == data_filter]
        energy_logs = data_filtered_energy_logs
    # return energy_logs

    # Resources counting
    grouped_by_resource = energy_logs.groupby(['resource'], sort=False)
    nr_resources = grouped_by_resource.ngroups
    print(f'Number of resources = {nr_resources}')

    # Per-resource DataFrame(s)
    per_resource_df = pd.DataFrame()
    resource_dfs = []
    resource_heads = ['timestamp']
    for resource_name in grouped_by_resource.head(1)['resource']:
        res_energy = grouped_by_resource['energy'].get_group(resource_name)
        res_times = grouped_by_resource['date_time'].get_group(resource_name)
        per_resource_df = pd.concat([res_times, res_energy], axis=1, sort=False)
        per_resource_df.columns = ['timestamp', 'energy']
        print(f'Per-resource DataFrame [{resource_name}]: {per_resource_df}')
        resource_dfs.append(per_resource_df)
        resource_heads.append(resource_name)

    # Merged DataFrame (resources per column)
    if nr_resources > 1:
        result_df = pd.merge_asof(resource_dfs[0], resource_dfs[1], on='timestamp',
                                  direction='nearest', allow_exact_matches=True)  # tolerance=pd.Timedelta('1s'))
        for i in (range(2, nr_resources)):
            result_df = pd.merge_asof(result_df, resource_dfs[i], on='timestamp',
                                      direction='nearest', allow_exact_matches=True)  # tolerance=pd.Timedelta('1s'))
        result_df.columns = resource_heads
        print(f'Merged DataFrame:\n {result_df}\n')
        return result_df
    else:
        print(f'Final DataFrame:\n {per_resource_df}\n')
        return per_resource_df


def load_epc_data(filename, data_filter):
    epc_logs = pd.read_csv(filename, sep=' ',
                           usecols=[0, 1, 2, 4, 5,6],
                           names=['date', 'time', 'application', 'awm', 'epc','epc2'],
                           parse_dates=[[0, 1]],
                           infer_datetime_format=True)
    print(epc_logs)
    return epc_logs
    # grouped_by_app = epc_logs.groupby(['application'], sort=False)
    # nr_resources = grouped_by_app.ngroups
    # print(f'Number of applications = {nr_resources}')
    #
    # # Per-application DataFrame(s)
    # per_application_df = pd.DataFrame()
    # application_dfs = []
    # application_heads = ['timestamp']
    # for application_name in grouped_by_app.head(1)['application']:
    #     res_epc = grouped_by_app['epc'].get_group(application_name)
    #     res_times = grouped_by_app['date_time'].get_group(application_name)
    #     per_application_df = pd.concat([res_times, res_epc], axis=1, sort=False)
    #     per_application_df.columns = ['date_time', 'epc']
    #     print(f'Per-application DataFrame [{application_name}]: {per_application_df}')
    #     application_dfs.append(per_application_df)
    #     application_heads.append(application_name)


def get_resource_label(resource_path):
    """
    Retrieve the processor type from a resource path string
    :param resource_path: a string like 'sys0.cpu2.pe'
    :return: The resource type in string format, e.g. 'cpu'
    """
    resources = resource_path.split('.')
    for r in resources:
        if r.find('cpu') >= 0 or r.find('gpu') >= 0 or r.find('acc') >= 0:
            return r


def plot_resources_profiling(all_df):
    """
    Generate the output plots
    :param all_df: the DataFrame with the merge of the per-resource energy consumptions
    :return:
    """
    plt.style.use("ggplot")
    register_matplotlib_converters()
    t0 = all_df['timestamp'][0]
    t1 = all_df['timestamp'][1]
    period_len = pd.to_datetime(t1) - pd.to_datetime(t0)
    plot_energy_per_resource(all_df, period_len)
    plot_energy_breakdown(all_df, period_len)


def plot_energy_per_resource(all_df, period_len):
    """
    Plot per-processor energy consumption over the time
    :param all_df: DataFrame with the merge of the per-resource energy consumptions
    :param period_len: Period length of the energy sampling
    :return: None
    """
    fig, ax = plt.subplots()
    for resource_name in all_df.columns[1:]:
        print(f'Processor: {resource_name}')
        rl = get_resource_label(resource_name)
        ax.plot(all_df['timestamp'], all_df[resource_name],
                color=colors_[rl],
                marker='x',
                linewidth=2,
                label=rl)

    plt.gcf().autofmt_xdate()
    time_formatter = mdates.DateFormatter('%H:%M:%S')
    plt.gca().xaxis.set_major_formatter(time_formatter)
    plt.setp(ax.get_xticklabels(), rotation=45)
    plt.ylabel('Energy [J]')
    plt.title(f'Per-processor energy consumption (per processor)\n[execution intervals t={period_len.seconds}s]')
    plt.legend(loc='best')
    #	plt.show()
    outfile = f'{output_directory}/energy_per_resource.png'
    plt.savefig(outfile)
    print(f'Plot saved to {outfile}')


def plot_energy_breakdown(all_df, period_len):
    """
    Plot stacked bars with energy consumption breakdown
    :param all_df: DataFrame with the merge of the per-resource energy consumptions
    :param period_len: Period length of the energy sampling
    :return: None
    """
    fig, ax1 = plt.subplots()
    for (col_name,col_data) in all_df.filter(like='sys',axis=1).iteritems():
        ax1.bar(all_df['timestamp'], col_data,
                width=5e-5,
                label=col_name.split('.')[1])
    plt.gcf().autofmt_xdate()
    time_formatter = mdates.DateFormatter('%H:%M:%S')
    plt.gca().xaxis.set_major_formatter(time_formatter)
    plt.setp(ax1.get_xticklabels(), rotation=45)
    plt.ylabel('Energy [J]')
    plt.title(f'Energy consumption breakdown\n[execution intervals t={period_len.seconds}s]')
    plt.legend(loc='best')  # , bbox_to_anchor=(1.2, 1.0))
    # plt.show()
    outfile = f'{output_directory}/energy_breakdown.png'
    plt.savefig(outfile)
    print(f'Plot saved to {outfile}')


def plot_application_profiling(app_df):
    for i,(app_name,group) in enumerate(app_df.groupby('application')):
        fig, ax = plt.subplots()
        ax.plot(group['date_time'], group['epc'],
                color='black',
                marker='o',
                linewidth=2,
                label='EpC')
        ax.plot(group['date_time'], group['epc2'],
                color='red',
                marker='d',
                linewidth=2,
                label='EpC2')
        
        group=group.reset_index()
        for i, awm_id in enumerate(group['awm']):
            #print(f'{i} awm={awm_id}')
            ax.annotate(awm_id, (group['date_time'][i], group['epc'][i]))
            ax.annotate(awm_id, (group['date_time'][i], group['epc2'][i]))

        plt.gcf().autofmt_xdate()
        time_formatter = mdates.DateFormatter('%H:%M:%S')
        plt.gca().xaxis.set_major_formatter(time_formatter)
        plt.setp(ax.get_xticklabels(), rotation=45)
    
        
        plt.title(f'Energy-per-Cycle [{app_name}]')
        plt.ylabel('Energy [uJ]')
        plt.legend()
        # plt.show()
        plt.savefig(f'{output_directory}/epc_{app_name}.png')


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input-file-energy', help='The bbque.log extract containing the energy values ',
                        required=True)
    parser.add_argument('-a', '--input-file-epc', help='The bbque.log extract containing the energy values ',
                        required=True)
    parser.add_argument('-o', '--output-directory', help='The output directory containing the resulting plot images',
                        required=True)
    parser.add_argument('-d', '--date', help='Take lines from a specific day', default='')
    parser.add_argument('-t', '--time', help='Take lines from a specific time', default='')
    try:
        args = parser.parse_args()
    except argparse.ArgumentError:
        print('Error in the provided arguments')
        parser.print_help()
        sys.exit(1)
    arguments = vars(args)
    input_file_energy = arguments['input_file_energy']
    input_file_epc = arguments['input_file_epc']
    output_directory = arguments['output_directory']

    # Parse the BarbequeRTRM log:
    print(f'Loading data from {input_file_energy}...')
    all_df = load_energy_consumption_data(input_file_energy, arguments['date'])
    # Loading data from energy_lines.txt...
    #                  date_time       resource   energy
    # 0  2021-03-09 15:49:34.142   sys0.cpu0.pe    2.403
    # 1  2021-03-09 15:49:34.338  sys0.gpu0.pe0   68.410
    # 2  2021-03-09 15:49:34.339   sys0.cpu1.pe    1.873
    # 3  2021-03-09 15:49:54.361   sys0.cpu0.pe   33.942
    # 4  2021-03-09 15:49:54.453  sys0.gpu0.pe0  776.212
    # 5  2021-03-09 15:49:54.453   sys0.cpu1.pe   23.536
    # 6  2021-03-09 15:50:14.466   sys0.cpu0.pe   37.153
    # 7  2021-03-09 15:50:14.556  sys0.gpu0.pe0  799.797
    # ...
    # and
    # ...
    # Build the DataFrame including per-resource energy consumptions:
    #                   timestamp  sys0.cpu0.pe  sys0.gpu0.pe0  sys0.cpu1.pe
    # 0  2021-03-09 15:49:34.142         2.403         68.410         1.873
    # 1  2021-03-09 15:49:54.361        33.942        776.212        23.536
    # 2  2021-03-09 15:50:14.466        37.153        799.797        22.954
    # 3  2021-03-09 15:50:34.568        36.532        781.857        23.496
    # 4  2021-03-09 15:50:54.684        35.810        784.387        23.656
    # 5  2021-03-09 15:51:14.796        34.556        792.969        23.644
    # 6  2021-03-09 15:51:34.910        35.091        784.365        23.863
    # 7  2021-03-09 15:51:55.027        36.504        794.195        24.103
    # 8  2021-03-09 15:52:15.152        35.548        810.036        24.314
    # 9  2021-03-09 15:52:35.269        34.816        794.534        24.088
    # 10 2021-03-09 15:52:55.387        35.203        788.672        24.223
    # 11 2021-03-09 15:53:15.509        36.170        789.524        24.114
    # 12 2021-03-09 15:53:35.627        35.801        807.230        24.342
    # 13 2021-03-09 15:53:55.734        34.873        804.010        23.903
    # 14 2021-03-09 15:54:15.839        34.568        793.349        23.605
    # 15 2021-03-09 15:54:40.301         7.249        195.737         5.898
    # 16 2021-03-09 15:54:45.000         4.145        126.373         2.618

    # Plot
    # - energy_breakdown.*
    # - energy_per_resource.*
    #
    plot_resources_profiling(all_df)

    print(f'Loading data from {input_file_epc}...')
    epc_df = load_epc_data(input_file_epc, arguments['date'])
    # Loading data from data_epc.txt...
    #                  date_time    application  awm         epc 
    # 0  2021-03-09 15:49:54.455  02564:HELEN00    0    14139201
    # 1  2021-03-09 15:50:14.558  02564:HELEN00    1     9904225
    # 2  2021-03-09 15:50:34.668  02564:HELEN00    2  1182692266
    # 3  2021-03-09 15:50:54.786  02564:HELEN00    3  1975418880
    # 4  2021-03-09 15:51:14.900  02564:HELEN00    1  1331033429
    # 5  2021-03-09 15:51:35.015  02564:HELEN00    0  1316860501
    # 6  2021-03-09 15:51:55.133  02564:HELEN00    2  1201173589
    # 7  2021-03-09 15:52:15.259  02564:HELEN00    2  1494848000
    # 8  2021-03-09 15:52:35.377  02564:HELEN00    0  1466288128
    # 9  2021-03-09 15:52:55.495  02564:HELEN00    1  1789119232
    # 10 2021-03-09 15:53:15.608  02564:HELEN00    0  1194175914
    # 11 2021-03-09 15:53:35.724  02564:HELEN00    0  1355262037
    # 12 2021-03-09 15:53:55.829  02564:HELEN00    0  1483682474
    # 13 2021-03-09 15:54:15.937  02564:HELEN00    0  1464044885

    plot_application_profiling(epc_df)
