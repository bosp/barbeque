/*
 * Copyright (C) 2020  Politecnico di Milano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "enero_schedpol.h"

#include <cmath>
//#include <cstdlib>
#include <cstdint>
#include <functional>
#include <iostream>
#include <random>

#include "bbque/modules_factory.h"
#include "bbque/utils/logging/logger.h"
#include "bbque/utils/string_utils.h"

#include "bbque/app/working_mode.h"
#include "bbque/res/binder.h"

#define MODULE_CONFIG SCHEDULER_POLICY_CONFIG "." SCHEDULER_POLICY_NAME

namespace bu = bbque::utils;
namespace po = boost::program_options;

namespace bbque {
namespace plugins {

// :::::::::::::::::::::: Static plugin interface ::::::::::::::::::::::::::::

void * EnerOSchedPol::Create(PF_ObjectParams *)
{
	return new EnerOSchedPol();
}

int32_t EnerOSchedPol::Destroy(void * plugin)
{
	if (!plugin)
		return -1;
	delete (EnerOSchedPol *)plugin;
	return 0;
}

// ::::::::::::::::::::: Scheduler policy module interface :::::::::::::::::::

char const * EnerOSchedPol::Name()
{
	return SCHEDULER_POLICY_NAME;
}

EnerOSchedPol::EnerOSchedPol() :
    cm(ConfigurationManager::GetInstance()),
    eym(EnergyMonitor::GetInstance()),
    eyp(EnergyProfiler::GetInstance())
{
	logger = bu::Logger::GetLogger(MODULE_NAMESPACE);
	assert(logger);
	if (logger)
		logger->Info("enero: Built a new dynamic object[%p]", this);
	else
		fprintf(stderr,
			FI("enero: Built new dynamic object [%p]\n"), (void *)this);
}

EnerOSchedPol::~EnerOSchedPol() { }

SchedulerPolicyIF::ExitCode_t EnerOSchedPol::_Init()
{
	ba::AppCPtr_t papp;
	AppsUidMapIt app_it;

	// Update the <application, working mode> pairs and sort per EpC value
	papp = sys->GetFirstRunning(app_it);
	for (; papp; papp = sys->GetNextRunning(app_it)) {
		auto curr_awm = papp->CurrentAWM();
		auto curr_epc = papp->GetRuntimeProfiledEpC();
		auto curr_epc2 = papp->GetRuntimeProfiledEpC2();
		
		curr_awm->SetRuntimeProfEpC(curr_epc);
		curr_awm->SetRuntimeProfEpC2(curr_epc2);
		
		eyp.PushUpdate(papp, curr_awm);
		logger->Debug("Init: %s running on AWM=%s EpC=%.0f EpC2=%.0f",
			papp->StrId(),
			curr_awm->StrId(),
			curr_awm->GetProfilingData().epc_mean,
			curr_awm->GetProfilingData().epc2_mean);
	}

	for (auto & se: eyp.GetSchedEntityList()) {
		logger->Debug("Init: sched_entity %d:%d mean_epc=%.2f",
			se.papp->Pid(), se.pawm->Id(), se.pawm->GetProfilingData().epc2_mean);
	}

	// Initial resources availability
	this->nr_energy_prof_resources = eym.GetValues().size();
	logger->Debug("Init: number of energy-profiled resources: %u", this->nr_energy_prof_resources);
	this->UpdateResourcesAvailability();

	// Count the applications to schedule
	this->nr_apps_to_schedule  = sys->ApplicationsCount(ba::Schedulable::READY);
	this->nr_apps_to_schedule += sys->ApplicationsCount(ba::Schedulable::THAWED);
	this->nr_apps_to_schedule += sys->ApplicationsCount(ba::Schedulable::RESTORING);
	this->nr_apps_to_schedule += sys->ApplicationsCount(ba::Schedulable::RUNNING);
	logger->Debug("Init: number of applications to schedule = %d", this->nr_apps_to_schedule);

	return SCHED_OK;
}

void EnerOSchedPol::UpdateResourcesAvailability()
{
	for (const auto & eym_entry : eym.GetValues()) {
		const auto & resource_path(eym_entry.first);
		this->availability[resource_path] = sys->ResourceAvailable(
									resource_path,
									this->sched_status_view);
		logger->Debug("UpdateResourcesAvailability: <%s> = %lu",
			resource_path->ToString().c_str(),
			this->availability[resource_path]);
	}
}

SchedulerPolicyIF::ExitCode_t
EnerOSchedPol::Schedule(System & system,
			RViewToken_t & status_view)
{
	// Class providing query functions for applications and resources
	sys = &system;

	// Initialization
	auto result = Init();
	if (result != SCHED_OK) {
		logger->Fatal("Schedule: initialization failed");
		return SCHED_ERROR;
	}
	else {
		logger->Debug("Schedule: resource status view = %ld", sched_status_view);
	}

	// Running applications (we already have profiled EpC per working mode)
	for (auto & sched_entity : eyp.GetSchedEntityList()) {
		// Exploit best profiled working mode or explore a new one?
		if (sched_entity.papp->State() == ba::ApplicationStatusIF::SYNC) {
			logger->Debug("Schedule: %s AWM=%s already scheduled",
				sched_entity.papp->StrId(),
				sched_entity.pawm->StrId());
			continue;
		}
		ScheduleRunningApplication(sched_entity);
	}

	// Not running applications
	auto sched_not_running_app = std::bind(
					static_cast<ExitCode_t (EnerOSchedPol::*)(ba::AppCPtr_t)>
					(&EnerOSchedPol::ScheduleNotRunningApplication),
					this, std::placeholders::_1);

	ForEachApplicationNotRunningDo(sched_not_running_app);
	if (result != SCHED_OK) {
		logger->Debug("Schedule: not SCHED_OK return [result=%d]", result);
		return result;
	}

	// TODO: What if we end up here with some failed scheduling attempts?

	logger->Debug("Schedule: done with applications");

	// Return the new resource status view according to the new resource
	// allocation performed
	status_view = sched_status_view;

	return SCHED_DONE;
}

SchedulerPolicyIF::ExitCode_t
EnerOSchedPol::ScheduleApplication(ba::AppCPtr_t papp, ba::AwmPtr_t next_awm)
{
	if (papp == nullptr) {
		logger->Error("ScheduleApplications: null application descriptor!");
		return SCHED_ERROR;
	}

	if (next_awm == nullptr) {
		logger->Error("ScheduleApplications: null working mode!");
		return SCHED_ERROR;
	}

	// Validate the scheduling request
	ApplicationManager & am(ApplicationManager::GetInstance());
	auto am_ret = am.ScheduleRequest(papp, next_awm, sched_status_view);
	if (am_ret != ApplicationManager::AM_SUCCESS) {
		logger->Error("ScheduleApplication: [%s] schedule request failed"
			"[ret=%d]",
			papp->StrId(),
			am_ret);
		return SCHED_ERROR;
	}

	logger->Debug("ScheduleApplication: [%s] status = <%s %s>",
		papp->StrId(),
		ba::Schedulable::stateStr[papp->State()],
		ba::Schedulable::syncStateStr[papp->SyncState()]);

	this->nr_apps_to_schedule--;
	this->UpdateResourcesAvailability();

	return SCHED_OK;
}

SchedulerPolicyIF::ExitCode_t
EnerOSchedPol::ScheduleNotRunningApplication(bbque::app::AppCPtr_t papp)
{
	logger->Debug("ScheduleNotRunningApplication: %s [%s] is looking for an AWM...",
		papp->StrId(), ba::Schedulable::stateStr[papp->State()]);

	AwmPtr_t next_awm;
	if (papp->State() == ba::Schedulable::READY) {
		next_awm = this->ExploreInitialWorkingMode(papp);
	}
	else {
		next_awm = this->ExploreNewWorkingMode(papp, nullptr);
	}
	return this->ScheduleApplication(papp, next_awm);
}

SchedulerPolicyIF::ExitCode_t
EnerOSchedPol::ScheduleRunningApplication(SchedEntity & sched_entity)
{
	logger->Debug("ScheduleRunningApplication: %s starts from AWM=%s",
		sched_entity.papp->StrId(),
		sched_entity.pawm->StrId());

	AwmPtr_t next_awm; // TODO: we need a smarter criteria here...
	if (sched_entity.papp->ScheduleCount() > 10) {
		next_awm = sched_entity.pawm;
		logger->Debug("ScheduleRunningApplication: %s to re-schedule with awm=%s",
			sched_entity.papp->StrId(),
			sched_entity.pawm->StrId());
	}
	else {
		next_awm = this->ExploreNewWorkingMode(sched_entity.papp, sched_entity.pawm);
	}

	if (next_awm == nullptr) {
		logger->Debug("ScheduleRunningApplication: %s error in resource assignment",
			sched_entity.papp->StrId());
		return ExitCode_t::SCHED_ERROR_INCOMPLETE_ASSIGNMENT;
	}

	logger->Debug("ScheduleRunningApplication: %s scheduled with AWM=%s",
		sched_entity.papp->StrId(), next_awm->StrId());

	return this->ScheduleApplication(sched_entity.papp, next_awm);
}

ba::AwmPtr_t
EnerOSchedPol::ExploreInitialWorkingMode(ba::AppCPtr_t papp)
{
	if (papp->GetRuntimeWorkingModeCount() > 0) {
		logger->Warn("ExploreInitialWorkingMode: %s with not 0 runtime AWMs ",
			papp->StrId());
	}

	std::list<ResourceAssignmentCandidate> resources;
	this->ExploreInitialResourceAssignments(papp, resources);
	logger->Debug("ExploreInitialWorkingMode: %s assigning %d resources",
		papp->StrId(), resources.size());

	return CreateWorkingMode(papp, resources);
}

ba::AwmPtr_t
EnerOSchedPol::ExploreNewWorkingMode(ba::AppCPtr_t papp, ba::AwmPtr_t best_awm)
{
	logger->Debug("ExploreNewWorkingMode: %s looking for a new AWM",
		papp->StrId());

	// Given the resource assignments of the starting AWM explore a new
	// resources assignment solution
	std::list<ResourceAssignmentCandidate> resources;
	std::list<ResourceAssignmentAction> actions;
	this->ExploreNewResourceAssignments(papp, best_awm, resources, actions);
	if (actions.empty()) {
		logger->Debug("ExploreInitialWorkingMode: %s re-assigning [%s]",
			papp->StrId(), best_awm->StrId());
		return best_awm;
	}

	// Create the new AWM resulting from the exploration
	logger->Debug("ExploreNewWorkingMode: %s new AWM will include %d resources",
		papp->StrId(), resources.size());
	auto new_awm = CreateWorkingMode(papp, resources);

	// Keep track of the resource assignment variations(comma separated) that led us to this AWM
	std::string resource_paths,diff_amounts,randomly_generated;
	for(const auto & a : actions){
		int diff_amount = a.amount - papp->CurrentAWM()->GetRequestedAmount(a.resource_path);
		logger->Debug("ExploreNewWorkingMode: %s building attribute: resource=<%s> action=%d curr=%d diff=%d",
			papp->StrId(),
			a.resource_path->ToString().c_str(),
			a.amount,
			papp->CurrentAWM()->GetRequestedAmount(a.resource_path),
			diff_amount);
		resource_paths = resource_paths + a.resource_path->ToString() +",";
		diff_amounts = diff_amounts + std::to_string(diff_amount) + ",";
		randomly_generated = std::to_string(a.randomly_generated);
	}
	// Keep track of the resource assignment variation that led us to this AWM
	new_awm->SetAttribute("enero_path", resource_paths);
	new_awm->SetAttribute("enero_diff_amount", diff_amounts);
	new_awm->SetAttribute("enero_random",randomly_generated);
	
	return new_awm;
}

SchedulerPolicyIF::ExitCode_t
EnerOSchedPol::ExploreInitialResourceAssignments(ba::AppCPtr_t papp,
						 std::list<ResourceAssignmentCandidate> & resources)
{
	// Fair assignment of CPU quota
	auto cpu_quota_available  = sys->ResourceAvailable("sys.cpu.pe", this->sched_status_view);
	auto fair_cpu_quota       = cpu_quota_available / this->nr_apps_to_schedule;
	auto nr_cpu_units         = fair_cpu_quota / SCHED_CPU_QUOTA_MIN;
	auto cpu_quota_slice      = nr_cpu_units * SCHED_CPU_QUOTA_MIN;
	auto cpu_quota_slice_left = cpu_quota_slice;

	logger->Debug("ExploreInitialResourceAssignments: %s resources monitored=<%d>",
                        papp->StrId(),
                        eym.GetValues().size());
	
	// For each energy-monitored resource evaluate the amount to assign
	for (const auto & resource_energy : eym.GetValues()) {
		auto & resource_path(resource_energy.first);
		auto & energy(resource_energy.second);

		logger->Debug("ExploreInitialResourceAssignments: %s resource=<%s> energy=%lu",
			papp->StrId(),
			resource_path->ToString().c_str(),
			energy);

		logger->Debug("ExploreInitialResourceAssignments: %s resource_availability=<%d> type=%d",
                        papp->StrId(),
                        this->availability[resource_path],
                        resource_path->ParentType());

		if (this->availability[resource_path] == 0) {
			logger->Debug("ExploreInitialResourceAssignments: %s no <%s> left",
				papp->StrId(), resource_path->ToString().c_str());
			continue;
		}
		

		ResourceAssignmentCandidate candidate;
		candidate.first = resource_path;

		switch (resource_path->ParentType()) {

		case br::ResourceType::CPU:
			if (cpu_quota_slice_left == 0) {
				logger->Debug("ExploreInitialResourceAssignments: "
					"%s CPU already assigned",
					papp->StrId());
				continue;
			}
			// Try to enforce a per-CPU locality
			candidate.second = std::min(this->availability[resource_path], cpu_quota_slice_left);
			cpu_quota_slice_left -= candidate.second;
			logger->Debug("ExploreInitialResourceAssignments: "
				"%s assigned <%s>=%lu/%lu",
				papp->StrId(),
				candidate.first->ToString().c_str(),
				candidate.second,
				cpu_quota_slice);
			break;

		default:
			logger->Warn("ExploreInitialResourceAssignments: %s"
				" skipping <%s>",
				papp->StrId(),
				resource_path->ToString().c_str());
			continue;
		}

		logger->Debug("ExploreInitialResourceAssignments: %s found candidate resources",
			papp->StrId());
		resources.push_back(candidate);
	}

	return SCHED_OK;
}

void
EnerOSchedPol::ExploreNewResourceAssignments(ba::AppCPtr_t papp,
					     ba::AwmPtr_t best_awm,
					     std::list<ResourceAssignmentCandidate> & resources,
						 std::list<ResourceAssignmentAction> & next_action)
{
	ba::AwmPtr_t pawm;
	unsigned short int nr_attempts = 5; // make a certain number of attempts
	

	do {
		// Choose the action
		this->ExploreResourceAssignment(papp,
										best_awm,
										next_action);
		
		if (next_action.empty()) {
			logger->Error("FindExplorationAction: %s exploration failed",
				papp->StrId());
		}

		// Apply the action to get a new resources set
		this->ApplyResourceAssignmentExploration(papp,
												next_action,
												resources);
		if (!resources.empty()) {
			// Check there is no AWM with the same resources set
			logger->Debug("ExploreNewResourceAssignments: %s "
						"checking the exploration result...",
						papp->StrId());
			pawm = this->FindWorkingModeByResources(papp, resources);
			if (pawm != nullptr) {
				logger->Debug("ExploreNewResourceAssignments: %s "
					"exploration led to [%s]",
					papp->StrId(),
					pawm->StrId());
				resources.clear();
				best_awm=pawm;
			}
		}
		else {
			logger->Error("ExploreNewResourceAssignments: %s "
				"unexpected empty resources set",
				papp->StrId());
		}
	} while ( ( --nr_attempts > 0 ) && 
		  	( pawm != nullptr || resources.empty() ));

	if (pawm == nullptr) {
		logger->Debug("ExploreNewResourceAssignments: %s "
			" found a resources set useless for a new AWM",
			papp->StrId());
	}

}

void EnerOSchedPol::ExploreResourceAssignment(ba::AppCPtr_t papp,
										 ba::AwmPtr_t best_awm,
										 std::list<ResourceAssignmentAction> & actions)
{
	int delta_epc;
	bool last_chance=false;

	logger->Debug("ExploreResourceAssignment: %s scheduled %d times",
		papp->StrId(),
		papp->ScheduleCount());
	logger->Debug("ExploreResourceAssignment: %s curr_awm=%s-epc=%.0f best_awm=%s-epc=%.0f",
		papp->StrId(),
		papp->CurrentAWM()->StrId(),
		papp->CurrentAWM()->GetProfilingData().epc2_mean,
		best_awm->StrId(),
		best_awm->GetProfilingData().epc2_mean);

	actions.clear();
	
	if (papp->PrevAWM() != nullptr) {
		delta_epc = papp->PrevAWM()->GetProfilingData().epc2_mean - papp->CurrentAWM()->GetProfilingData().epc2_mean;
		
		logger->Debug("ExploreResourceAssignment: %s epc improvements=%d",
			papp->StrId(), delta_epc);

		//Get list of resources affected by previous actions
		std::string prev_resource_path(papp->CurrentAWM()->GetAttribute("enero_path"));
		std::string prev_diffs(papp->CurrentAWM()->GetAttribute("enero_diff_amount"));
		uint32_t prev_random=std::atoi(papp->CurrentAWM()->GetAttribute("enero_random").c_str());
		

		logger->Debug("ExploreResourceAssignment: %s  %s %d",
				prev_resource_path.c_str(),
				prev_diffs.c_str(),
				prev_random);
	
		std::list<std::string> split_prev_resource,split_prev_diffs;
		bu::SplitString(prev_resource_path,split_prev_resource,",");
		bu::SplitString(prev_diffs,split_prev_diffs,",");
		
		for(auto i=split_prev_resource.begin(),j=split_prev_diffs.begin();
			i != --split_prev_resource.end() && j != --split_prev_diffs.end();
			i++,j++){
			ResourceAssignmentAction a;
			a.resource_path=sys->GetResourcePath(*i);
			
			auto diff_amount=std::atoi(j->c_str());
			int32_t new_amount = papp->CurrentAWM()->GetRequestedAmount(*i) + diff_amount;
			logger->Debug("ExploreResourceAssignment: %s recover action value on <%s>:old=%d curr=%d (+/-)= %d new= %d",
							papp->StrId(),
							a.resource_path->ToString().c_str(),
							papp->CurrentAWM()->GetRequestedAmount(*i) + diff_amount,
							papp->CurrentAWM()->GetRequestedAmount(*i),
							diff_amount,
							new_amount);
			new_amount = std::min({ new_amount,
									(int32_t) this->availability[a.resource_path], 
									(int32_t) this->availability[a.resource_path]});
			if(new_amount>0) {
				a.amount = new_amount;
				logger->Debug("ExploreResourceAssignment: %s new action value on <%s>: set to %d",
							papp->StrId(),
							a.resource_path->ToString().c_str(),
							a.amount);
			}
			else {
				continue;
			}
			
			
			/*Reapplying previous action we could have skipped a local minimum, let's try an intermediate step*/
			if(delta_epc<0 && !prev_random){

				int granularity;
				switch (a.resource_path->ParentType()) {
					case br::ResourceType::CPU:
						granularity = SCHED_CPU_QUOTA_MIN;
						break;
					case br::ResourceType::GPU:
						granularity = SCHED_GPU_QUOTA_MIN;
						break;
					default:
						granularity = SCHED_ACCELERATOR_QUOTA_MIN;
						break;
				}
				
				diff_amount=(int) ( ( ( diff_amount/granularity ) / 2.0) * granularity );
				logger->Debug("ExploreResourceAssignment: %s trying intermediate step <%s>: (+/-) %d ",
							papp->StrId(),
							a.resource_path->ToString().c_str(),
							diff_amount);

				new_amount = papp->CurrentAWM()->GetRequestedAmount(*i) + diff_amount;
				new_amount = std::min({ new_amount,
									(int32_t) this->availability[a.resource_path], 
									(int32_t) this->availability[a.resource_path]});
				if(new_amount>0){
					logger->Debug("ExploreResourceAssignment: %s new interpolate amount <%s>: %d -> %d",
									papp->StrId(),
									a.resource_path->ToString().c_str(),
									a.amount,
									new_amount);
					if(!diff_amount)
							last_chance=true;
				}
				else{
					continue;
				}
			}
			if( new_amount ){
				actions.push_back(a);
			}
		}
		logger->Debug("ExploreResourceAssignment: %s actions %d",
				papp->StrId(),
				actions.size());
		// Check that we have at least one action on CPU >=0
		// and that we leave enough resources for the remaining applications
		// otherwise we have to empty the list
		/* Checks if current actions will leave enough resources for the next applications*/
		uint32_t remaining_cpu=0, max_remaining_cpu=0, total_cpu_taken=0;		
			
		for(const auto & a: actions){
				if( a.resource_path->ParentType() == br::ResourceType::CPU ){
					total_cpu_taken += a.amount;
					remaining_cpu += (this->availability[a.resource_path]-a.amount);
					if((this->availability[a.resource_path]-a.amount)>max_remaining_cpu){
						max_remaining_cpu=this->availability[a.resource_path]-a.amount;
					}
				}
		}
		
		if(this->nr_apps_to_schedule-1){
			if((max_remaining_cpu/(this->nr_apps_to_schedule-1))<SCHED_CPU_SLACK_MIN){
				actions.clear();
			}
		}
		logger->Debug("ExploreResourceAssignment: %s actions will leave %d CPU resources(%d max) for next %d applications",
				papp->StrId(),
				remaining_cpu,
				total_cpu_taken,
				this->nr_apps_to_schedule-1);

		if( papp->GetRecipe()->GetMinResources() >= total_cpu_taken ){
			actions.clear();
			logger->Debug("ExploreResourceAssignmentRandom: %s requires min %d CPU, %d taken",
					papp->StrId(),
					papp->GetRecipe()->GetMinResources(),
					total_cpu_taken);
		}
		
	}
	else {
		logger->Debug("ExploreResourceAssignment: %s no previous AWM",
			papp->StrId());
	}

	if ((delta_epc>=0 && papp->ScheduleCount() >= 2) || last_chance) {
		// Let's apply the same action to improve further
		if (actions.empty()) {
			logger->Debug("ExploreResourceAssignment: %s exploration not feasible, restart with random assignment",
				papp->StrId());
			this->ExploreResourceAssignmentRandom(papp,actions);
			return;
		}
		
		for(auto a : actions) {
			logger->Debug("ExploreResourceAssignment: %s applying <%s> : %d",
			papp->StrId(),
			a.resource_path->ToString().c_str(),
			a.amount);
		}
			return;
	}
	else {
		// Previous scheduling did not assign the best AWM (so far)
		delta_epc = papp->CurrentAWM()->GetProfilingData().epc2 - best_awm->GetProfilingData().epc2;
		logger->Debug("ExploreResourceAssignment: %s distance from the "
			"best working mode delta_epc=%d",
			papp->StrId(), delta_epc);
	}
	actions.clear();
	// Go with a random resource allocation exploration
	this->ExploreResourceAssignmentRandom(papp,actions);
}

void EnerOSchedPol::ExploreResourceAssignmentRandom(ba::AppCPtr_t papp, 
													std::list<ResourceAssignmentAction> & actions)
{
	int attempts=5;
	// Random selection of a energy-profiled resource
	static std::random_device rand_gen;
	
	
	logger->Debug("ExploreResourceAssignmentRandom: %s random exploration",
				   papp->StrId());
	
	
	do{
		attempts--;
		for(int i=0;i<this->nr_energy_prof_resources;i++){
			auto resource_index = i;

			logger->Debug("ExploreResourceAssignmentRandom: random index %d",
						   resource_index);

			ResourceAssignmentAction action;
			for (auto & resource_entry : eym.GetValues()) {
				if (resource_index-- == 0)
					action.resource_path = resource_entry.first;
			}

			logger->Debug("ExploreResourceAssignmentRandom: random resource path %s",
			action.resource_path->ToString().c_str());
			// Set a minimum value and the granularity, depending on the processor type
			int min_amount = 0;
			unsigned int granularity;
			switch (action.resource_path->ParentType()) {
			case br::ResourceType::CPU:
				min_amount  = 100 / SCHED_CPU_QUOTA_MIN;
				granularity = SCHED_CPU_QUOTA_MIN;
				break;
			case br::ResourceType::GPU:
				granularity = SCHED_GPU_QUOTA_MIN;
				break;
			default:
				granularity = SCHED_ACCELERATOR_QUOTA_MIN;
				break;
			}

			// Random resource amount given a certain granularity
			int max_amount = this->availability[action.resource_path] / granularity;
			

			if(max_amount>=min_amount){
				std::uniform_int_distribution<int> resource_amount_dist(min_amount, max_amount);
				int random_amount = resource_amount_dist(rand_gen);
				logger->Debug("ExploreResourceAssignmentRandom: %s action: <%s> random=%d [min=%d, max=%d]",
								papp->StrId(),
								action.resource_path->ToString().c_str(),
								random_amount,
								min_amount,
								max_amount);
				logger->Debug("ExploreResourceAssignmentRandom: %s action: <%s>,%d [granularity=%d]",
									papp->StrId(),
									action.resource_path->ToString().c_str(),
									action.amount,
									granularity);
				if ( random_amount ){
					// The new resource amount
					action.amount = random_amount * granularity;
					action.randomly_generated=true;
					actions.push_back(action);	
				}
				else{
					logger->Debug("ExploreResourceAssignmentRandom: %s action: <%s>. Resource skipped!",
									papp->StrId(),
									action.resource_path->ToString().c_str());
				}
				
			}
		}

		/* Checks if current actions will leave enough resources for the next applications*/
		uint32_t remaining_cpu=0, max_remaining_cpu=0, total_cpu_taken=0;		
			
		for(const auto & a: actions){
				if( a.resource_path->ParentType() == br::ResourceType::CPU ){
					total_cpu_taken += a.amount;
					remaining_cpu += (this->availability[a.resource_path]-a.amount);
					if((this->availability[a.resource_path]-a.amount)>max_remaining_cpu){
						max_remaining_cpu=this->availability[a.resource_path]-a.amount;
					}
				}
		}

		logger->Debug("ExploreResourceAssignmentRandom: %s actions will leave %d CPU total resources (%d max) for next %d applications",
					papp->StrId(),
					remaining_cpu,
					max_remaining_cpu,
					this->nr_apps_to_schedule-1);
		
		if(this->nr_apps_to_schedule-1){
			if ((max_remaining_cpu/(this->nr_apps_to_schedule-1))<(SCHED_CPU_SLACK_MIN))
			{
				actions.clear();
			}
		}
		
		if( papp->GetRecipe()->GetMinResources() >= total_cpu_taken ){
			actions.clear();
			logger->Debug("ExploreResourceAssignmentRandom: %s requires min %d CPU, %d taken",
					papp->StrId(),
					papp->GetRecipe()->GetMinResources(),
					total_cpu_taken);
		}
		}while( actions.empty() && attempts>=0);
}

void EnerOSchedPol::ApplyResourceAssignmentExploration(ba::AppCPtr_t papp,
						       std::list<EnerOSchedPol::ResourceAssignmentAction> & actions,
						       std::list<ResourceAssignmentCandidate> & resources)
{

	for(const auto & action : actions){
		if (action.amount > 0) {
			resources.emplace_back(action.resource_path, action.amount);
		}
	}
	

	
}

ba::AwmPtr_t
EnerOSchedPol::CreateWorkingMode(ba::AppCPtr_t papp,
				 std::list<ResourceAssignmentCandidate> & resources)
{
	auto pawm = papp->CreateNewRuntimeWorkingMode();
	
	logger->Debug("CreateWorkingMode: %s new AWM id=%s",
		papp->StrId(), pawm->StrId());

	for (const auto & r : resources) {
		auto & resource_path(r.first);
		auto resource_amount = r.second;
		if (resource_amount == 0) {
			logger->Debug("CreateWorkingMode: %s [%s] discarding <%s>",
				papp->StrId(),
				pawm->StrId(),
				resource_path->ToString().c_str());
			continue;
		}

		logger->Debug("CreateWorkingMode: %s [%s] assigning <%s>",
			papp->StrId(),
			pawm->StrId(),
			r.first->ToString().c_str());

		auto wm_ret = pawm->AssignResource(resource_path, resource_amount);
		if (wm_ret != ba::WorkingModeStatusIF::WM_SUCCESS) {
			logger->Debug("CreateWorkingMode: %s [%s] error=%d",
				papp->StrId(),
				pawm->StrId(),
				wm_ret);
			return nullptr;
		}
	}

	return pawm;
}

ba::AwmPtr_t
EnerOSchedPol::FindWorkingModeByResources(ba::AppCPtr_t papp,
					  std::list<ResourceAssignmentCandidate> & resources_request)
{
	logger->Debug("FindWorkingModeByResources: %s resource assignments: %d ",
		papp->StrId(), resources_request.size());

	ba::AwmPtr_t match_awm;
	for (const auto & awm_entry : papp->GetRuntimeWorkingModes()) {
		match_awm = awm_entry.second;
		//Ugly workaround to overcome errors with std::map.find()
		std::map<std::string,uint64_t> resource_bindings;
		for(auto const & res_bind : *(match_awm->GetResourceBinding())){
			resource_bindings[res_bind.first->ToString().c_str()]=res_bind.second->GetAmount();
		}
		for (const auto & request : resources_request) {
			logger->Debug("FindWorkingModeByResources: %s [%s] looking for "
					" <%s>:%lu",
					papp->StrId(),
					awm_entry.second->StrId(),
					request.first->ToString().c_str(),
					request.second);
			logger->Debug("FindWorkingModeByResources: %s in [%s] %d times",
					request.first->ToString().c_str(),
					awm_entry.second->StrId(),
					resource_bindings.count(request.first->ToString()));	
			if(resource_bindings.count(request.first->ToString()) == 0){
				logger->Debug("FindWorkingModeByResources: %s [%s] does not include "
					" <%s>:%lu",
					papp->StrId(),
					awm_entry.second->StrId(),
					request.first->ToString().c_str(),
					request.second);
				match_awm = nullptr;
				break;
			}
			else {
				const auto awm_request = resource_bindings.at(request.first->ToString());
				logger->Debug("FindWorkingModeByResources: %s [%s] includes "
					" <%s>:%lu/%lu",
					papp->StrId(),
					awm_entry.second->StrId(),
					request.first->ToString().c_str(),
					request.second,
					awm_request);

				if(awm_request == request.second){
					logger->Debug("FindWorkingModeByResources: %s [%s] same request on "
					" <%s>",
					papp->StrId(),
					awm_entry.second->StrId(),
					request.first->ToString().c_str());
				}
				else{
					logger->Debug("FindWorkingModeByResources: %s [%s] different request on "
					" <%s>",
					papp->StrId(),
					awm_entry.second->StrId(),
					request.first->ToString().c_str());
					match_awm = nullptr;
					break;
				}
			}
		}

		if (match_awm != nullptr) {
			if(match_awm->NumberOfResourceRequests()!=resources_request.size()){
				logger->Warn("FindWorkingModeByResources: %s explored "
							"resource assignments are a subset of AWM [%s]. Size %d/%d(AWM/REQ) ",
							papp->StrId(),
							match_awm->StrId(),
							match_awm->NumberOfResourceRequests(),
							resources_request.size());
				match_awm = nullptr;
			}
			else{
				logger->Warn("FindWorkingModeByResources: %s explored "
								"resource assignments already existing in [%s]. Size %d/%d(AWM/REQ) ",
								papp->StrId(),
								match_awm->StrId(),
								match_awm->NumberOfResourceRequests(),
								resources_request.size());
				return match_awm;
			}
			
		}
	}

	logger->Debug("FindWorkingModeByResources: %s no working modes"
		" containing the new resource assignments", papp->StrId());
	return nullptr;
}

} // namespace plugins

} // namespace bbque
