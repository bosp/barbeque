/*
 * Copyright (C) 2016  Politecnico di Milano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BBQUE_ENERO_SCHEDPOL_H_
#define BBQUE_ENERO_SCHEDPOL_H_

#include <cstdint>
#include <list>
#include <memory>

#include "bbque/configuration_manager.h"
#include "bbque/energy_monitor.h"
#include "bbque/energy_profiler.h"
#include "bbque/plugins/plugin.h"
#include "bbque/plugins/scheduler_policy.h"
#include "bbque/scheduler_manager.h"

#define SCHEDULER_POLICY_NAME "enero"

#define MODULE_NAMESPACE SCHEDULER_POLICY_NAMESPACE "." SCHEDULER_POLICY_NAME

// The CPU quota assignment must be multiple of this
#ifdef CONFIG_TARGET_TEGRA
#define SCHED_CPU_QUOTA_MIN          100
#else
#define SCHED_CPU_QUOTA_MIN          50
#endif
#define SCHED_CPU_SLACK_MIN			 100
// Amount of GPU quota to assign, used to define the max number of applications
// to co-schedule on the GPUs
#define SCHED_GPU_QUOTA_MIN          25 // => max 4 application per GPU
// Accelerators can be assigned in a exclusive manner only
#define SCHED_ACCELERATOR_QUOTA_MIN 100

using bbque::res::RViewToken_t;
using bbque::utils::MetricsCollector;
using bbque::utils::Timer;

// These are the parameters received by the PluginManager on create calls
struct PF_ObjectParams;

namespace bbque {
namespace plugins {

class LoggerIF;

/**
 * @class PrFCFSSchedPol
 *
 * PrFCFS scheduler policy registered as a dynamic C++ plugin.
 */
class EnerOSchedPol : public SchedulerPolicyIF
{
public:

	using ResourceAssignmentCandidate = std::pair<br::ResourcePathPtr_t, uint64_t>;

	class ResourceAssignmentAction
	{
	public:

		ResourceAssignmentAction() :
		    resource_path(nullptr),
		    amount(0),
		    perf_state(0),
			randomly_generated(0)
			{ }

		bool operator==(const ResourceAssignmentAction & other) const
		{
			if ((other.resource_path == this->resource_path) &&
			(other.amount == this->amount) &&
			(other.perf_state == this->perf_state))
				return true;
			return false;
		}

		br::ResourcePathPtr_t resource_path;
		int32_t amount;
		int32_t perf_state;
		int32_t randomly_generated;
	};

	// :::::::::::::::::::::: Static plugin interface :::::::::::::::::::::::::

	/**
	 * @brief Create the enero plugin
	 */
	static void * Create(PF_ObjectParams *);

	/**
	 * @brief Destroy the enero plugin
	 */
	static int32_t Destroy(void *);


	// :::::::::::::::::: Scheduler policy module interface :::::::::::::::::::

	/**
	 * @brief Destructor
	 */
	virtual ~EnerOSchedPol();

	/**
	 * @brief Return the name of the policy plugin
	 */
	char const * Name();


	/**
	 * @brief The member function called by the SchedulerManager to perform a
	 * new scheduling / resource allocation
	 */
	ExitCode_t Schedule(System & system, RViewToken_t & status_view);


private:

	/** Configuration manager instance */
	ConfigurationManager & cm;

	/** Energy monitor instance */
	EnergyMonitor & eym;

	/** The profiler sorting the scheduled applications per energy efficiency */
	EnergyProfiler & eyp;

	/** System logger instance */
	std::unique_ptr<bu::Logger> logger;


	/** The number of energy profiled resources */
	size_t nr_energy_prof_resources;

	/** Amount of available resources, used as a cache data structure */
	std::map<br::ResourcePathPtr_t, uint64_t> availability;

	/** Counter of the number of applications to schedule */
	uint32_t nr_apps_to_schedule;

	/**
	 * @brief Constructor
	 *
	 * Plugins objects could be build only by using the "create" method.
	 * Usually the PluginManager acts as object
	 */
	EnerOSchedPol();

	/**
	 * @brief Optional initialization member function
	 */
	ExitCode_t _Init();

	/**
	 * @brief Invoked after each application scheduling to update the
	 * current availability of resources used as a cache, instead of
	 * calling the System member functions
	 */
	void UpdateResourcesAvailability();

	/**
	 * @brief Entry point for the scheduling of applications not in RUNNING
	 * state
	 *
	 * @param papp The pointer to the application descriptor
	 */
	ExitCode_t ScheduleNotRunningApplication(ba::AppCPtr_t papp);

	/**
	 * @brief Entry point for the scheduling of applications in RUNNING state
	 *
	 * @param se A SchedEntity object containing the pointer to the application
	 * descriptor and the best working mode for energy efficiency
	 */
	ExitCode_t ScheduleRunningApplication(SchedEntity & se);

	/**
	 * @brief The function containing the statements for performing the
	 * actual scheduling request
	 *
	 * @param papp The pointer to the application descriptor
	 * @param next_awm Assigned working mode
	 * @return
	 */
	ExitCode_t ScheduleApplication(ba::AppCPtr_t papp, ba::AwmPtr_t next_awm);

	/**
	 * @brief Creation of the first working mode for a yet to start application
	 * @param papp The pointer to the application descriptor
	 * @return A pointer to the working mode object created
	 */
	ba::AwmPtr_t ExploreInitialWorkingMode(ba::AppCPtr_t papp);

	/**
	 * @brief For the creation of the first working mode, we need to explore
	 * a first assignment of resources
	 *
	 * @param papp The pointer to the application descriptor
	 * @param resources The list of ResourceAssignmentCandidate objects that
	 * will be used to populate with AWM with resource assignments
	 * @return
	 */
	ExitCode_t ExploreInitialResourceAssignments(ba::AppCPtr_t papp,
						std::list<ResourceAssignmentCandidate> & resources);

	/**
	 * @brief Exploration of a new working mode for running applications.
	 *
	 * The function creates a new working mod to provide a comparison in
	 * terms of energy efficiency, with the following policy invocations.
	 * @param papp Application descriptor
	 * @param best_awm The best working mode found so far
	 * @return The new working mode obtained from the exploration
	 */
	ba::AwmPtr_t ExploreNewWorkingMode(ba::AppCPtr_t papp, ba::AwmPtr_t best_awm);

	/**
	 * @brief Explore new resource assignments for a new working mode
	 *
	 * @param papp The pointer to the application descriptor
	 * @param best_awm The pointer to the best working mode object
	 * @param resources The list of ResourceAssignmentCandidate objects that
	 * will be used to populate with AWM with resource assignments
	 * @param next_action the actions to apply to the current working mode
	 * @return The transition action that leads to the new working mode, from
	 * the current one
	 */
	void ExploreNewResourceAssignments(ba::AppCPtr_t papp,
							ba::AwmPtr_t best_awm,
							std::list<ResourceAssignmentCandidate> & resources,
							std::list<ResourceAssignmentAction> & next_action);
	/**
	 *
	 * @brief Look for the resource assignment action to apply for scheduling
	 * the application by assigning a new working mode
	 *
	 * @param papp The pointer to the application descriptor
	 * @param best_awm the best working mode so far
	 * @param resources The list of ResourceAssignmentCandidate objects that
	 * will be used to populate with AWM with resource assignments
	 * @param actions The actions to apply to the current working mode
	 * @return The transition action to apply to the current AWM to get
	 * the resource assignments for the new working mode
	 */
	void ExploreResourceAssignment(ba::AppCPtr_t papp,
							ba::AwmPtr_t best_awm,
							std::list<ResourceAssignmentAction> & actions);

	/**
	 *
	 * @param papp
	 * @param actions
	 * @return
	 */
	void ExploreResourceAssignmentRandom(ba::AppCPtr_t papp,
							std::list<ResourceAssignmentAction> & actions);

	/**
	 * @brief Apply the exploration action to the current working mode to
	 * obtain the new set of resource assignments for the next working mode
	 *
	 * @param papp The pointer to the application descriptor
	 * @param actions the actions to apply to the current working mode
	 * @param resources the list of candidate resource assignments obtained
	 * after applying the action
	 */
	void ApplyResourceAssignmentExploration(ba::AppCPtr_t papp,
						std::list<EnerOSchedPol::ResourceAssignmentAction> & actions,
						std::list<ResourceAssignmentCandidate> & resources);

	/**
	 * @brief Creation of a new working mode, given the new resource assignments
	 *
	 * @param papp The pointer to the application descriptor
	 * @param resources the candidate resource assignments
	 * @return a pointer to the new working mode object
	 */
	ba::AwmPtr_t CreateWorkingMode(ba::AppCPtr_t papp,
				std::list<ResourceAssignmentCandidate> & resources);

	/**
	 * @brief Check if the resource assignments match an already existing working mode
	 *
	 * @param papp The pointer to the application descriptor
	 * @param resources the candidate resource assignments
	 * @return the working mode object (pointer) if a match has been found,
	 * nullptr in case the resource assignments can be used for a new working mode
	 */
	ba::AwmPtr_t FindWorkingModeByResources(ba::AppCPtr_t papp,
						std::list<ResourceAssignmentCandidate> & resources);

};

} // namespace plugins

} // namespace bbque

#endif // BBQUE_ENERO_SCHEDPOL_H_
