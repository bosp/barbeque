/*
 * Copyright (C) 2016  Politecnico di Milano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "reliam_schedpol.h"
#include "pid_controller.h"

#include <cstdlib>
#include <cstdint>
#include <iostream>
#include <math.h> 

#include <fstream>

#include "bbque/modules_factory.h"
#include "bbque/binding_manager.h"
#include "bbque/utils/logging/logger.h"

#include "bbque/app/working_mode.h"
#include "bbque/res/resource_path.h"
#include "bbque/res/binder.h"
#include "bbque/res/resource_utils.h"
#include "bbque/utils/timer.h"


#define MIN_QUOTA 25
#define OVERPROVISIONING 10
#define DEAD_BAND_CUT_OFF_VALUE 5

#define MODULE_CONFIG SCHEDULER_POLICY_CONFIG "." SCHEDULER_POLICY_NAME

namespace bu = bbque::utils;
namespace po = boost::program_options;

using namespace std::placeholders;

namespace bbque { namespace plugins {

// :::::::::::::::::::::: Static plugin interface ::::::::::::::::::::::::::::

void * ReliamSchedPol::Create(PF_ObjectParams *) {
	return new ReliamSchedPol();
}

int32_t ReliamSchedPol::Destroy(void * plugin) {
	if (!plugin)
		return -1;
	delete (ReliamSchedPol *)plugin;
	return 0;
}

// ::::::::::::::::::::: Scheduler policy module interface :::::::::::::::::::

char const * ReliamSchedPol::Name() {
	return SCHEDULER_POLICY_NAME;
}

ReliamSchedPol::ReliamSchedPol():
		cm(ConfigurationManager::GetInstance()),
		ra(ResourceAccounter::GetInstance())
#ifdef CONFIG_BBQUE_LINUX_PROC_MANAGER
		,prm(ProcessManager::GetInstance())
#endif
{
	logger = bu::Logger::GetLogger(MODULE_NAMESPACE);
	assert(logger);
	if (logger)
		logger->Info("reliam: Built a new dynamic object[%p]", this);
	else
		fprintf(stderr,
			FI("reliam: Built new dynamic object [%p]\n"), (void *)this);
}


ReliamSchedPol::~ReliamSchedPol() {

}



#ifdef CONFIG_BBQUE_LINUX_PROC_MANAGER

SchedulerPolicyIF::ExitCode_t
ReliamSchedPol::AssignProcessWorkingMode(ProcPtr_t proc)
{
	if (proc == nullptr) {
		logger->Error("AssignWorkingMode: null process descriptor!");
		return SCHED_ERROR;
	}
	
	if ( !assigned_quota.count(proc) )
    {
        return SCHED_SKIP_APP;
    }
    
	// Build a new working mode featuring assigned resources
    auto pawm = std::make_shared<ba::WorkingMode>(
        20, "Adaptation", 1, proc);

	// Resource request addition
	std::string pe_request("sys.cpu.pe");
	logger->Debug("AssignWorkingMode: [%s] adding resource request <%s>",
		proc->StrId(), pe_request.c_str());
	pawm->AddResourceRequest(
				pe_request,
				assigned_quota[proc],
				br::ResourceAssignment::Policy::SEQUENTIAL);

	// Look for the first available CPU
	ProcessManager & prm(ProcessManager::GetInstance());
	BindingManager & bdm(BindingManager::GetInstance());
	BindingMap_t & bindings(bdm.GetBindingDomains());
	auto & cpu_ids(bindings[br::ResourceType::CPU]->r_ids);
    logger->Debug("AssignWorkingMode: [%s] nr. CPU ids: %d",
		proc->StrId(), cpu_ids.size());

    ba::WorkingModeStatusIF::ExitCode_t awm_ret;
	for (BBQUE_RID_TYPE cpu_id : cpu_ids) {
		logger->Info("AssingWorkingMode: [%s] binding attempt CPU id = %d",
			proc->StrId(), cpu_id);

		// CPU binding
		awm_ret = pawm->BindResource(br::ResourceType::CPU, R_ID_ANY, cpu_id, br::ResourceType::PROC_ELEMENT, &pes);
        if (awm_ret != ba::WorkingModeStatusIF::WM_SUCCESS) {
			logger->Error("AssingWorkingMode: [%s] CPU binding to < %d > failed",
				proc->StrId(), cpu_id);
			continue;
		}

		// Schedule request
		auto prm_ret = prm.ScheduleRequest(proc, pawm, sched_status_view);
		if (prm_ret != ProcessManager::SUCCESS) {
			logger->Error("AssignWorkingMode: schedule request failed for [%s]", proc->StrId());
			continue;
		}

		return SCHED_OK;
	}

	return SCHED_ERROR;
}

#endif // CONFIG_BBQUE_LINUX_PROC_MANAGER

SchedulerPolicyIF::ExitCode_t ReliamSchedPol::_Init() {
    
    proc_elements = sys->GetResources("sys.cpu.pe");
    logger->Info("Init: %d processing elements available", proc_elements.size());
    if (proc_elements.empty()) {
		logger->Crit("Init: not available CPU cores!");
		return SCHED_R_UNAVAILABLE;
	}
	
    // Applications count
    nr_run_apps = sys->SchedulablesCount(ba::Schedulable::RUNNING);
    nr_thaw_apps = sys->SchedulablesCount(ba::Schedulable::THAWED);
    nr_not_run_apps = sys->SchedulablesCount(ba::Schedulable::READY);
    nr_not_run_apps += sys->SchedulablesCount(ba::Schedulable::RESTORING);
    
    nr_apps = nr_not_run_apps + nr_run_apps + nr_thaw_apps;
    logger->Info("Init: nr. active applications = %d", nr_apps);
    
    available_cpu = ra.Total("sys.cpu.pe");
    logger->Info("Init: available cpu = %d", available_cpu);

#ifdef CONFIG_TARGET_OPENCL
	if (this->acc_opencl_list.empty()) {
		this->acc_opencl_list = sys->GetResources("sys.grp.acc.pe");
		logger->Info("Init: %d OpenCL GPU(s) available", acc_opencl_list.size());
	}
#endif

	return SCHED_OK;
}

SchedulerPolicyIF::ExitCode_t ReliamSchedPol::ManageReliabilityCriticalResources()
{
    uint32_t pe_temp, pe_max_temp;
    PowerManager & pm(PowerManager::GetInstance());
    nr_pe_to_idle = 0;
    for ( auto pe : proc_elements )
    {
        pm.GetTemperature(pe->Path(), pe_temp);
        pm.GetMaxTemperature(pe->Path(), pe_max_temp);
        
        if ( pe_temp > pe_max_temp )
        {
            proc_elements_to_idle[pe->Path()] = pe_temp;
            nr_pe_to_idle++;
        }
    }
    
    ReliabilityManager & lm(ReliabilityManager::GetInstance());
    nr_pe_activated_from_idle = lm.GetNrIdleProcessingElements() - nr_pe_to_idle;
    lm.StoreNrIdleProcessingElements(nr_pe_to_idle);
    
    available_cpu -= nr_pe_to_idle*100;
    if ( available_cpu < MIN_QUOTA ){
		logger->Crit("ManageReliabilityCriticalResources: not available CPU cores!");
		return SCHED_R_UNAVAILABLE;
	}

    return SCHED_OK;
}

SchedulerPolicyIF::ExitCode_t ReliamSchedPol::ThawFrozenExecutions(app::SchedPtr_t psched){
    
    
    auto pid_state = psched->GetPIDControllerState();
    uint64_t assigned_quota = pid_state.actual_output;
    
    if ( nr_pe_activated_from_idle > 0 )
    {
        if ( assigned_quota/100 > nr_pe_activated_from_idle )
        {
            assigned_quota = nr_pe_activated_from_idle*100;
        }
        
        nr_pe_activated_from_idle -= assigned_quota/100;
        PlatformManager & plm(PlatformManager::GetInstance());
        plm.Thaw(psched);
    }
    
    return SCHED_OK;
}

SchedulerPolicyIF::ExitCode_t
ReliamSchedPol::AssignApplicationWorkingMode(app::AppCPtr_t papp)
{

	if (papp == nullptr) {
		logger->Error("AssignWorkingMode: null application descriptor!");
		return SCHED_ERROR;
	}
	
	if ( !assigned_quota.count(papp) )
	{
        	return SCHED_SKIP_APP;
    	}

    	auto pawm = std::make_shared<ba::WorkingMode>(20, "Adaptation", 1, papp);

	// Resource request addition
	std::string pe_request("sys.cpu0.pe");
	logger->Debug("AssignWorkingMode: [%s] adding resource request <%s>", papp->StrId(), pe_request.c_str());
	pawm->AddResourceRequest(pe_request,
				assigned_quota[papp],
				br::ResourceAssignment::Policy::SEQUENTIAL);

#ifdef CONFIG_TARGET_OPENCL
	// OpenCL applications are under a different resource path
	if (!acc_opencl_list.empty() && (papp->Language() & RTLIB_LANG_OPENCL)) {
		logger->Debug("AddResourceRequests: [%s] adding resource request <%s>",
				papp->StrId(), "sys.grp0.acc.pe");

		pawm->AddResourceRequest("sys.grp0.acc.pe", 100);
		this->BindToFirstAvailableOpenCL(pawm, br::ResourceType::ACCELERATOR, 100);
	}
#endif

	// Look for the first available CPU
  	ApplicationManager & am(ApplicationManager::GetInstance());
	BindingManager & bdm(BindingManager::GetInstance());
	BindingMap_t & bindings(bdm.GetBindingDomains());
	auto & cpu_ids(bindings[br::ResourceType::CPU]->r_ids);
	logger->Debug("AssignWorkingMode: [%s] nr. CPU ids: %d",
		papp->StrId(), cpu_ids.size());

    	ba::WorkingModeStatusIF::ExitCode_t awm_ret;
	for (BBQUE_RID_TYPE cpu_id : cpu_ids) {
		logger->Info("AssingWorkingMode: [%s] binding attempt CPU id = %d",
			papp->StrId(), cpu_id);

		// CPU binding
       		// awm_ret = pawm->BindResource(br::ResourceType::CPU, R_ID_ANY, cpu_id, br::ResourceType::PROC_ELEMENT, &pes);
        	awm_ret = pawm->BindResource(br::ResourceType::CPU, R_ID_ANY, cpu_id);

        	if (awm_ret != ba::WorkingModeStatusIF::WM_SUCCESS) {
			logger->Error("AssingWorkingMode: [%s] CPU binding to < %d > failed",
				papp->StrId(), cpu_id);
			continue;
		}
		logger->Debug("AssingWorkingMode: [%s] CPU binding to < %d > success", papp->StrId(), cpu_id);
		ApplicationManager::ExitCode_t ret = am.ScheduleRequest(papp, pawm, sched_status_view);
        	if (ret != ApplicationManager::AM_SUCCESS) {
			logger->Error("AssignWorkingMode: [%s] schedule request failed",
            		papp->StrId());
        		continue;
        	}
		
		return SCHED_OK;
	}

	return SCHED_ERROR;
}

SchedulerPolicyIF::ExitCode_t
ReliamSchedPol::AssignWorkingMode(app::SchedPtr_t psched)
{
#ifdef CONFIG_BBQUE_LINUX_PROC_MANAGER
    if (psched->GetType() == ba::Schedulable::Type::PROCESS)
    {
        return ReliamSchedPol::AssignProcessWorkingMode(std::dynamic_pointer_cast<app::Process>(psched));
    }
#endif
    return ReliamSchedPol::AssignApplicationWorkingMode(std::dynamic_pointer_cast<app::Application>(psched));
    
}

void ReliamSchedPol::QuotaLimiter(){
    int total_allocated_quota = abs(available_cpu) + ra.Total("sys.cpu.pe");
    uint64_t quota;
    double x = (ra.Total("sys.cpu.pe") - nr_pe_to_idle*100)/total_allocated_quota;
    
    for ( auto& app : assigned_quota )
        {
            quota = app.second;
	    double quota_lim = quota*x;
	    app.second = quota_lim;
	    auto pid_state = (app.first)->GetPIDControllerState();
	    logger->Debug("QuotaLimiter: <%s> new quota -> %d", app.first->StrId(), app.second);
            PIDController::UpdateState(pid_state, quota_lim);
            (app.first)->SetPIDControllerState(pid_state);

        }
}
    
    

void ReliamSchedPol::InitResourceBitset()
{    
    int64_t nr_idle;
    logger->Debug("InitResourceBitset: remaining not allocated CPU quota -> %d", available_cpu);
    if (available_cpu < 0)
    {
        nr_idle = 0;
        ReliamSchedPol::QuotaLimiter();
        //TODO frequency scaling
    }
    nr_idle = available_cpu/100;
    
    proc_elements.sort(bbque::res::CompareTemperature);
    
    auto end = std::next(proc_elements.begin(), proc_elements.size() - nr_idle);
    for (auto it=proc_elements.begin(); it!=end; ++it)
        pes.Set((*it)->ID());
}

bool ReliamSchedPol::IsSchedulableBoundToCriticalProcElements(br::ResourceAssignmentMapPtr_t const assign_map){
    
    std::list<br::ResourcePathPtr_t> crit_proc_elements_paths;
    
    for (auto & am_entry : * (assign_map.get())) {
		br::ResourcePathPtr_t const & rsrc_path(am_entry.first);
		br::ResourceAssignmentPtr_t & r_assign(am_entry.second);
        if ( r_assign->GetAmount() == 100 )
        {
            if ( !proc_elements_to_idle.count(rsrc_path) )
                return false;
            else
                crit_proc_elements_paths.push_back(rsrc_path);
        }
        else
            return false;
    }
    
    return true;
}

SchedulerPolicyIF::ExitCode_t ReliamSchedPol::ComputeQuota(app::SchedPtr_t psched)
{
    logger->Info("ComputeQuota: <%s> computing quota", psched->StrId());
     
    uint64_t prev_used;

	if (psched->GetType() == ba::Schedulable::Type::PROCESS)
	{
		auto prof = std::dynamic_pointer_cast<bbque::app::Process>(psched)->GetRuntimeProfile();
		prev_used = prof.cpu_usage.curr;
	}
	else {
		auto prof = std::dynamic_pointer_cast<bbque::app::Application>(psched)->GetRuntimeProfile();
		prev_used = prof.cpu_usage.curr;
	}

	uint64_t prev_quota  = ra.UsedBy(
		"sys.cpu.pe",
		psched,
		0);

    int64_t prev_delta = prev_quota - prev_used;

    int64_t next_quota;
#ifdef CONFIG_BBQUE_CHECKPOINT_SCHEDULER
    if ( psched->IsDumping())
    {
        logger->Debug("ComputeQuota: <%s> dump in progress, assign same resources as previous", psched->StrId());
        
        next_quota = prev_quota;
        available_cpu -= next_quota;
        assigned_quota[psched] = next_quota;
        return SCHED_OK;
    }
#endif
    if (psched->Running()){
		auto assign_map = psched->CurrentAWM()->GetResourceBinding();
		if ( IsSchedulableBoundToCriticalProcElements(assign_map) )
		{
			logger->Info("ComputeQuota: <%s> bound to temperature critical processing elements, freezing schedulable for reliability issues.",
						 psched->StrId());

			PlatformManager & plm(PlatformManager::GetInstance());
			plm.Freeze(psched);

			return SCHED_OK;
 }
    }

    if (prev_delta < 0){
        prev_delta = 0;
    }

    logger->Info("ComputeQuota: <%s> Previous settings -> quota=%d, cpu_usage=%d, delta=%d available_cpu=%d",
            psched->StrId(),
            prev_quota,
            prev_used,
            prev_delta,
            available_cpu);

    int64_t pre_band_error = OVERPROVISIONING - prev_delta;

    auto pid_state = psched->GetPIDControllerState();

    next_quota = PIDController::UpdateState(pid_state, pre_band_error, 4, DEAD_BAND_CUT_OFF_VALUE);
    logger->Debug("ComputeQuota: <%s> next controller state -> initial integral=%d, actual output=%d, desired output=%d, prev error=%d, integral=%f", psched->StrId(), pid_state.initial_integral, pid_state.actual_output, pid_state.desired_output, pid_state.prev_error, pid_state.integral);
    if (next_quota < MIN_QUOTA){
    	PIDController::Reset(pid_state);
	next_quota = PIDController::UpdateState(pid_state, DEAD_BAND_CUT_OFF_VALUE, 4, DEAD_BAND_CUT_OFF_VALUE);
	logger->Debug("ComputeQuota: <%s> next controller state -> initial integral=%d, actual output=%d, desired output=%d, prev error=%d, integral=%f", psched->StrId(), pid_state.initial_integral, pid_state.actual_output, pid_state.desired_output, pid_state.prev_error, pid_state.integral);

	}
    psched->SetPIDControllerState(pid_state);


    if ((psched->Ready() || psched->Restoring()) && available_cpu <= next_quota){
 		logger->Debug("ComputeQuota: not enough resources to schedule <%s>, skipped.");
 		return SCHED_SKIP_APP;
    }

    available_cpu -= next_quota;

    logger->Notice("ComputeQuota: <%s> New settings -> quota=%d, available_cpu=%d",
            psched->StrId(),
            next_quota,
            available_cpu);

    assigned_quota[psched] = next_quota;

    
    return SCHED_OK;
    
}

SchedulerPolicyIF::ExitCode_t
ReliamSchedPol::BindToFirstAvailableOpenCL(bbque::app::AwmPtr_t pawm,
					 br::ResourceType dev_type,
					 uint64_t amount)
{
	uint32_t opencl_platform_id;
	br::ResourceBitset opencl_devs_bitset;
	logger->Debug("BindToFirstAvailableOpenCL: looking for an available OpenCL accelerator...");

	for (auto const & acc : this->acc_opencl_list) {
		auto acc_avail_amount = acc->Available(nullptr, this->sched_status_view);
		logger->Debug("BindToFirstAvailableOpenCL: group=%d acc_id=%d available=%lu",
			acc->Path()->GetID(br::ResourceType::GROUP),
			acc->Path()->GetID(br::ResourceType::ACCELERATOR),
			acc_avail_amount);
		if (amount > acc_avail_amount) {
			continue;
		}

		opencl_platform_id = acc->Path()->GetID(br::ResourceType::GROUP);
		opencl_devs_bitset.Set(acc->Path()->GetID(dev_type));
		logger->Debug("BindToFirstAvailableOpenCL: accelerator(s) bitset=%s",
			opencl_devs_bitset.ToString().c_str());

		auto ret = pawm->BindResource(br::ResourceType::GROUP,
					R_ID_ANY,
					opencl_platform_id,
					dev_type,
					&opencl_devs_bitset);

		if (ret != ba::WorkingModeStatusIF::ExitCode_t::WM_SUCCESS) {
			logger->Error("BindToFirstAvailableOpenCL: <%s> failed",
				pawm->StrId());
			return ExitCode_t::SCHED_ERROR;
		}
	}

	return ExitCode_t::SCHED_R_UNAVAILABLE;
}

SchedulerPolicyIF::ExitCode_t 
ReliamSchedPol::ForEachApplicationToScheduleDo(std::function <ExitCode_t(bbque::app::AppCPtr_t) > do_func)
{
	AppsUidMapIt app_it;
	ba::AppCPtr_t app_ptr = sys->GetFirstRunning(app_it);
	for (; app_ptr; app_ptr = sys->GetNextRunning(app_it)) {
		do_func(app_ptr);
	}
	ForEachApplicationNotRunningDo(do_func);
	return SCHED_OK;
}

SchedulerPolicyIF::ExitCode_t
ReliamSchedPol::ForEachFrozenSchedulableDo(std::function
<SchedulerPolicyIF::ExitCode_t(app::SchedPtr_t) > do_func)
{
    ApplicationManager & am(ApplicationManager::GetInstance());
    AppsUidMapIt app_it;
    ba::AppCPtr_t app_ptr;
    ProcessMapIterator proc_it;
    ProcPtr_t proc_ptr;
    
    app_ptr = am.GetFirst(ba::Schedulable::State_t::FROZEN, app_it);
    for (; app_ptr; app_ptr = am.GetNext(ba::Schedulable::State_t::FROZEN, app_it)) {
        do_func(app_ptr);
    }
    
    proc_ptr = prm.GetFirst(ba::Schedulable::FROZEN, proc_it);
    for (; proc_ptr; proc_ptr = prm.GetNext(ba::Schedulable::FROZEN, proc_it)) {
            do_func(proc_ptr);
    }
    
    return SCHED_OK;
}

#ifdef CONFIG_BBQUE_LINUX_PROC_MANAGER
SchedulerPolicyIF::ExitCode_t
ReliamSchedPol::ForEachProcessToScheduleDo(std::function <ExitCode_t(ProcPtr_t) > do_func)
{
	ProcessManager & prm(ProcessManager::GetInstance());
	ProcessMapIterator proc_it;
	ProcPtr_t proc = prm.GetFirst(ba::Schedulable::RUNNING, proc_it);
	for (; proc; proc = prm.GetNext(ba::Schedulable::RUNNING, proc_it)) {
		do_func(proc);
	}
	ForEachProcessNotRunningDo(do_func);


	return SCHED_OK;
}
#endif


SchedulerPolicyIF::ExitCode_t
ReliamSchedPol::Schedule(
		System & system,
		RViewToken_t & status_view) {

	// Class providing query functions for applications and resources
	sys = &system;

	// Initialization
	auto result = Init();
	if (result != SCHED_OK) {
		logger->Fatal("Schedule: initialization failed");
		return SCHED_ERROR;
	} else {
		logger->Debug("Schedule: resource status view = %ld",
			sched_status_view);	
	}
	
	result = ManageReliabilityCriticalResources();

	auto thaw_exec = std::bind(
        static_cast<SchedulerPolicyIF::ExitCode_t (ReliamSchedPol::*)(app::SchedPtr_t)>
        (&ReliamSchedPol::ThawFrozenExecutions),
        this, _1);
    
	ForEachFrozenSchedulableDo(thaw_exec);
    
	auto compute_quota = std::bind(
        static_cast<SchedulerPolicyIF::ExitCode_t (ReliamSchedPol::*)(app::SchedPtr_t)>
        (&ReliamSchedPol::ComputeQuota),
        this, _1);
    
	ReliamSchedPol::ForEachApplicationToScheduleDo(compute_quota);
#ifdef CONFIG_BBQUE_LINUX_PROC_MANAGER
	ReliamSchedPol::ForEachProcessToScheduleDo(compute_quota);
#endif

	ReliamSchedPol::InitResourceBitset();
    
	auto assign_awm = std::bind(static_cast<ExitCode_t (ReliamSchedPol::*)(app::SchedPtr_t psched)>
					(&ReliamSchedPol::AssignWorkingMode),
					this, _1);

	ForEachApplicationToScheduleDo(assign_awm);
	logger->Debug("Schedule: done with applications.");

#ifdef CONFIG_BBQUE_LINUX_PROC_MANAGER

	ForEachProcessToScheduleDo(assign_awm);
	logger->Debug("Schedule: done with processes.");

#endif // CONFIG_BBQUE_LINUX_PROC_MANAGER

	// Iterate over all the applications to schedule
	if (result != SCHED_OK) {
		logger->Debug("Schedule: not SCHED_OK return [result=%d]", result);
		return result;
	}
	logger->Debug("Schedule: done.");

	// Return the new resource status view according to the new resource
	// allocation performed
	status_view = sched_status_view;


	return SCHED_DONE;
}


} // namespace plugins

} // namespace bbque
