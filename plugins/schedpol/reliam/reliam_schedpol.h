/*
 * Copyright (C) 2016  Politecnico di Milano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BBQUE_RELIAM_SCHEDPOL_H_
#define BBQUE_RELIAM_SCHEDPOL_H_

#include <cstdint>
#include <list>
#include <memory>

#include "bbque/configuration_manager.h"
#include "bbque/plugins/plugin.h"
#include "bbque/plugins/scheduler_policy.h"
#include "bbque/scheduler_manager.h"
#include "bbque/reliability_manager.h"
#include "bbque/process_manager.h"

#define SCHEDULER_POLICY_NAME "reliam"

#define MODULE_NAMESPACE SCHEDULER_POLICY_NAMESPACE "." SCHEDULER_POLICY_NAME

using bbque::res::RViewToken_t;
using bbque::utils::MetricsCollector;
using bbque::utils::Timer;

// These are the parameters received by the PluginManager on create calls
struct PF_ObjectParams;

namespace bbque { namespace plugins {
    
class LoggerIF;

/**
 * @class ReliamSchedPol
 *
 * Reliam scheduler policy registered as a dynamic C++ plugin.
 */
class ReliamSchedPol: public SchedulerPolicyIF {

public:

	// :::::::::::::::::::::: Static plugin interface :::::::::::::::::::::::::

	/**
	 * @brief Create the reliam plugin
	 */
	static void * Create(PF_ObjectParams *);

	/**
	 * @brief Destroy the reliam plugin 
	 */
	static int32_t Destroy(void *);


	// :::::::::::::::::: Scheduler policy module interface :::::::::::::::::::

	/**
	 * @brief Destructor
	 */
	virtual ~ReliamSchedPol();

	/**
	 * @brief Return the name of the policy plugin
	 */
	char const * Name();


	/**
	 * @brief The member function called by the SchedulerManager to perform a
	 * new scheduling / resource allocation
	 */
	ExitCode_t Schedule(System & system, RViewToken_t & status_view);
	std::unique_ptr<bu::Logger> logger;
private:

	/** Configuration manager instance */
	ConfigurationManager & cm;

	/** Resource accounter instance */
	ResourceAccounter & ra;
#ifdef CONFIG_BBQUE_LINUX_PROC_MANAGER
    /** Process Manager instance */
    	ProcessManager & prm;
#endif

	/** System logger instance */

    
    br::ResourceBitset pes;
    res::ResourcePtrList_t proc_elements;
    std::map<res::ResourcePathPtr_t, uint32_t> proc_elements_to_idle;
    std::map<res::ResourcePtr_t, uint32_t> pe_temp;
    std::set<BBQUE_RID_TYPE> pe_ids;
    int active_pe_count;
    
    uint32_t nr_apps;
    uint32_t nr_run_apps;
    uint32_t nr_thaw_apps;
    uint32_t nr_not_run_apps;
    uint64_t quota_not_run_apps;
    
    int64_t available_cpu;
    int64_t nr_pe_to_idle;
    int64_t nr_pe_activated_from_idle;
	br::ResourcePtrList_t acc_opencl_list;
    
    int64_t neg_delta;
    
    std::map<app::SchedPtr_t, uint64_t> assigned_quota;

	/**
	 * @brief Constructor
	 *
	 * Plugins objects could be build only by using the "create" method.
	 * Usually the PluginManager acts as object
	 */
	ReliamSchedPol();

	/**
	 * @brief Optional initialization member function
	 */
	ExitCode_t _Init();
#ifdef CONFIG_BBQUE_LINUX_PROC_MANAGER

	ExitCode_t AssignProcessWorkingMode(ProcPtr_t proc);

	ExitCode_t ForEachProcessToScheduleDo(std::function <ExitCode_t(ProcPtr_t) > do_func);


#endif // CONFIG_BBQUE_LINUX_PROC_MANAGER
	ExitCode_t ForEachApplicationToScheduleDo(std::function <ExitCode_t(bbque::app::AppCPtr_t) > do_func);

	ExitCode_t ForEachFrozenSchedulableDo(std::function<SchedulerPolicyIF::ExitCode_t(app::SchedPtr_t) > do_func);

	ExitCode_t ComputeQuota(app::SchedPtr_t psched);

	bool IsSchedulableBoundToCriticalProcElements(br::ResourceAssignmentMapPtr_t const assign_map);
    
	void QuotaLimiter();
    
	void InitResourceBitset();
    
	ExitCode_t AssignWorkingMode(app::SchedPtr_t psched);
    
	ExitCode_t AssignApplicationWorkingMode(app::AppCPtr_t papp);

	ExitCode_t ManageReliabilityCriticalResources();
    
	ExitCode_t ThawFrozenExecutions(app::SchedPtr_t psched);
    
	ExitCode_t BindToFirstAvailableOpenCL(bbque::app::AwmPtr_t pawm,
					br::ResourceType r_type,
					uint64_t amount);

};

} // namespace plugins

} // namespace bbque

#endif // BBQUE_RELIAM_SCHEDPOL_H_
