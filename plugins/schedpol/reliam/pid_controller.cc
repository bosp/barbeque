/*
 * Copyright (C) 2012  Politecnico di Milano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "pid_controller.h"

#include <math.h>



int PIDController::UpdateState(PIDState_t &state, int curr_error, int invocation_period, int cutoff_value){
    	if (cutoff_value)
    		curr_error = ApplyDeadBand(curr_error,cutoff_value);
    	// Proportional part
    	double proportional = state.kp * curr_error;

    	// Integral part
    	double backpropagation = state.actual_output - state.desired_output;
    	double integral_increment = state.ki * curr_error + backpropagation;
    	double integral = state.integral + integral_increment * invocation_period;

    	// Derivative part
    	double derivative = state.kd * (curr_error - state.prev_error) / invocation_period;

    	// State update
    	state.integral = integral;
    	state.prev_error = curr_error;

    	state.desired_output = proportional + state.integral + derivative;
    	state.actual_output = state.desired_output;

    	return state.desired_output;
}

double PIDController::ApplyDeadBand(int x, int cutoff_value){
    	if (abs(x) <= cutoff_value)
    		return 0;
    	return x > 0 ? x - cutoff_value : x + cutoff_value;
}

void PIDController::Reset(PIDState_t &state){
    	state.actual_output = 0;
    	state.desired_output = 0;
    	state.prev_error = 0;
    	state.integral = state.initial_integral;
}
