/*
 * Copyright (C) 2012  Politecnico di Milano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef BBQUE_PID_CONTROLLER_H
#define BBQUE_PID_CONTROLLER_H

#include "bbque/app/schedulable.h"


/**
 * @class PIDController
 *
 * @brief PID controller parameters and contributions, currently used
 * by Reliam_SchedPol for the computation of the quota.
 */

class PIDController {
public:

    static void UpdateState(PIDState_t &state, int actual_output){
    	state.actual_output = actual_output;
    }

    static int UpdateState(PIDState_t &state, int curr_error, int invocation_period, int cutoff_value);

    static double ApplyDeadBand(int x, int cutoff_value);

    static void Reset(PIDState_t &state);

};

#endif //BBQUE_PID_CONTROLLER_H

