/*
 * Copyright (C) 2016  Politecnico di Milano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "distime_schedpol.h"

#include <algorithm>
#include <cstdlib>
#include <cstdint>
#include <iostream>
#include <type_traits>


#include "bbque/modules_factory.h"
#include "bbque/utils/logging/logger.h"

#include "bbque/app/working_mode.h"
#include "bbque/res/binder.h"

#define MODULE_CONFIG SCHEDULER_POLICY_CONFIG "." SCHEDULER_POLICY_NAME

namespace bu = bbque::utils;
namespace po = boost::program_options;

namespace bbque { namespace plugins {

// :::::::::::::::::::::: Static plugin interface ::::::::::::::::::::::::::::

void * DisTimeSchedPol::Create(PF_ObjectParams *) {
	return new DisTimeSchedPol();
}

int32_t DisTimeSchedPol::Destroy(void * plugin) {
	if (!plugin)
		return -1;
	delete (DisTimeSchedPol *)plugin;
	return 0;
}

// ::::::::::::::::::::: Scheduler policy module interface :::::::::::::::::::

char const * DisTimeSchedPol::Name() {
	return SCHEDULER_POLICY_NAME;
}

DisTimeSchedPol::DisTimeSchedPol():
		cm(ConfigurationManager::GetInstance()),
		ra(ResourceAccounter::GetInstance()) {
	logger = bu::Logger::GetLogger(MODULE_NAMESPACE);
	assert(logger);
	if (logger)
		logger->Info("distime: Built a new dynamic object[%p]", this);
	else
		fprintf(stderr,
			FI("distime: Built new dynamic object [%p]\n"), (void *)this);
}


DisTimeSchedPol::~DisTimeSchedPol() {

}


SchedulerPolicyIF::ExitCode_t DisTimeSchedPol::_Init() {
	ba::AppCPtr_t papp;
	AppsUidMapIt app_it;
	//Check initial resource availability
	UpdateResourcesAvailability();

	// Count the applications to schedule
	nr_apps_to_schedule  = sys->ApplicationsCount(ba::Schedulable::READY);
	nr_apps_to_schedule += sys->ApplicationsCount(ba::Schedulable::THAWED);
	nr_apps_to_schedule += sys->ApplicationsCount(ba::Schedulable::RESTORING);
	nr_apps_to_schedule += sys->ApplicationsCount(ba::Schedulable::RUNNING);
	logger->Debug("Init: number of applications to schedule = %d", this->nr_apps_to_schedule);

	//sort sched_entity_list based on application_state
	sched_entity_list.sort(SchedEntity::CompareWcetEstimationState);
	for (auto & sched_entity : sched_entity_list) {
		logger->Debug("%s AWM = %s",
			sched_entity.papp->StrId(),
			sched_entity.pawm->StrId());
	}
	return SCHED_OK;
}

void DisTimeSchedPol::UpdateResourcesAvailability()
{
	for(auto const & cpu_pe : sys->GetResources("sys.cpu.pe")){
		auto const & cpu_path = cpu_pe->Path();
		resources_list[cpu_path]=sys->ResourceAvailable(cpu_path,
									this->sched_status_view);
		logger->Debug("UpdateResourcesAvailability: <%s> = %lu",
			cpu_path->ToString().c_str(),
			this->resources_list[cpu_path]);
	}
	
	for(auto const & gpu_pe : sys->GetResources("sys.gpu.pe")){
		auto const & gpu_path = gpu_pe->Path();
		resources_list[gpu_path]=sys->ResourceAvailable(gpu_path,
									this->sched_status_view);
		logger->Debug("UpdateResourcesAvailability: <%s> = %lu",
			gpu_path->ToString().c_str(),
			this->resources_list[gpu_path]);
	}
	
	
}

SchedulerPolicyIF::ExitCode_t
DisTimeSchedPol::Schedule(
		System & system,
		RViewToken_t & status_view) {

	// Class providing query functions for applications and resources
	sys = &system;

	// Initialization
	auto result = Init();
	if (result != SCHED_OK) {
		logger->Fatal("Schedule: initialization failed");
		return SCHED_ERROR;
	} else {
		logger->Debug("Schedule: resource status view = %ld",
			sched_status_view);	
	}

	// Running applications (we already have profiled EpC per working mode)
	//for (auto & sched_entity : sched_entity_list) {
	for(auto psched_entity = sched_entity_list.begin();psched_entity!=sched_entity_list.end();){
		
		logger->Debug("Schedule: considering sched entity: %s AWM= %s",
			psched_entity->papp->StrId(),
			psched_entity->pawm->StrId());
		if (psched_entity->papp->State() == ba::ApplicationStatusIF::SYNC) {
			logger->Debug("Schedule: %s AWM=%s already scheduled",
				psched_entity->papp->StrId(),
				psched_entity->pawm->StrId());
			psched_entity++;
			continue;
		}

		// If the application is in monitoring state, then performs the
		// monitoring algorithm
		if(psched_entity->papp->GetWcetEstimationState() == app::Application::WcetEstimationState::MONITORING) {
			if(!PerformMonitoring(psched_entity->papp)){
				logger->Debug("Schedule: %s was in MONITORING state, now back to PROFILING state",
							psched_entity->papp->StrId());
				//delete awms for app that return in PROFILING state
				auto next_awm=ExploreInitialWorkingMode(psched_entity->papp);
				if(!ScheduleApplication(psched_entity->papp,next_awm)){
					next_awm=psched_entity->pawm;
				}
				psched_entity->papp->ClearAWMMap(next_awm->Id());
				psched_entity->papp->ClearTimingSamples();
				logger->Debug("Schedule: %s AWMs and timing info deleted %d",
							psched_entity->papp->StrId(),
							psched_entity->papp->GetRuntimeWorkingModeCount());
				se_rejected_list.emplace_back(psched_entity->papp->Uid(),next_awm->Id());
				psched_entity++;
				
			}
			else{
				logger->Debug("Schedule: %s is in MONITORING state, AWM=[%s]", 
					psched_entity->papp->StrId(),
					psched_entity->pawm->StrId());
				ScheduleApplication(psched_entity->papp,psched_entity->pawm);
				psched_entity++;
			}
			
		}
		else{
			logger->Debug("Schedule: %s is in PROFILING state, AWM=[%s]", 
					psched_entity->papp->StrId(),
					psched_entity->pawm->StrId());
			if(ScheduleRunningApplication(*psched_entity)==SCHED_UNRESPONSIVE){
				logger->Debug("Schedule: %s AWM is not responsive. Deleting %s",
					psched_entity->papp->StrId(),
					psched_entity->pawm->StrId());
				psched_entity=sched_entity_list.erase(psched_entity);
				
			}
			else{
				psched_entity++;
			}
		}

	}

	// Not running applications
	auto sched_not_running_app = std::bind(
					static_cast<ExitCode_t (DisTimeSchedPol::*)(ba::AppCPtr_t)>
					(&DisTimeSchedPol::ScheduleNotRunningApplication),
					this, std::placeholders::_1);

	ForEachApplicationNotRunningDo(sched_not_running_app);
	if (result != SCHED_OK) {
		logger->Debug("Schedule: not SCHED_OK return [result=%d]", result);
		return result;
	}

	sched_entity_list.splice(sched_entity_list.end(),new_se_list);

	// Erasing explored sched_entities for apps passed to MONITORING mode
	for(const auto & p : se_promoted_list){
		for(auto i = sched_entity_list.begin();i!=sched_entity_list.end();){
			if((i->papp->Uid() == p.first) && (i->pawm->Id() != p.second)){
				logger->Debug("Schedule: %s AWM deleted %s after PROFILING->MONITORING transition",
							i->papp->StrId(),
							i->pawm->StrId());
					i=sched_entity_list.erase(i);
			}
			else{
				i++;
			}

		}
	}
	se_promoted_list.clear();
	// Erasing explored sched_entities for apps get back to PROFILING mode
	for(const auto & p : se_rejected_list){
		for(auto i = sched_entity_list.begin();i!=sched_entity_list.end();){
			if((i->papp->Uid() == p.first) && (i->pawm->Id() != p.second)){
				logger->Debug("Schedule: %s AWM deleted %s after MONITORING->PROFILING transition",
							i->papp->StrId(),
							i->pawm->StrId());
					i=sched_entity_list.erase(i);	
			}
			else{
				i++;
			}

		}
	}
	se_rejected_list.clear();

	// TODO: What if we end up here with some failed scheduling attempts?

	logger->Debug("Schedule: done with applications");

	// Return the new resource status view according to the new resource
	// allocation performed
	status_view = sched_status_view;

	return SCHED_DONE;
}

static double KS_empirical_F(const std::list<int> &cycle_times, double x) {
	// From https://github.com/federeghe/chronovise/blob/master/src/statistical/test_ks.cpp#L18
	const auto n = cycle_times.size();
	const auto minnum   = std::min_element(cycle_times.begin(), cycle_times.end());

	double curr_point = *minnum;
	auto list_iterator = cycle_times.begin();
	
	std::remove_const<decltype(n)>::type i=0;
	while (curr_point <= x) {
		i++;
		if (i >= n) break;
		curr_point = *list_iterator;
		list_iterator++;
	}
	
	double cumulative_F = ((double)i) / n;
	assert(cumulative_F >= 0. && cumulative_F <= 1.);
    return cumulative_F;
}


static double gpd_cdf(double x, double mu, double sg, double xi) noexcept {

    // Check the support limits
    if (xi >= 0. && x < mu)
        return 0.;
    if (xi < 0.  && (x < mu || x > mu - sg / xi))
        return 0.;

    const double norm_x = (x-mu)/sg;
    double cond_value = 1. + xi * norm_x;

    double cdf = 0.;

    if (cond_value >= 0. && xi != 0.) {
        cdf = 1. - std::pow(cond_value, - 1. / xi);
    }
    else if (x >= 0. && xi == 0.) {
        cdf = 1. - std::exp(-norm_x);
    }
    
    assert(cdf >= 0. && cdf <= 1. && "Something bad happened in calculation.");
    return cdf;
}

bool DisTimeSchedPol::perform_KS_test(const std::list<int> &cycle_times, double mu, double sg, double xi) {
    constexpr double alpha = 0.01;
	const auto n = cycle_times.size();
    double critical_value = std::sqrt(-0.5 * std::log(alpha/2))/std::sqrt(n);

    /* **** Compute the empirical F(x) **** */
    const auto minmax_pair = std::minmax_element(cycle_times.begin(), cycle_times.end());
    const double min_meas = *minmax_pair.first;
    const double max_meas = *minmax_pair.second;
    const double step_f   = (max_meas - min_meas) / n;

    double max_statistic = 0.;

    for (double x = min_meas; x <= max_meas; x += step_f) {
        double Fn = KS_empirical_F(cycle_times, x);
        double F = gpd_cdf(x, mu, sg, xi);

        if ( std::abs(F - Fn) > max_statistic) {
            max_statistic = std::abs(F - Fn);
        }
    }
    
    double min_epsilon = gpd_cdf(min_meas - std::numeric_limits<double>::epsilon(), mu, sg, xi);
    double max_epsilon = 1 - gpd_cdf(max_meas - std::numeric_limits<double>::epsilon(), mu, sg, xi);
    
    if ( min_epsilon > max_statistic) {
        max_statistic = min_epsilon;
    }

    if ( max_epsilon > max_statistic) {
        max_statistic = max_epsilon;
    }
	logger->Debug("perform_KS_test: max_statistic=%f critical_value=%f",max_statistic,critical_value);
    return max_statistic <= critical_value;
}


bool
DisTimeSchedPol::PerformMonitoring(ba::AppCPtr_t papp) {
	bbque_assert(papp->GetWcetEstimationState() == app::Application::WcetEstimationState::MONITORING);
	return true;
	constexpr int MONITOR_WINDOW_SIZE = 20;
	auto rt_prof = papp->GetRuntimeProfile();
	for(auto i=rt_prof.wcet.cycle_times.begin();i!=rt_prof.wcet.cycle_times.end();){
		if(*i<=rt_prof.wcet.threshold){
			i=rt_prof.wcet.cycle_times.erase(i);
		}
		else{
			i++;
		}
	}
	int diff_nr_samples = rt_prof.wcet.cycle_times.size() - MONITOR_WINDOW_SIZE;
	
	if (diff_nr_samples < 0) {
		return true;	// Not sufficient samples to run the monitor
	}

	if (diff_nr_samples > 0) {
		logger->Warn("PerformMonitoring: [%s] I'm skipping some samples [nr_samples=%d > window_size=%d]", papp->StrId(), rt_prof.wcet.cycle_times.size(), MONITOR_WINDOW_SIZE);
		logger->Warn("PerformMonitoring: Please consider to increase monitoring window size or policy frequency.");
	}

	bool test_result = perform_KS_test(rt_prof.wcet.cycle_times, rt_prof.wcet.mu, rt_prof.wcet.sigma, rt_prof.wcet.xi);

	if (! test_result) {
		logger->Notice("PerformMonitoring: [%s] pWCET discrepancy detected, switch back to profiling", papp->StrId());
		papp->SetWcetEstimationState(app::Application::WcetEstimationState::PROFILING);
	} else {
		logger->Info("PerformMonitoring: [%s] pWCET matches, continuing monitoring...", papp->StrId());	
	}

	// Remove all elements but keep the last MONITOR_WINDOW_SIZE - 1:
	//papp->ClearTimingSamples(diff_nr_samples+1);
	papp->ClearTimingSamples();

	
	return test_result;
}

SchedulerPolicyIF::ExitCode_t
DisTimeSchedPol::ScheduleApplication(ba::AppCPtr_t papp, ba::AwmPtr_t next_awm)
{
	if (papp == nullptr) {
		logger->Error("ScheduleApplications: null application descriptor!");
		return SCHED_ERROR;
	}

	if (next_awm == nullptr) {
		logger->Error("ScheduleApplications: null working mode!");
		return SCHED_ERROR;
	}

	// Validate the scheduling request
	ApplicationManager & am(ApplicationManager::GetInstance());
	auto am_ret = am.ScheduleRequest(papp, next_awm, sched_status_view);
	if (am_ret != ApplicationManager::AM_SUCCESS) {
		logger->Error("ScheduleApplication: [%s] schedule request failed"
			"[ret=%d]",
			papp->StrId(),
			am_ret);
		return SCHED_ERROR;
	}

	logger->Debug("ScheduleApplication: [%s] status = <%s %s>",
		papp->StrId(),
		ba::Schedulable::stateStr[papp->State()],
		ba::Schedulable::syncStateStr[papp->SyncState()]);

	this->nr_apps_to_schedule--;
	this->UpdateResourcesAvailability();

	if(papp->CurrentAWM() != nullptr) {
		if(papp->CurrentAWM()->Id() != next_awm->Id()){
			new_se_list.emplace_back(papp,next_awm);
		}
	}
	else{
		new_se_list.emplace_back(papp,next_awm);
	}
	return SCHED_OK;
}

SchedulerPolicyIF::ExitCode_t
DisTimeSchedPol::ScheduleNotRunningApplication(bbque::app::AppCPtr_t papp)
{
	logger->Debug("ScheduleNotRunningApplication: %s [%s] looking for an AWM...",
		papp->StrId(), ba::Schedulable::stateStr[papp->State()]);
	
	timeout_map[papp->Uid()]=ScheduleHistory(papp);

	AwmPtr_t next_awm;
	if (papp->State() == ba::Schedulable::READY) {
		next_awm = this->ExploreInitialWorkingMode(papp);
	}
	else {
		next_awm = this->ExploreNewWorkingMode(papp, nullptr); 
	}
	return this->ScheduleApplication(papp, next_awm);
}

SchedulerPolicyIF::ExitCode_t
DisTimeSchedPol::ScheduleRunningApplication(SchedEntity & sched_entity)
{
	logger->Debug("ScheduleRunningApplication: %s starts from AWM=%s",
		sched_entity.papp->StrId(),
		sched_entity.pawm->StrId());
	auto result=SCHED_OK;
	AwmPtr_t next_awm;
	if(sched_entity.papp->UpdateTimingAnalysis()){
		next_awm = this->ExploreNewWorkingMode(sched_entity.papp, sched_entity.pawm);
		
		if (next_awm == nullptr) {
			next_awm=sched_entity.pawm;
			logger->Debug("ScheduleRunningApplication: %s error in resource assignment. Rescheduling with AWM=%s",
				sched_entity.papp->StrId(),
				next_awm->StrId());
		}
		result=this->ScheduleApplication(sched_entity.papp, next_awm);
		if(result){
			sched_entity.papp->ClearTimingSamples();
			logger->Debug("ScheduleRunningApplication: %s scheduled with AWM=%s",
				sched_entity.papp->StrId(), next_awm->StrId());
		}
		
	}
	else{
		next_awm=sched_entity.pawm;
		
		
		if(!timeout_map[sched_entity.papp->Uid()].Update()){
			logger->Debug("ScheduleRunningApplication: %s AWM=%s timed out. Rescheduling with previous AWM",
					sched_entity.papp->StrId(),
					next_awm->StrId());
			logger->Debug("%s",timeout_map[sched_entity.papp->Uid()].ToString().c_str());
			timeout_map[sched_entity.papp->Uid()].Reset();
			result=SCHED_UNRESPONSIVE;
		}
		else{
			logger->Debug("ScheduleRunningApplication: %s UpdateTimingAnalysis failed. Rescheduling with AWM=%s",
					sched_entity.papp->StrId(),
					next_awm->StrId());
			logger->Debug("%s",timeout_map[sched_entity.papp->Uid()].ToString().c_str());
			result=this->ScheduleApplication(sched_entity.papp, next_awm);
		}
		
		
	}

	return result;
}
ba::AwmPtr_t
DisTimeSchedPol::ExploreInitialWorkingMode(ba::AppCPtr_t papp){

	std::list<ResourceAssignmentCandidate> resources_request;
	this->ExploreInitialResourceAssignments(papp, resources_request);
	logger->Debug("ExploreInitialWorkingMode: %s assigning %d resources",
		papp->StrId(), resources_request.size());

	return CreateWorkingMode(papp, resources_request);
}

ba::AwmPtr_t
DisTimeSchedPol::ExploreNewWorkingMode(ba::AppCPtr_t papp, ba::AwmPtr_t pawm){

	logger->Debug("ExploreNewWorkingMode: %s looking for a new AWM",
		papp->StrId());

	// Given the resource assignments of the starting AWM explore a new
	// resources assignment solution
	std::list<ResourceAssignmentCandidate> resources_request;

	this->ExploreNewResourceAssignment(papp, resources_request);
	// Create the new AWM resulting from the exploration
	logger->Debug("ExploreNewWorkingMode: %s new AWM will include %d resources",
		papp->StrId(), resources_request.size());

	if (resources_request.empty()) {
		logger->Debug("ExploreInitialWorkingMode: %s re-assigning [%s]",
			papp->StrId(), pawm->StrId());
		return pawm;
	}
	else{
		
		auto wm = this->FindWorkingModeByResources(papp, resources_request);
		if(wm!=nullptr){
			logger->Debug("ExploreInitialWorkingMode: %s AWM already exists. Re-assigning previous AWM [%s]",
			papp->StrId(),wm->StrId());
			//Switch to monitoring mode and delete all explored AWMs
			//TODO what if deadline not met?
			papp->SetWcetEstimationState(app::Application::WcetEstimationState::MONITORING);
			se_promoted_list.emplace_back(papp->Uid(),wm->Id());
			return wm;
		}
	}

	return CreateWorkingMode(papp, resources_request);
}
SchedulerPolicyIF::ExitCode_t
DisTimeSchedPol::ExploreInitialResourceAssignments(ba::AppCPtr_t papp,
						 std::list<ResourceAssignmentCandidate> & resources_request)
{
	// Fair assignment of CPU quota
	auto cpu_quota_available  = sys->ResourceAvailable("sys.cpu.pe", this->sched_status_view);
	auto fair_cpu_quota_total = cpu_quota_available / (2 * this->nr_apps_to_schedule);
	auto cpu_number	  = sys->GetResources("sys.cpu.pe").size(); 
	auto cpu_quota_slice      = (fair_cpu_quota_total / (cpu_number*SCHED_CPU_QUOTA_MIN) +
								((fair_cpu_quota_total % (cpu_number*SCHED_CPU_QUOTA_MIN)) != 0))*SCHED_CPU_QUOTA_MIN;
	

	// For each energy-monitored resource evaluate the amount to assign
	for (const auto & resource : resources_list) {
		auto & resource_path(resource.first);
		auto & availability(resource.second);

		logger->Debug("ExploreInitialResourceAssignments: %s resource=<%s> availability=%lu",
			papp->StrId(),
			resource_path->ToString().c_str(),
			availability);

		if (this->resources_list[resource_path] == 0) {
			logger->Debug("ExploreInitialResourceAssignments: %s no <%s> left",
				papp->StrId(), resource_path->ToString().c_str());
			continue;
		}
		

		ResourceAssignmentCandidate candidate;
		candidate.first = resource_path;
		
		switch (resource_path->ParentType()) {

			case br::ResourceType::CPU:
			{	if (this->resources_list[resource_path] == 0) {
					logger->Debug("ExploreInitialResourceAssignments: "
						"%s CPU already assigned",
						papp->StrId());
					continue;
				}
				if (fair_cpu_quota_total<=0){
					logger->Debug("ExploreInitialResourceAssignments: "
						"%s fair CPU quota already assigned. Skipping resource %s",
						papp->StrId(),
						resource_path->ToString().c_str());
					continue;
				}
				 
				auto quota_taken = std::min(this->resources_list[resource_path], cpu_quota_slice);
				auto quota_available=resources_list[resource_path]-quota_taken;

				fair_cpu_quota_total-=quota_taken;

				candidate.second = quota_taken;
				resources_list[resource_path]=std::max(0UL,quota_available);
				
				logger->Debug("ExploreInitialResourceAssignments: "
					"%s assigned <%s>=%lu. Left to assign %lu",
					papp->StrId(),
					candidate.first->ToString().c_str(),
					candidate.second,
					fair_cpu_quota_total);
				break;
			}
			default:
			{
				logger->Warn("ExploreInitialResourceAssignments: %s"
					" skipping <%s>",
					papp->StrId(),
					resource_path->ToString().c_str());
				break;
			}
		}

			logger->Debug("ExploreInitialResourceAssignments: %s found candidate resources",
				papp->StrId());
			resources_request.push_back(candidate);
		}

	return SCHED_OK;
}

bool DisTimeSchedPol::IsGPUUsed(ba::AwmPtr_t pawm){
	logger->Debug("IsGPUUsed: looking for GPU in %s",
						pawm->StrId());
	for(auto const & resource : *(pawm->GetResourceBinding())){
		if(resource.first->ParentType()==br::ResourceType::GPU){
			return true;
		}
	}
	return false;
}
uint64_t DisTimeSchedPol::GetAvailableCPU(){
	uint64_t total_cpu_available=0;
			for(auto const & resource : resources_list){
					if(resource.first->ParentType()==br::ResourceType::CPU){
						total_cpu_available+=resource.second;
					}
			}
	return total_cpu_available;
}
uint64_t DisTimeSchedPol::GetUsedCPU(ba::AwmPtr_t pawm){
	if (pawm == nullptr){
		return 0;
	}
	uint64_t curr_cpu_used=0;
	for(auto const & resource : *(pawm->GetResourceBinding())){
			if(resource.first->ParentType()==br::ResourceType::CPU){
				curr_cpu_used+=resource.second->GetAmount();
			}
	}
	return curr_cpu_used;

}
uint64_t DisTimeSchedPol::ComputeNextUpperStep(uint64_t total_cpu,uint64_t curr_cpu){
	uint64_t next_step=(total_cpu-curr_cpu)/2;
	if(next_step%SCHED_CPU_QUOTA_MIN){
		next_step+= SCHED_CPU_QUOTA_MIN - (next_step%SCHED_CPU_QUOTA_MIN);
	}
	return next_step;
}
uint64_t DisTimeSchedPol::ComputeNextLowerStep(uint64_t curr_cpu){
	uint64_t next_step=curr_cpu/2;
	if(next_step%SCHED_CPU_QUOTA_MIN){
		next_step+= SCHED_CPU_QUOTA_MIN - (next_step%SCHED_CPU_QUOTA_MIN);
	}
	if(next_step==curr_cpu){
		return 0;
	}

	return next_step;
}
uint64_t DisTimeSchedPol::GetCPUQuota(uint64_t new_amount){
	return (new_amount / (sys->GetResources("sys.cpu.pe").size()*SCHED_CPU_QUOTA_MIN) +
										((new_amount % (sys->GetResources("sys.cpu.pe").size()*SCHED_CPU_QUOTA_MIN)) != 0))*SCHED_CPU_QUOTA_MIN;
}

void DisTimeSchedPol::CopyResourceRequest(ba::AppCPtr_t papp,ba::AwmPtr_t pawm,std::list<ResourceAssignmentCandidate> & resources_request){
	for(auto const & res_bind : *(pawm->GetResourceBinding())){
				auto res_path=res_bind.first;
				auto res_amount=res_bind.second->GetAmount();
				resources_request.emplace_back(res_path,res_amount);
				logger->Debug("ExploreResourceAssignment: %s Reapplying %s : %ld",
						papp->StrId(),
						res_path->ToString().c_str(),
						res_amount);
	}
}
void DisTimeSchedPol::AddGPU(ba::AppCPtr_t papp,std::list<ResourceAssignmentCandidate> & resources_request){
	for(auto const & resource : resources_list){
		if(resource.first->ParentType()==br::ResourceType::GPU){
			auto res_path=resource.first;
			resources_request.emplace_back(res_path,10);
			
			logger->Debug("ExploreResourceAssignment: %s Adding GPU %s",
				papp->StrId(),
				resource.first->ToString().c_str());
				
			break;
		}
	}
}	
void DisTimeSchedPol::AllocateAmount(ba::AppCPtr_t papp,uint64_t new_amount,uint64_t cpu_quota_slice,std::list<ResourceAssignmentCandidate> & resources_request){
	for(auto const & resource : resources_list){
			if(resource.first->ParentType()==br::ResourceType::CPU && resource.second && new_amount){
				ResourceAssignmentCandidate assignment;
				assignment.first=resource.first;
				assignment.second=std::min({cpu_quota_slice,new_amount,resource.second});
				
				resources_request.push_back(assignment);
				new_amount-=assignment.second;

				logger->Debug("ExploreResourceAssignment: %s allocating %s : %ld. Left to assign %ld",
				papp->StrId(),
				resource.first->ToString().c_str(),
				assignment.second,
				new_amount);
				
			}
	}
}
void DisTimeSchedPol::ExploreNewResourceAssignment(ba::AppCPtr_t papp,
										 std::list<ResourceAssignmentCandidate> & resources_request)
{
	auto rt_profile=papp->GetRuntimeProfile();
	auto deadline=papp->GetRecipe()->GetDeadline();
	auto min_res=papp->GetRecipe()->GetMinResources();
	logger->Debug("ExploreResourceAssignment: %s scheduled %d times, min resources asked: %lu",
		papp->StrId(),
		papp->ScheduleCount(),
		min_res);
	
	logger->Debug("ExploreResourceAssignment: %s curr_awm=%s wcet=%f deadline=%f",
		papp->StrId(),
		papp->CurrentAWM()->StrId(),
		rt_profile.wcet.pwcet,
		deadline);
	if (deadline <= 0){
		logger->Error("ExploreResourceAssignment: %s deadline value: %f not valid. Must be greater than 0",
						papp->StrId(),
						deadline);
		return;
	}
	uint64_t total_cpu_available=GetAvailableCPU();
	uint64_t curr_cpu_used=GetUsedCPU(papp->CurrentAWM());

	if (rt_profile.wcet.pwcet > deadline) {
		logger->Debug("ExploreResourceAssignment: %s deadline not met",
						papp->StrId());
		
		if(curr_cpu_used<GetUsedCPU(papp->GetRuntimeWorkingMode(papp->CurrentAWM()->Id()-1))){
			logger->Debug("ExploreResourceAssignment: %s Minimum cost AWM reached at previous step AWM[%s]",
							papp->StrId(),
							papp->GetRuntimeWorkingMode(papp->CurrentAWM()->Id()-1)->StrId());
			CopyResourceRequest(papp,papp->GetRuntimeWorkingMode(papp->CurrentAWM()->Id()-1),resources_request);
			return;
		}
		
		if(!IsGPUUsed(papp->CurrentAWM())){
			logger->Debug("ExploreResourceAssignment: %s trying to add GPU",
						papp->StrId());
			CopyResourceRequest(papp,papp->CurrentAWM(),resources_request);
			AddGPU(papp,resources_request);
		}
		else{
			
			if(total_cpu_available>curr_cpu_used){
				uint64_t new_amount=curr_cpu_used+ComputeNextUpperStep(total_cpu_available,curr_cpu_used);
				uint64_t cpu_quota_slice=GetCPUQuota(new_amount); 
				
				logger->Debug("ExploreResourceAssignment: %s CPU resources total:%lu awm: %lu next: %lu slice: %lu",
							papp->StrId(),
							total_cpu_available,
							curr_cpu_used,
							new_amount,
							cpu_quota_slice);
				AllocateAmount(papp,new_amount,cpu_quota_slice,resources_request);
			}
		}
	}
	else{
		logger->Debug("ExploreResourceAssignment: %s deadline met",
						papp->StrId());
		

		if(IsGPUUsed(papp->CurrentAWM())){
			logger->Debug("ExploreResourceAssignment: %s Minimum cost AWM reached adding GPU AWM[%s]",
								papp->StrId(),
								papp->CurrentAWM()->StrId());
			CopyResourceRequest(papp,papp->CurrentAWM(),resources_request);
			return;
		}
		uint64_t new_amount=ComputeNextLowerStep(curr_cpu_used); 
		if(new_amount>=min_res && total_cpu_available>=new_amount){
			
			uint64_t cpu_quota_slice=GetCPUQuota(new_amount); 
			
			logger->Debug("ExploreResourceAssignment: %s CPU resources total:%ld awm: %ld next: %ld slice: %ld",
						papp->StrId(),
						total_cpu_available,
						curr_cpu_used,
						new_amount,
						cpu_quota_slice);

			AllocateAmount(papp,new_amount,cpu_quota_slice,resources_request);
		}
		else{
			logger->Debug("ExploreResourceAssignment: %s Minimum cost AWM [%s] reached",
							papp->StrId(),
							papp->CurrentAWM()->StrId());
			CopyResourceRequest(papp,papp->CurrentAWM(),resources_request);
		}
		
		
	}

}


ba::AwmPtr_t
DisTimeSchedPol::CreateWorkingMode(ba::AppCPtr_t papp,
				 std::list<ResourceAssignmentCandidate> & resources_request)
{
	auto pawm = papp->CreateNewRuntimeWorkingMode();
	logger->Debug("CreateWorkingMode: %s new AWM id=%s",
		papp->StrId(), pawm->StrId());

	for (const auto & r : resources_request) {
		auto & resource_path(r.first);
		auto resource_amount = r.second;
		if (resource_amount == 0) {
			logger->Debug("CreateWorkingMode: %s [%s] discarding <%s>",
				papp->StrId(),
				pawm->StrId(),
				resource_path->ToString().c_str());
			continue;
		}

		logger->Debug("CreateWorkingMode: %s [%s] assigning <%s>",
			papp->StrId(),
			pawm->StrId(),
			r.first->ToString().c_str());

		auto wm_ret = pawm->AssignResource(resource_path, resource_amount);
		if (wm_ret != ba::WorkingModeStatusIF::WM_SUCCESS) {
			logger->Debug("CreateWorkingMode: %s [%s] error=%d",
				papp->StrId(),
				pawm->StrId(),
				wm_ret);
			return nullptr;
		}
	}

	return pawm;
}

ba::AwmPtr_t
DisTimeSchedPol::FindWorkingModeByResources(ba::AppCPtr_t papp,
					  std::list<ResourceAssignmentCandidate> & resources_request)
{
	logger->Debug("FindWorkingModeByResources: %s resource assignments: %d ",
		papp->StrId(), resources_request.size());

	ba::AwmPtr_t match_awm;
	for (const auto & awm_entry : papp->GetRuntimeWorkingModes()) {
		match_awm = awm_entry.second;
		//Ugly workaround to overcome errors with std::map.find()
		std::map<std::string,uint64_t> resource_bindings;
		for(auto const & res_bind : *(match_awm->GetResourceBinding())){
			resource_bindings[res_bind.first->ToString().c_str()]=res_bind.second->GetAmount();
		}
		for (const auto & request : resources_request) {
			logger->Debug("FindWorkingModeByResources: %s [%s] looking for "
					" <%s>:%lu",
					papp->StrId(),
					awm_entry.second->StrId(),
					request.first->ToString().c_str(),
					request.second);
			logger->Debug("FindWorkingModeByResources: %s in [%s] %d times",
					request.first->ToString().c_str(),
					awm_entry.second->StrId(),
					resource_bindings.count(request.first->ToString()));	
			if(resource_bindings.count(request.first->ToString()) == 0){
				logger->Debug("FindWorkingModeByResources: %s [%s] does not include "
					" <%s>:%lu",
					papp->StrId(),
					awm_entry.second->StrId(),
					request.first->ToString().c_str(),
					request.second);
				match_awm = nullptr;
				break;
			}
			else {
				const auto awm_request = resource_bindings.at(request.first->ToString());
				logger->Debug("FindWorkingModeByResources: %s [%s] includes "
					" <%s>:%lu/%lu",
					papp->StrId(),
					awm_entry.second->StrId(),
					request.first->ToString().c_str(),
					request.second,
					awm_request);

				if(awm_request == request.second){
					logger->Debug("FindWorkingModeByResources: %s [%s] same request on "
					" <%s>",
					papp->StrId(),
					awm_entry.second->StrId(),
					request.first->ToString().c_str());
				}
				else{
					logger->Debug("FindWorkingModeByResources: %s [%s] different request on "
					" <%s>",
					papp->StrId(),
					awm_entry.second->StrId(),
					request.first->ToString().c_str());
					match_awm = nullptr;
					break;
				}
			}
		}

		if (match_awm != nullptr) {
			if(match_awm->NumberOfResourceRequests()!=resources_request.size()){
				logger->Warn("FindWorkingModeByResources: %s explored "
							"resource assignments are a subset of AWM [%s]. Size %d/%d(AWM/REQ) ",
							papp->StrId(),
							match_awm->StrId(),
							match_awm->NumberOfResourceRequests(),
							resources_request.size());
				match_awm = nullptr;
			}
			else{
				logger->Warn("FindWorkingModeByResources: %s explored "
								"resource assignments already existing in [%s]. Size %d/%d(AWM/REQ) ",
								papp->StrId(),
								match_awm->StrId(),
								match_awm->NumberOfResourceRequests(),
								resources_request.size());
				return match_awm;
			}
			
		}
	}

	logger->Debug("FindWorkingModeByResources: %s no working modes"
		" containing the new resource assignments", papp->StrId());
	return nullptr;
}
} // namespace plugins

} // namespace bbque
