/*
 * Copyright (C) 2016  Politecnico di Milano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BBQUE_DISTIME_SCHEDPOL_H_
#define BBQUE_DISTIME_SCHEDPOL_H_

#include <cstdint>
#include <list>
#include <memory>

#include "bbque/configuration_manager.h"
#include "bbque/plugins/plugin.h"
#include "bbque/plugins/scheduler_policy.h"
#include "bbque/scheduler_manager.h"

#define SCHEDULER_POLICY_NAME "distime"

#define MODULE_NAMESPACE SCHEDULER_POLICY_NAMESPACE "." SCHEDULER_POLICY_NAME

#define SCHED_CPU_QUOTA_MIN 100
#define SCHED_MAX_DELAY 10

using bbque::res::RViewToken_t;
using bbque::utils::MetricsCollector;
using bbque::utils::Timer;

// These are the parameters received by the PluginManager on create calls
struct PF_ObjectParams;


namespace bbque { namespace plugins {

class LoggerIF;

/**
 * @class DisTimeSchedPol
 *
 * DisTime scheduler policy registered as a dynamic C++ plugin.
 */
class DisTimeSchedPol: public SchedulerPolicyIF {
	using ResourceAssignmentCandidate = std::pair<br::ResourcePathPtr_t, uint64_t>;
	using SchedEntity = plugins::SchedulerPolicyIF::SchedEntity_t;
public:
	class ScheduleHistory{
		public:
			ScheduleHistory():
				papp(nullptr),
				from_timed_out(false),
				last_cycle_count(0),
				schedule_count(0){};
			ScheduleHistory(ba::AppPtr_t papp):
				papp(papp),
				from_timed_out(false),
				last_cycle_count(papp->GetRuntimeProfile().cycle_count),
				schedule_count(0){};

			bool ComesFromTimeout(){
				return from_timed_out;
			}
			void Reset(){
				schedule_count=0;
				last_cycle_count=papp->GetRuntimeProfile().cycle_count;
				from_timed_out=false;
			}
			bool Update(){
				if( last_cycle_count == papp->GetRuntimeProfile().cycle_count){
					schedule_count++;
				}
				else{
					last_cycle_count=papp->GetRuntimeProfile().cycle_count;
					schedule_count=0;
				}
				if(schedule_count>=SCHED_MAX_DELAY){
					from_timed_out=true;
					return false;
				}
				return true;
			}
			std::string ToString(){
				return "ScheduleCount: "+std::to_string(schedule_count) +
						" LastCycleCount: "+std::to_string(last_cycle_count)+
						" RT_PROFILER: "+std::to_string(papp->GetRuntimeProfile().cycle_count)+
						" MAX: "+std::to_string(SCHED_MAX_DELAY);
			}

		private:
			ba::AppPtr_t papp;
			bool from_timed_out;
			int32_t last_cycle_count;
			int32_t schedule_count;
	} ;
	// :::::::::::::::::::::: Static plugin interface :::::::::::::::::::::::::

	/**
	 * @brief Create the distime plugin
	 */
	static void * Create(PF_ObjectParams *);

	/**
	 * @brief Destroy the distime plugin 
	 */
	static int32_t Destroy(void *);


	// :::::::::::::::::: Scheduler policy module interface :::::::::::::::::::

	/**
	 * @brief Destructor
	 */
	virtual ~DisTimeSchedPol();

	/**
	 * @brief Return the name of the policy plugin
	 */
	char const * Name();


	/**
	 * @brief The member function called by the SchedulerManager to perform a
	 * new scheduling / resource allocation
	 */
	ExitCode_t Schedule(System & system, RViewToken_t & status_view);

private:

	/** Configuration manager instance */
	ConfigurationManager & cm;

	/** Resource accounter instance */
	ResourceAccounter & ra;

	/** System logger instance */
	std::unique_ptr<bu::Logger> logger;

	/** Amount of available resources, used as a cache data structure */
	std::map<br::ResourcePathPtr_t, uint64_t> resources_list;

	std::list<SchedEntity> sched_entity_list;
	std::list<SchedEntity> new_se_list;

	std::list<std::pair<uint32_t,int32_t>> se_promoted_list;
	std::list<std::pair<uint32_t,int32_t>> se_rejected_list;
	std::map<int32_t,ScheduleHistory> timeout_map;

	/** Counter of the number of applications to schedule */
	uint32_t nr_apps_to_schedule;

	/**
	 * @brief Constructor
	 *
	 * Plugins objects could be build only by using the "create" method.
	 * Usually the PluginManager acts as object
	 */
	DisTimeSchedPol();

	/**
	 * @brief Optional initialization member function
	 */
	ExitCode_t _Init();

	/**
	 * @brief Invoked after each application scheduling to update the
	 * current availability of resources used as a cache, instead of
	 * calling the System member functions
	 */
	void UpdateResourcesAvailability();

	/**
	 * @brief Entry point for the scheduling of applications not in RUNNING
	 * state
	 *
	 * @param papp The pointer to the application descriptor
	 */
	ExitCode_t ScheduleNotRunningApplication(ba::AppCPtr_t papp);

	/**
	 * @brief Entry point for the scheduling of applications in RUNNING state
	 *
	 * @param se A SchedEntity object containing the pointer to the application
	 * descriptor and the best working mode for energy efficiency
	 */
	ExitCode_t ScheduleRunningApplication(SchedEntity & se);

	/**
	 * @brief The function containing the statements for performing the
	 * actual scheduling request
	 *
	 * @param papp The pointer to the application descriptor
	 * @param next_awm Assigned working mode
	 * @return
	 */
	ExitCode_t ScheduleApplication(ba::AppCPtr_t papp, ba::AwmPtr_t next_awm);

	/**
	 * @brief Creation of the first working mode for a yet to start application
	 * @param papp The pointer to the application descriptor
	 * @return A pointer to the working mode object created
	 */
	ba::AwmPtr_t ExploreInitialWorkingMode(ba::AppCPtr_t papp);

	/**
	 * @brief For the creation of the first working mode, we need to explore
	 * a first assignment of resources
	 *
	 * @param papp The pointer to the application descriptor
	 * @param resources_request The list of ResourceAssignmentCandidate objects that
	 * will be used to populate with AWM with resource assignments
	 * @return
	 */
	ExitCode_t ExploreInitialResourceAssignments(ba::AppCPtr_t papp,
						std::list<ResourceAssignmentCandidate> & resources_request);

	/**
	 * @brief Exploration of a new working mode for running applications.
	 *
	 * The function creates a new working mod to provide a comparison in
	 * terms of energy efficiency, with the following policy invocations.
	 * @param papp Application descriptor
	 * @param best_awm The best working mode found so far
	 * @return The new working mode obtained from the exploration
	 */
	ba::AwmPtr_t ExploreNewWorkingMode(ba::AppCPtr_t papp, ba::AwmPtr_t pawm);


	bool perform_KS_test(const std::list<int> &cycle_times, double mu, double sg, double xi);
	bool IsGPUUsed(ba::AwmPtr_t awm);
	uint64_t GetAvailableCPU();
	uint64_t GetUsedCPU(ba::AwmPtr_t pawm);
	uint64_t GetCPUQuota(uint64_t new_amount);
	uint64_t ComputeNextUpperStep(uint64_t total_cpu,uint64_t curr_cpu);
	uint64_t ComputeNextLowerStep(uint64_t curr_cpu);
	void AllocateAmount(ba::AppCPtr_t papp,uint64_t new_amount,uint64_t cpu_quota_slice,std::list<ResourceAssignmentCandidate> & resources_request);
	void AddGPU(ba::AppCPtr_t papp,std::list<ResourceAssignmentCandidate> & resources_request);
	void CopyResourceRequest(ba::AppCPtr_t papp,ba::AwmPtr_t pawm,std::list<ResourceAssignmentCandidate> & resources_request);

	/**
	 *
	 * @brief Look for the resource assignment action to apply for scheduling
	 * the application by assigning a new working mode
	 *
	 * @param papp The pointer to the application descriptor
	 * @param resources_request The list of ResourceAssignmentCandidate objects that
	 * will be used to populate with AWM with resource assignments
	 * @return The transition action to apply to the current AWM to get
	 * the resource assignments for the new working mode
	 */
	void ExploreNewResourceAssignment(ba::AppCPtr_t papp,
										 std::list<ResourceAssignmentCandidate> & resources_request);


	/**
	 * @brief Creation of a new working mode, given the new resource assignments
	 *
	 * @param papp The pointer to the application descriptor
	 * @param resources_request the candidate resource assignments
	 * @return a pointer to the new working mode object
	 */
	ba::AwmPtr_t CreateWorkingMode(ba::AppCPtr_t papp,
				std::list<ResourceAssignmentCandidate> & resources_request);

	/**
	 * @brief Check if the resource assignments match an already existing working mode
	 *
	 * @param papp The pointer to the application descriptor
	 * @param resources_request the candidate resource assignments
	 * @return the working mode object (pointer) if a match has been found,
	 * nullptr in case the resource assignments can be used for a new working mode
	 */
	ba::AwmPtr_t FindWorkingModeByResources(ba::AppCPtr_t papp,
						std::list<ResourceAssignmentCandidate> & resources_request);

	/**
	 * @brief The function performing the monitoring of the pWCET. It may change the application
	 * 		  state to profiling.
	 *
	 * @param papp The pointer to the application descriptor
	 * @return
	 */
	bool PerformMonitoring(ba::AppCPtr_t papp);

};

} // namespace plugins

} // namespace bbque

#endif // BBQUE_DISTIME_SCHEDPOL_H_
