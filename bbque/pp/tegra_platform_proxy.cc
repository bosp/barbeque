/*
 * Copyright (C) 2020  Politecnico di Milano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string>

#include "bbque/config.h"
#include "bbque/configuration_manager.h"
#include "bbque/command_manager.h"
#include "bbque/pp/tegra_platform_proxy.h"
#include "bbque/platform_manager.h"
#include "bbque/power_monitor.h"
#include "bbque/resource_accounter.h"
#include "bbque/res/resource_path.h"

#ifdef CONFIG_BBQUE_ENERGY_MONITOR
#include "bbque/energy_monitor.h"
#endif

#define MODULE_NAMESPACE "bq.pp.tegra"

namespace br = bbque::res;
namespace po = boost::program_options;

namespace bbque {

namespace pp {

TEGRAPlatformProxy * TEGRAPlatformProxy::GetInstance()
{
	static TEGRAPlatformProxy * instance;
	if (instance == nullptr)
		instance = new TEGRAPlatformProxy();
	return instance;
}

TEGRAPlatformProxy::TEGRAPlatformProxy():
    cm(ConfigurationManager::GetInstance()),
#ifndef CONFIG_BBQUE_PM
    cmm(CommandManager::GetInstance())
#else
    cmm(CommandManager::GetInstance()),
    pm(PowerManager::GetInstance())
#endif
{
	

	logger = bu::Logger::GetLogger(MODULE_NAMESPACE);
	assert(logger);
	logger->Info("Initializing TegraPlatformProxy");
	platform_id = BBQUE_PP_TEGRA_PLATFORM_ID;
	hardware_id = BBQUE_PP_TEGRA_HARDWARE_ID;
}

TEGRAPlatformProxy::~TEGRAPlatformProxy() { }

PlatformProxy::ExitCode_t TEGRAPlatformProxy::LoadPlatformData()
{
	


	// Local system ID for resource paths construction
	PlatformManager & plm(PlatformManager::GetInstance());
	local_sys_id = plm.GetPlatformDescription().GetLocalSystem().GetId();

	// Register into Resource Accounter and Power Manager
	RegisterDevices();

	// Power management support
#ifdef CONFIG_BBQUE_PM_TEGRA
	PrintDevicesPowerInfo();
#endif
	return PlatformProxy::PLATFORM_OK;
}

PlatformProxy::ExitCode_t TEGRAPlatformProxy::Setup(SchedPtr_t papp)
{
	(void) papp;
	logger->Warn("Setup: No setup action implemented");
	return PlatformProxy::PLATFORM_OK;
}
#ifdef CONFIG_BBQUE_PM

void TEGRAPlatformProxy::PrintPowerInfo(br::ResourcePathPtr_t r_path) const
{
	uint32_t min, max, step, s_min, s_max, ps_count;
	int  s_step;
	auto pm_result = pm.GetFanSpeedInfo(r_path, min, max, step);
	if (pm_result == PowerManager::PMResult::OK)
		logger->Info("PrintPowerInfo: [%s] Fanspeed range: [%4d, %4d, s:%2d] RPM ",
			r_path->ToString().c_str(), min, max, step);

	pm_result = pm.GetVoltageInfo(r_path, min, max, step);
	if (pm_result == PowerManager::PMResult::OK)
		logger->Info("PrintPowerInfo: [%s] Voltage range:  [%4d, %4d, s:%2d] mV ",
			r_path->ToString().c_str(), min, max, step);

	pm_result = pm.GetClockFrequencyInfo(r_path, min, max, step);
	if (pm_result == PowerManager::PMResult::OK)
		logger->Info("PrintPowerInfo: [%s] ClkFreq range:  [%4d, %4d, s:%2d] MHz ",
			r_path->ToString().c_str(),
			min / 1000, max / 1000, step / 1000);

	std::vector<uint32_t> freqs;
	std::string freqs_str(" ");
	pm_result = pm.GetAvailableFrequencies(r_path, freqs);
	if (pm_result == PowerManager::PMResult::OK) {
		for (auto & f : freqs)
			freqs_str += (std::to_string(f) + " ");
		logger->Info("PrintPowerInfo: [%s] ClkFrequencies:  [%s] MHz ",
			r_path->ToString().c_str(), freqs_str.c_str());
	}

	pm_result = pm.GetPowerStatesInfo(r_path, s_min, s_max, s_step);
	if (pm_result == PowerManager::PMResult::OK)
		logger->Info("PrintPowerInfo: [%s] Power states:   [%4d, %4d, s:%2d] ",
			r_path->ToString().c_str(), s_min, s_max, s_step);

	pm_result = pm.GetPerformanceStatesCount(r_path, ps_count);
	if (pm_result == PowerManager::PMResult::OK)
		logger->Info("PrintPowerInfo: [%s] Performance states: %2d",
			r_path->ToString().c_str(), ps_count);
	//		pm.SetFanSpeed(r_path,PowerManager::FanSpeedType::PERCENT, 5);
	//		pm.ResetFanSpeed(r_path);
}

void TEGRAPlatformProxy::PrintDevicesPowerInfo() const
{
	std::string sys_path(br::GetResourceTypeString(br::ResourceType::SYSTEM));
        sys_path += std::to_string(local_sys_id) + ".";

        
        // Build the resource path
        std::string r_path(sys_path);
        r_path += GetResourceTypeString(br::ResourceType::GPU)
                + std::to_string(0)
                + std::string(".pe0");
	
	ResourceAccounter & ra(ResourceAccounter::GetInstance());
	PrintPowerInfo(ra.GetPath(r_path));
		
	
}

#endif // CONFIG_BBQUE_PM


PlatformProxy::ExitCode_t TEGRAPlatformProxy::MapResources(ba::SchedPtr_t papp,
							    br::ResourceAssignmentMapPtr_t assign_map,
							    bool excl)
{
	(void) papp;
	(void) assign_map;
	(void) excl;
	logger->Warn("TEGRA: No mapping action implemented");
	return PlatformProxy::PLATFORM_OK;
}

PlatformProxy::ExitCode_t TEGRAPlatformProxy::RegisterDevices()
{

	std::string sys_path(br::GetResourceTypeString(br::ResourceType::SYSTEM));
	sys_path += std::to_string(local_sys_id) + ".";

	
	// Build the resource path
	std::string r_path(sys_path);
	r_path += GetResourceTypeString(br::ResourceType::GPU)
		+ std::to_string(0)
		+ std::string(".pe0");
	logger->Debug("RegisterDevices: r_path=<%s>", r_path.c_str());

	// Add to resource accounter
	ResourceAccounter & ra(ResourceAccounter::GetInstance());
	auto resource = ra.RegisterResource(r_path, "", 100, "TEGRA");
	auto resource_path = resource->Path();
	bbque_assert(resource_path);
	logger->Debug("RegisterDevices: resource path = <%s>",
		resource_path->ToString().c_str());

#ifdef CONFIG_BBQUE_WM
	PowerMonitor & wm(PowerMonitor::GetInstance());
	wm.Register(resource_path);
#endif

#ifdef CONFIG_BBQUE_ENERGY_MONITOR
	EnergyMonitor & eym(EnergyMonitor::GetInstance());
	eym.RegisterResource(resource_path);
#endif
	
	logger->Info("RegisterDevices: id=%d type=<%s> model=%s",
		0,
		GetResourceTypeString(br::ResourceType::GPU),
		resource->Model().c_str());

	return PlatformProxy::PLATFORM_OK;
}

PlatformProxy::ExitCode_t TEGRAPlatformProxy::Refresh()
{
	return PlatformProxy::PLATFORM_OK;
}

PlatformProxy::ExitCode_t TEGRAPlatformProxy::Release(SchedPtr_t papp)
{
	(void) papp;
	logger->Warn("Release: No release action implemented");
	return PlatformProxy::PLATFORM_OK;
}

PlatformProxy::ExitCode_t TEGRAPlatformProxy::ReclaimResources(SchedPtr_t papp)
{
	(void) papp;
	logger->Warn("ReclaimResources: No reclaiming action implemented");
	return PlatformProxy::PLATFORM_OK;
}
void TEGRAPlatformProxy::Exit()
{
	logger->Debug("Exiting the TEGRA Platform Proxy...");

	logger->Notice("TEGRA Platform Proxy.shutdownend correctly");

}

} // namespace pp
} // namespace bbque
