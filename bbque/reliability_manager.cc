/*
 * Copyright (C) 2019  Politecnico di Milano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "bbque/reliability_manager.h"
#include "bbque/resource_manager.h"

#ifdef CONFIG_BBQUE_LIBHWREL
	#include "bsc/bschwrel.h"
#endif

#undef MODULE_NAMESPACE
#define MODULE_NAMESPACE "bq.lm"

#define CPU_INITIAL_FIT 12345	// TODO

namespace po = boost::program_options;
using namespace std::placeholders;
using namespace std::chrono;

namespace bbque {

ReliabilityManager & ReliabilityManager::GetInstance()
{
	static ReliabilityManager instance;
	return instance;
}

ReliabilityManager::ReliabilityManager() :
    cm(CommandManager::GetInstance()),
    ra(ResourceAccounter::GetInstance()),
    am(ApplicationManager::GetInstance()),
#ifdef CONFIG_BBQUE_LINUX_PROC_MANAGER
    prm(ProcessManager::GetInstance()),
#endif
    plm(PlatformManager::GetInstance()),
    logger(bu::Logger::GetLogger(MODULE_NAMESPACE))
{

	// Notify the degradation of a resource
#define CMD_NOTIFY_DEGRADATION "notify_degradation"
	cm.RegisterCommand(MODULE_NAMESPACE "." CMD_NOTIFY_DEGRADATION,
			static_cast<CommandHandler*> (this),
			"Performance degradation affecting the resource "
			"[percentage]");

#define CMD_SIMULATE_FAULT "simulate_fault"
	cm.RegisterCommand(MODULE_NAMESPACE "." CMD_SIMULATE_FAULT,
			static_cast<CommandHandler*> (this),
			"Simulate the occurrence of a resource fault");

#define CMD_FREEZE "freeze"
	cm.RegisterCommand(MODULE_NAMESPACE "." CMD_FREEZE,
			static_cast<CommandHandler*> (this),
			"Freeze a managed application or process");

#define CMD_THAW "thaw"
	cm.RegisterCommand(MODULE_NAMESPACE "." CMD_THAW,
			static_cast<CommandHandler*> (this),
			"Thaw a managed application or process");

#define CMD_CHECKPOINT "checkpoint"
	cm.RegisterCommand(MODULE_NAMESPACE "." CMD_CHECKPOINT,
			static_cast<CommandHandler*> (this),
			"Checkpoint of a managed application or process");

#define CMD_RESTORE "restore"
	cm.RegisterCommand(MODULE_NAMESPACE "." CMD_RESTORE,
			static_cast<CommandHandler*> (this),
			"Restore a managed application or process");

	// Create the directory for the general checkpoint information of each
	// integrated application "/info"
	try {
		if (!boost::filesystem::exists(checkpoint_appinfo_dir))
			boost::filesystem::create_directory(checkpoint_appinfo_dir);
		boost::filesystem::perms prms(boost::filesystem::owner_all);
		prms |= boost::filesystem::others_read;
		prms |= boost::filesystem::group_read;
		prms |= boost::filesystem::group_write;
		boost::filesystem::permissions(checkpoint_appinfo_dir, prms);
	}
	catch (std::exception & ex) {
		logger->Error("ReliabilityManager: %s: %s",
			ex.what(), checkpoint_appinfo_dir.c_str());
	}

#ifdef CONFIG_BBQUE_LIBHWREL
	this->hwrel = std::make_unique<libhwrel::BSC_HWReliabilityMonitor>();

    for(const auto &res : monitored_resources) {
		if(res.path->Type() == ResourceType::CPU) {
			auto nr_cores = ra.GetResources(res.path->ToString() + ".pe").size();
			hwrel_state[*res.path] = this->hwrel->init(libhwrel::resource_type_t::CPU, libhwrel::technology_type_t::SILICON, CPU_INITIAL_FIT, nr_cores);
		}
    }

	//
#endif

	// HW monitoring thread
	Worker::Setup(BBQUE_MODULE_NAME("lm.hwmon"), MODULE_NAMESPACE);
	Worker::Start();
}

ReliabilityManager::~ReliabilityManager()
{
 	logger->Info("Reliability manager: terminating...");
}

void ReliabilityManager::Task()
{
#ifdef CONFIG_BBQUE_PERIODIC_CHECKPOINT
	// Periodic checkpoint
	chk_timer.start();
        chk_thread = std::thread(&ReliabilityManager::PeriodicCheckpointTask, this);

#elif defined CONFIG_BBQUE_DYNAMIC_CHECKPOINT
	// Dynamic checkpoint
	chk_timer.start();
	chk_thread = std::thread(&ReliabilityManager::DynamicCheckpointTask, this);
#endif
	while (!done) {
		std::this_thread::sleep_for(std::chrono::seconds(1));
		// TODO: HW reliability monitoring...
	}

#ifdef CONFIG_BBQUE_CHECKPOINT_SCHEDULER
	logger->Debug("Task: waiting for periodic checkpoint thread");
	chk_timer.stop();
	chk_thread.join();
#endif
}



#ifdef CONFIG_BBQUE_DYNAMIC_CHECKPOINT

double ReliabilityManager::ComputeCheckpointOverhead(app::SchedPtr_t psched)
{
    
    double expected_chk_time = psched->GetCheckpointLatencyMean();
    
    if (std::isnan(expected_chk_time))
         expected_chk_time = DEFAULT_EXPECTED_CHECKPOINT_TIME;
    
    double elapsed_time = psched->GetElapsedTimeSinceLastCheckpoint();
    
    double total_time = elapsed_time + expected_chk_time;
    
    double chk_overhead = expected_chk_time/total_time;
    logger->Debug("ComputeCheckpointOverhead: expected_chk_overhead=%.2f", chk_overhead); 
    
    return chk_overhead;

    
}


void ReliabilityManager::DynamicCheckpointTask()
{
	logger->Debug("DynamicCheckpointTask: thread launched [tid=%d]", gettid());

	while (!done) {
		// Applications
                auto start_period = chk_timer.getTimestampMs();
                AppsUidMapIt app_it;
                ba::AppCPtr_t papp = am.GetFirst(ba::Schedulable::RUNNING, app_it);
                for (; papp; papp = am.GetNext(ba::Schedulable::RUNNING, app_it))
                {
                        logger->Debug("DynamicCheckpointTask: specified max checkpoint overhead ratio <%s>: %.2f ",
                                  papp->StrId(),
                                  papp->GetCheckpointOverheadRatioGoal());
                        double chk_overhead = ReliabilityManager::ComputeCheckpointOverhead(papp);

                        if (chk_overhead <= papp->GetCheckpointOverheadRatioGoal())
                        {
                                auto start_ms = chk_timer.getTimestampMs();
                                papp->StartDumping();
                                Dump(papp);
                                papp->SetLastResumeTime();
                                papp->EndDumping();
                                auto end_ms = chk_timer.getTimestampMs();
                                auto elapsed_ms = end_ms - start_ms;
                                logger->Info("DynamicCheckpointTask: <%s> checkpoint routine completed in %.f ms", papp->StrId(), elapsed_ms);
                        }
                        else
                        {
                                logger->Debug("DynamicCheckpointTask: expected_chk_overhead=%.2f, checkpoint not scheduled.", chk_overhead);
                        }
		}

        // Processes
#ifdef CONFIG_BBQUE_LINUX_PROC_MANAGER
		ProcessMapIterator proc_it;
		ProcPtr_t proc = prm.GetFirst(ba::Schedulable::RUNNING, proc_it);
		for (; proc; proc = prm.GetNext(ba::Schedulable::RUNNING, proc_it)) {
			logger->Debug("DynamicCheckpointTask: specified max checkpoint overhead ratio <%s>: %.2f",
			proc->StrId(),
			proc->GetCheckpointOverheadRatioGoal());

			double chk_overhead = ReliabilityManager::ComputeCheckpointOverhead(proc);

			if (chk_overhead <= proc->GetCheckpointOverheadRatioGoal())
			{
				auto start_ms = chk_timer.getTimestampMs();
				proc->StartDumping();
				Dump(proc);
				proc->SetLastResumeTime();
				proc->EndDumping();
				auto end_ms = chk_timer.getTimestampMs();
				auto elapsed_ms = end_ms - start_ms;
				logger->Info("DynamicCheckpointTask: <%s> checkpoint routine completed in %.f ms", proc->StrId(), elapsed_ms);
			}
			else {
				logger->Debug("DynamicCheckpointTask: expected_chk_overhead=%.2f, checkpoint not scheduled.", chk_overhead);
			}

		}
 #endif
		auto end_period = chk_timer.getTimestampMs();
		auto elapsed_time = end_period - start_period;
		auto wait_time = std::max<int>(chk_period_len - elapsed_time, 0);
		// Wake me up later
		std::this_thread::sleep_for(std::chrono::milliseconds(wait_time));

	}

	logger->Notice("DynamicCheckpoint: terminating...");
	chk_timer.stop();
}

#endif // CONFIG_BBQUE_DYNAMIC_CHECKPOINT


#ifdef CONFIG_BBQUE_PERIODIC_CHECKPOINT

void ReliabilityManager::PeriodicCheckpointTask()
{
	logger->Debug("PeriodicCheckpointTask: thread launched [tid=%d]",
		gettid());

	while (!done) {
		// Now
		//auto start_sec = chk_timer.getTimestampMs();

		// Applications
		AppsUidMapIt app_it;
		ba::AppCPtr_t papp = am.GetFirst(ba::Schedulable::RUNNING, app_it);
		for (; papp; papp = am.GetNext(ba::Schedulable::RUNNING, app_it)) {
			papp->StartDumping();
			Dump(papp);
			papp->EndDumping();
		}
        auto elapsed_sec = chk_timer.getTimestampMs();

		// Processes
#ifdef CONFIG_BBQUE_LINUX_PROC_MANAGER
		ProcessMapIterator proc_it;
		ProcPtr_t proc = prm.GetFirst(ba::Schedulable::RUNNING, proc_it);
		for (; proc; proc = prm.GetNext(ba::Schedulable::RUNNING, proc_it)) {
                        auto start_sec = chk_timer.getTimestampMs();

			proc->StartDumping();
            Dump(proc);
            proc->EndDumping();

                        auto end_sec = chk_timer.getTimestampMs();
                        elapsed_sec = end_sec - start_sec;
                        logger->Debug("PeriodicCheckpoint: task completed in %.f ms",
			elapsed_sec);

		}
#endif

		// End time
// 		auto end_sec = chk_timer.getTimestampMs();
// 		auto elapsed_sec = end_sec - start_sec;
// 		logger->Debug("PeriodicCheckpoint: task completed in %.f ms",
//			elapsed_sec);

		unsigned int next_chk_in = std::max<int>(
			chk_period_len - elapsed_sec,
			BBQUE_MIN_CHECKPOINT_PERIOD_MS);
		// Wake me up later
		logger->Debug("PeriodicCheckpoint: next in %d ms", next_chk_in);
		std::this_thread::sleep_for(std::chrono::milliseconds(next_chk_in));
	}

	logger->Notice("PeriodicCheckpoint: terminating...");
	chk_timer.stop();

}

#endif

void ReliabilityManager::NotifyFaultDetection(res::ResourcePtr_t rsrc)
{
	// Freeze involved applications or processes
	br::AppUsageQtyMap_t apps;
	rsrc->Applications(apps);
	logger->Debug("NotifyFaultDetection: <%s> used by <%d> applications",
		rsrc->Path()->ToString().c_str(),
		apps.size());

	ba::SchedPtr_t psched;
	for (auto app_entry : apps) {
		psched = am.GetApplication(app_entry.first);

#ifdef CONFIG_BBQUE_LINUX_PROC_MANAGER
		if (!psched) {
			psched = prm.GetProcess(app_entry.first);
			logger->Debug("NotifyFaultDetection: <%s> is a process",
				psched->StrId());
		}
#endif
		if (!psched) {
			logger->Warn("NotifyFaultDetection: UID=<%d>"
				": no application or process",
				app_entry.first);
			continue;
		}

		logger->Debug("NotifyFaultDetection: <%s> => freeze <%s>",
			rsrc->Path()->ToString().c_str(),
			psched->StrId());

		auto ret = plm.Freeze(psched);
		if (ret != ReliabilityActionsIF::ExitCode_t::OK) {
			logger->Error("NotifyFaultDetection: <%s> => <%s> "
				"platform failure while freezing",
				rsrc->Path()->ToString().c_str(),
				psched->StrId());
			continue;
		}
		logger->Info("NotifyFaultDetection: <%s> => <%s> successfully frozen");
	}

	// Set offline faulty resources
	logger->Debug("NotifyFaultDetection: <%s> to switch off",
		rsrc->Path()->ToString().c_str());
	ra.SetOffline(rsrc->Path());

	// Trigger policy execution by notifying a "platform" event
	ResourceManager & rm = ResourceManager::GetInstance();
	rm.NotifyEvent(ResourceManager::BBQ_PLAT);
}

/************************************************************************
 *                   COMMANDS HANDLING                                  *
 ************************************************************************/

int ReliabilityManager::CommandsCb(int argc, char * argv[])
{
	uint8_t cmd_offset = ::strlen(MODULE_NAMESPACE) + 1;
	char * cmd_id = argv[0] + cmd_offset;
	logger->Info("CommandsCb: processing command [%s]", cmd_id);

	// Set the degradation value of the given resources
	if (!strncmp(CMD_NOTIFY_DEGRADATION, cmd_id, strlen(CMD_NOTIFY_DEGRADATION))) {
		if (!(argc % 2)) {
			logger->Error("'%s.%s' expecting {resource path, value} pairs.",
				MODULE_NAMESPACE, CMD_NOTIFY_DEGRADATION);
			logger->Error("Example: '%s.%s <resource_path> "
				"(e.g., sys0.cpu0.pe0)"
				" <degradation_percentage> (e.g. 10) ...'",
				MODULE_NAMESPACE, CMD_NOTIFY_DEGRADATION);
			return 1;
		}
		return ResourceDegradationHandler(argc, argv);
	}

	// Set the degradation value of the given resources
	if (!strncmp(CMD_SIMULATE_FAULT, cmd_id, strlen(CMD_SIMULATE_FAULT))) {
		if (argc < 2) {
			logger->Error("'%s.%s' expecting {resource path} .",
				MODULE_NAMESPACE, CMD_SIMULATE_FAULT);
			logger->Error("Example: '%s.%s <r1>"
				"(e.g., sys0.cpu0.pe0 ...)",
				MODULE_NAMESPACE, CMD_SIMULATE_FAULT);
			return 2;
		}
		SimulateFault(argv[1]);
		return 0;
	}

	if (!strncmp(CMD_FREEZE, cmd_id, strlen(CMD_FREEZE))) {
		if (argc < 2) {
			logger->Error("'%s.%s' expecting process id.",
				MODULE_NAMESPACE, CMD_FREEZE);
			logger->Error("Example: '%s.%s 12319",
				MODULE_NAMESPACE, CMD_FREEZE);
			return 3;
		}
		Freeze(std::stoi(argv[1]));
		return 0;
	}


	if (!strncmp(CMD_THAW, cmd_id, strlen(CMD_THAW))) {
		if (argc < 2) {
			logger->Error("'%s.%s' expecting process id.",
				MODULE_NAMESPACE, CMD_THAW);
			logger->Error("Example: '%s.%s 12319",
				MODULE_NAMESPACE, CMD_THAW);
			return 3;
		}
		Thaw(std::stoi(argv[1]));
		return 0;
	}

	if (!strncmp(CMD_CHECKPOINT, cmd_id, strlen(CMD_CHECKPOINT))) {
		if (argc < 2) {
			logger->Error("'%s.%s' expecting process id.",
				MODULE_NAMESPACE, CMD_CHECKPOINT);
			logger->Error("Example: '%s.%s 8823",
				MODULE_NAMESPACE, CMD_CHECKPOINT);
			return 5;
		}
		Dump(std::stoi(argv[1]));
		return 0;
	}

	if (!strncmp(CMD_RESTORE, cmd_id, strlen(CMD_RESTORE))) {
		if (argc < 3) {
			logger->Error("'%s.%s' expecting process id and executable name",
				MODULE_NAMESPACE, CMD_RESTORE);
			logger->Error("Example: '%s.%s 8823 myprogram",
				MODULE_NAMESPACE, CMD_RESTORE);
			return 5;
		}
		Restore(std::stoi(argv[1]), argv[2]);
		return 0;
	}

	logger->Error("CommandsCb: unexpected value [%s]", cmd_id);

	return 0;
}

void ReliabilityManager::SimulateFault(std::string const & resource_path)
{
	auto resource_list = ra.GetResources(resource_path);
	if (resource_list.empty()) {
		logger->Error("SimulateFault: <%s> not a valid resource",
			resource_path.c_str());
	}

	for (auto & rsrc : resource_list) {
		logger->Notice("SimulateFault: fault on <%s>",
			rsrc->Path()->ToString().c_str());
		NotifyFaultDetection(rsrc);
	}
}

void ReliabilityManager::Freeze(app::AppPid_t pid)
{
	AppUid_t uid = app::Application::Uid(pid, 0);
	app::SchedPtr_t psched = am.GetApplication(uid);
	if (psched) {
		logger->Debug("Freeze: moving application <%s> into freezer...",
			psched->StrId());
	}
#ifdef CONFIG_BBQUE_LINUX_PROC_MANAGER
	else {
		psched = prm.GetProcess(pid);
		if (psched)
			logger->Debug("Freeze: moving process <%s> into freezer",
				psched->StrId());
	}
#endif

	if (!psched) {
		logger->Warn("Freeze: pid=<%d> no application or process", pid);
		return;
	}
	plm.Freeze(psched);
	logger->Debug("Freeze: is <%s> frozen for you?", psched->StrId());
}

void ReliabilityManager::Thaw(app::AppPid_t pid)
{
	bool exec_found = false;
	AppUid_t uid = app::Application::Uid(pid, 0);
	auto ret = am.SetToThaw(uid);
	if (ret == ApplicationManager::ExitCode_t::AM_SUCCESS) {
		logger->Debug("Thaw: moving application uid=<%d> into freezer...", uid);
		exec_found = true;
	}
#ifdef CONFIG_BBQUE_LINUX_PROC_MANAGER
	else {
		auto ret = prm.SetToThaw(pid);
		if (ret == ProcessManager::ExitCode_t::SUCCESS) {
			logger->Debug("Thaw: moving process pid=<%d> into freezer", pid);
			exec_found = true;
		}
	}
#endif

	if (!exec_found) {
		logger->Warn("Thaw: pid=<%d> no application or process", pid);
		return;
	}

	logger->Debug("Thaw: triggering re-scheduling");
	ResourceManager & rm = ResourceManager::GetInstance();
	rm.NotifyEvent(ResourceManager::BBQ_PLAT);
}

void ReliabilityManager::Dump(app::AppPid_t pid)
{
	app::SchedPtr_t psched;
	AppUid_t uid = app::Application::Uid(pid, 0);
	psched = am.GetApplication(uid);
	if (psched) {
		logger->Debug("Dump: <%s> application checkpoint...",
			psched->StrId());
	}
#ifdef CONFIG_BBQUE_LINUX_PROC_MANAGER
	else {
		psched = prm.GetProcess(pid);
		if (psched)
			logger->Debug("Dump: <%s> process checkpoint...",
				psched->StrId());
	}
#endif

	if (!psched) {
		logger->Warn("Dump: pid=<%d> no application or process "
			"to checkpoint", pid);
		return;
	}

	Dump(psched);
}

void ReliabilityManager::Dump(app::SchedPtr_t psched)
{
#ifdef CONFIG_BBQUE_CHECKPOINT_SCHEDULER
	double _start = chk_timer.getTimestampMs();
#endif
	auto ret = plm.Dump(psched);
	if (ret != ReliabilityActionsIF::ExitCode_t::OK) {
		logger->Error("Dump: <%s> checkpoint failed", psched->StrId());
		am.CheckEXC(psched->Pid(), 0, true);
#ifdef CONFIG_BBQUE_LINUX_PROC_MANAGER
		prm.CheckProcess(psched->Pid(), true);
#endif
		return;
	}

#ifdef CONFIG_BBQUE_CHECKPOINT_SCHEDULER
	double _end = chk_timer.getTimestampMs();
	psched->UpdateCheckpointLatency(_end - _start);
	logger->Debug("Dump: <%s> checkpointed in %.f ms [mean = %.f ms]",
		psched->StrId(),
		_end - _start,
		psched->GetCheckpointLatencyMean());

        if (psched->GetNumberOfCheckpointsPerformed() > BBQUE_MAX_NR_CHECKPOINT_IMAGES)
        {
                std::string image_prefix_dir(BBQUE_CHECKPOINT_IMAGE_PATH);
                std::string image_dir(ApplicationPath(
                                image_prefix_dir + "/linux/",
                                psched->Pid(),
                                psched->Name()));
                std::string old_image_dir(image_dir + "/dump_"
			+ std::to_string(psched->GetNumberOfCheckpointsPerformed() - BBQUE_MAX_NR_CHECKPOINT_IMAGES));
                logger->Debug("Dump: <%s> deleting directory %s",psched->StrId(), old_image_dir.c_str());
                boost::filesystem::remove_all(old_image_dir);
        }
#endif
}

void ReliabilityManager::Restore(app::AppPid_t pid, std::string exe_name)
{
	// Retrieve info about the application type
	std::string app_type_filename(
				ApplicationPath(checkpoint_appinfo_dir, pid, exe_name)
				+ "/type");
	logger->Debug("Restore: opening %s...", app_type_filename.c_str());
	std::ifstream type_ifs(app_type_filename, std::ifstream::in);
	if (!type_ifs.is_open()) {
		logger->Error("Restoring: missing type info file %s",
			app_type_filename.c_str());
		return;
	}

	std::string app_type;
	type_ifs >> app_type;
	type_ifs.close();

	if (app_type.compare("ADAPTIVE") == 0) {
		logger->Info("Restore: ADAPTIVE application");
		AppUid_t uid = app::Application::Uid(pid, 0);
		app::SchedPtr_t psched = am.GetApplication(uid);
		if (psched) {
			if (psched->Active() && am.CheckEXC(pid, 0, false)) {
				logger->Warn("Restore: trying to restore a "
					"running application: <%s>",
					psched->StrId());
				return;
			}
		}

		// Retrieve the recipe previously used
		std::string app_recipe_filename(ApplicationPath(checkpoint_appinfo_dir,
								pid,
								exe_name)
						+ "/recipe");
		logger->Debug("Restore: opening %s...", app_recipe_filename.c_str());
		std::ifstream recipe_ifs(app_recipe_filename, std::ifstream::in);
		if (!recipe_ifs.is_open()) {
			logger->Error("Restoring: missing recipe info file %s",
				app_type_filename.c_str());
			return;
		}

		std::string app_recipe;
		recipe_ifs >> app_recipe;
		recipe_ifs.close();

		// Restore the execution context
		am.RestoreEXC(exe_name, pid, 0, app_recipe);

		// Trigger the resource allocation policy execution
		ResourceManager & rm = ResourceManager::GetInstance();
		rm.NotifyEvent(ResourceManager::BBQ_OPTS);
	}
#ifdef CONFIG_BBQUE_LINUX_PROC_MANAGER
	else if (app_type.compare("PROCESS") == 0) {
		app::SchedPtr_t psched = prm.GetProcess(pid);
		if (psched) {
			logger->Warn("Restore: trying to restore a "
				"running process: <%s>",
				psched->StrId());
			return;
		}
		logger->Notice("Restore: PROCESS application");
		prm.NotifyStart(exe_name, pid, app::Schedulable::RESTORING);
	}
#endif
	else {
		logger->Error("Restore: unknown application type [%s]",
			app_type.c_str());
		return;
	}

	logger->Debug("Restore: [pid=%d name=%s] restore sequence started",
		pid, exe_name.c_str());
}

int ReliabilityManager::ResourceDegradationHandler(int argc, char * argv[])
{
	int j = 1;
	int nr_args = argc + 1;

	// Parsing the "<resource> <degradation_value>" pairs
	while ((nr_args -= 2) && (j += 2)) {
		auto rsrc(ra.GetResource(argv[j]));
		if (rsrc == nullptr) {
			logger->Error("Resource degradation: "
				" <%s> not a valid resource",
				argv[j]);
			continue;
		}

		if (IsNumber(argv[j + 1])) {
			rsrc->UpdateDegradationPerc(atoi(argv[j + 1]));
			logger->Warn("Resource degradation: "
				"<%s> = %2d%% [mean=%.2f]",
				argv[j],
				rsrc->CurrentDegradationPerc(),
				rsrc->MeanDegradationPerc());
		}
		else {
			logger->Error("Resource degradation: "
				"<%s> not a valid value",
				argv[j + 1]);
		}
	}

	return 0;
}

ReliabilityManager::ExitCode_t ReliabilityManager::Register(br::ResourcePathPtr_t rp)
{
	ResourceAccounter & ra(ResourceAccounter::GetInstance());

	// Register all the resources referenced by the path specified
	auto r_list(ra.GetResources(rp));
	if (r_list.empty()) {
		logger->Warn("Register: no resources to monitor <%s>", rp->ToString().c_str());
		return ExitCode_t::ERR_RSRC_MISSING;
	}

	// Register each resource to monitor
	for (auto & rsrc : r_list) {
		logger->Info("Register: adding <%s> to reliability monitoring...",
			rsrc->Path()->ToString().c_str());
		monitored_resources.push_back( { rsrc->Path(), rsrc});
	}

	return ExitCode_t::OK;
}

ReliabilityManager::ExitCode_t ReliabilityManager::Register(const std::string & rp_str)
{
	ResourceAccounter & ra(ResourceAccounter::GetInstance());
	auto res = ra.GetPath(rp_str);
	assert(res);
	return Register(res);
}

#ifdef CONFIG_BBQUE_LIBHWREL
void ReliabilityManager::UpdateReliabilityInfo() {

    for(const auto &res : monitored_resources) {
        
    }

}
#endif

} // namespace bbque
