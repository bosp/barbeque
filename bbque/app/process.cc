/*
 * Copyright (C) 2019  Politecnico di Milano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iomanip>
#include <sstream>
#include <sys/sysinfo.h>

#include "bbque/app/process.h"

#define MODULE_NAMESPACE "bq.pr"
#define MODULE_CONFIG    "Process"
#define CPU_USAGE_PERIOD 2000

namespace bbque {
namespace app {

Process::Process(
		 std::string const & _name,
		 AppPid_t _pid,
		 AppPrio_t _prio,
		 State_t _state,
		 SyncState_t _sync) :
	Schedulable(_name, _pid, Schedulable::Type::PROCESS)
{
	priority = _prio;
	schedule.state = _state;
	schedule.syncState = _sync;

	logger = bbque::utils::Logger::GetLogger(MODULE_NAMESPACE);

	// Format the application string identifier for logging purpose
	std::stringstream pid_stream;
	pid_stream << std::right << std::setfill('0')
		<< std::setw(5) << std::to_string(Pid());
	str_id = pid_stream.str() + ":" + Name().substr(0, 8);
    cpu_monitor_thread = std::thread(&Process::MonitorCPUUsage, this);

#ifdef CONFIG_BBQUE_DYNAMIC_CHECKPOINT
    chk_ovh_ratio = BBQUE_DEFAULT_CHK_OVERHEAD_RATIO;
#endif
}

void Process::MonitorCPUUsage()
{

        int nr_proc_elements = get_nprocs();
        
        uint32_t total_cpu_time_t1, total_cpu_time_t2;
        uint32_t proc_cpu_time_t1, proc_cpu_time_t2; 
        uint32_t cpu_usage;
        
        total_cpu_time_t1 = GetTotalCPUTime(nr_proc_elements);
        proc_cpu_time_t1 = GetProcessCPUTime();
        std::this_thread::sleep_for(std::chrono::milliseconds(CPU_USAGE_PERIOD));
        
        std::string proc_path = "/proc/"+std::to_string(pid);
        struct stat buf;
        stat(proc_path.c_str(), &buf);
        while(!stat(proc_path.c_str(), &buf))
        {
                total_cpu_time_t2 = Process::GetTotalCPUTime(nr_proc_elements);
                proc_cpu_time_t2 = GetProcessCPUTime();

                cpu_usage = 100*(proc_cpu_time_t2-proc_cpu_time_t1)/(total_cpu_time_t2 - total_cpu_time_t1);

                UpdateRuntimeProfile(cpu_usage);
                
                total_cpu_time_t1 = total_cpu_time_t2;
                proc_cpu_time_t1 = proc_cpu_time_t2;
                std::this_thread::sleep_for(std::chrono::milliseconds(CPU_USAGE_PERIOD));
        }
}

uint32_t Process::GetProcessCPUTime()
{
        uint32_t utime, stime;
        std::string ignore;
        std::ifstream pid_stat ("/proc/"+std::to_string(pid)+"/stat");
        for (int i = 1; i < 14; i++){
                pid_stat >> ignore;
        }
        
        pid_stat >> utime >> stime;
        uint32_t proc_cpu_time = utime + stime;
        
        return proc_cpu_time;
}
    
uint32_t Process::GetTotalCPUTime(int nr_proc_elements)
{
        uint32_t user, nice, system, idle;
        std::string ignore;
        std::ifstream stat ("/proc/stat");
        
        stat >> ignore >> user >> nice >> system >> idle;
        
        uint32_t total_cpu_time = user + nice + system + idle;
        
        return total_cpu_time/nr_proc_elements;
}
    
    
} // namespace app

} // namespace bbque
