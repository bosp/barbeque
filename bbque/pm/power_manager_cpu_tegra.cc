/*
 * Copyright (C) 2016  Politecnico di Milano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <dlfcn.h>
#include <fstream>
#include <algorithm>


#include "bbque/pm/power_manager_cpu_tegra.h"
#include "bbque/resource_accounter.h"
#include "bbque/utils/iofs.h"
#include "bbque/utils/string_utils.h"



namespace br = bbque::res;
namespace bu = bbque::utils;

namespace bbque {


TEGRA_CPUPowerManager & TEGRA_CPUPowerManager::GetInstance()
{
	static TEGRA_CPUPowerManager instance;
	return instance;
}

TEGRA_CPUPowerManager::TEGRA_CPUPowerManager()
{
	is_sampling=false;
	energy=0;
	logger->Info("TEGRA_CPUPowerManager initialization...");
}


TEGRA_CPUPowerManager::~TEGRA_CPUPowerManager()
{
	is_sampling=false;
	energy_thread.join();
}


PowerManager::PMResult
TEGRA_CPUPowerManager::GetTemperature(br::ResourcePathPtr_t const & rp,
				   uint32_t & celsius)
{
	(void) rp;
	bu::IoFs::ExitCode_t io_result;

	io_result = bu::IoFs::ReadIntValueFrom<uint32_t>(BBQUE_TEGRA_CPU_TEMP_PATH,celsius,1);

	if(io_result != bu::IoFs::ExitCode_t::OK){
		logger->Error("GetTemperature: Cannot get CPU temperature");
		return PMResult::ERR_SENSORS_ERROR;
	}

	celsius = celsius / 1000;
	logger->Debug("GetTemperature: [CPU] temperature: %d", celsius);

	return PMResult::OK;
}

/* Fan */

PowerManager::PMResult
TEGRA_CPUPowerManager::GetFanSpeed(br::ResourcePathPtr_t const & rp,
				FanSpeedType fs_type,	uint32_t & value)
{
	(void) rp;
	bu::IoFs::ExitCode_t io_result;

	// Fan speed type
	if (fs_type == FanSpeedType::PERCENT){

		io_result = bu::IoFs::ReadIntValueFrom<uint32_t>(BBQUE_TEGRA_CPU_FAN_PWM_PATH,value,1);
		if(io_result != bu::IoFs::ExitCode_t::OK){
			logger->Error("GetTemperature: Cannot get fan pwm");
			return PMResult::ERR_SENSORS_ERROR;
		}

		value = (value/255) * 100;

	}
	
	else if (fs_type == FanSpeedType::RPM){
		
		io_result = bu::IoFs::ReadIntValueFrom<uint32_t>(BBQUE_TEGRA_CPU_FAN_RPM_PATH,value,1);
		if(io_result != bu::IoFs::ExitCode_t::OK){
			logger->Error("GetTemperature: Cannot get fan rpm");
			return PMResult::ERR_SENSORS_ERROR;
		}
	}
	

	return PMResult::OK;
}

/* Power */

PowerManager::PMResult
TEGRA_CPUPowerManager::GetPowerUsage(br::ResourcePathPtr_t const & rp,
				  uint32_t & mwatt)
{
	(void) rp;
	bu::IoFs::ExitCode_t io_result;

	io_result = bu::IoFs::ReadIntValueFrom<uint32_t>(BBQUE_TEGRA_CPU_POWER_PATH,mwatt,1);

	if(io_result != bu::IoFs::ExitCode_t::OK){
		logger->Error("GetPowerUsage: Cannot get CPU power");
		return PMResult::ERR_SENSORS_ERROR;
	}
	
	logger->Debug("GetPowerUsage: [CPU] power usage value=%d mW [+/-5%c]", mwatt,'%');
	
	return PMResult::OK;
}


PowerManager::PMResult
TEGRA_CPUPowerManager::GetPowerState(br::ResourcePathPtr_t const & rp,
				  uint32_t & state)
{
	(void) rp;
	bu::IoFs::ExitCode_t io_result;
	
	std::string curr_mode;
	io_result = bu::IoFs::ReadValueFrom(BBQUE_TEGRA_CPU_PWSTATE_PATH,curr_mode);

	if(io_result != bu::IoFs::ExitCode_t::OK){
		logger->Error("GetPowerState: Cannot get CPU power state");
		return PMResult::ERR_SENSORS_ERROR;
	}

	//format of the line "pmode:000x"
	std::list<std::string> curr_mode_split;
	bu::SplitString(curr_mode,curr_mode_split,":");
	
	//discard last SplitString generated element
    curr_mode_split.pop_back();
	state = (uint32_t) std::stoi(curr_mode_split.back());
	
	return PMResult::OK;
}

/* States */


#define TEGRA_CPU_PSTATE_MAX    0
#define TEGRA_CPU_PSTATE_MIN    7

PowerManager::PMResult
TEGRA_CPUPowerManager::GetPowerStatesInfo(br::ResourcePathPtr_t const & rp,
				       uint32_t & min, uint32_t & max, int & step)
{
	(void) rp;
	min  = TEGRA_CPU_PSTATE_MIN;
	max  = TEGRA_CPU_PSTATE_MAX;
	step = 1;

	return PMResult::OK;
}

PowerManager::PMResult
TEGRA_CPUPowerManager::GetPerformanceState(br::ResourcePathPtr_t const & rp,
					uint32_t & state)
{
	(void) rp;
	bu::IoFs::ExitCode_t io_result;
	
	std::string curr_mode;
	io_result = bu::IoFs::ReadValueFrom(BBQUE_TEGRA_CPU_PWSTATE_PATH,curr_mode);

	if(io_result != bu::IoFs::ExitCode_t::OK){
		logger->Error("GetPerformanceState: Cannot get CPU performance state");
		return PMResult::ERR_SENSORS_ERROR;
	}

	//format of the line "pmode:000x"
	std::list<std::string> curr_mode_split;
	bu::SplitString(curr_mode,curr_mode_split,":");
	
	//discard last SplitString generated element
    curr_mode_split.pop_back();
	state = (uint32_t) std::stoi(curr_mode_split.back());

	logger->Debug("GetPerformanceState: valid interval [%d-%d]+{32}:", TEGRA_CPU_PSTATE_MAX, TEGRA_CPU_PSTATE_MIN);
	logger->Debug("GetPerformanceState:\t *) %d for Maximum Performance", TEGRA_CPU_PSTATE_MAX);
	logger->Debug("GetPerformanceState:\t *) %d for Minimum Performance", TEGRA_CPU_PSTATE_MIN);
	logger->Debug("GetPerformanceState:\t [CPU] PerformanceState: %u ", state);
	
	return PMResult::OK;
}

PowerManager::PMResult
TEGRA_CPUPowerManager::GetPerformanceStatesCount(br::ResourcePathPtr_t const & rp,
					      uint32_t & count)
{
	(void) rp;

	count = TEGRA_CPU_PSTATE_MIN - TEGRA_CPU_PSTATE_MAX;

	return PMResult::OK;
}

int64_t TEGRA_CPUPowerManager::StartEnergyMonitor(br::ResourcePathPtr_t const & rp)
{
	(void) rp;

	logger->Warn("StartEnergyMonitor: [CPU] StartingEnergyMonitor %d",int(is_sampling));

	if (is_sampling) {
		logger->Warn("StartEnergyMonitor: [CPU] device already started");
		return -2;
	}

	is_sampling = true;

	energy_thread = std::thread(&TEGRA_CPUPowerManager::ProfileEnergyConsumption,this);

	logger->Debug("ProfileEnergyConsumption: [CPU] started");

	return 0;
}

uint64_t TEGRA_CPUPowerManager::StopEnergyMonitor(br::ResourcePathPtr_t const & rp)
{
	
	if (!is_sampling) {
		logger->Warn("StopEnergyMonitor: [CPU] energy sampling not started");
		return 0;
	}

	is_sampling = false;

	uint64_t energy_cons;
	
	logger->Debug("StopEnergyMonitor: [CPU] waiting for the profiler termination...");
	energy_thread.join();
	energy_cons = energy;
	

	// Reset energy value reading for the next sampling
	energy = 0;
	logger->Info("StopEnergyMonitor: [CPU] consumption=%llu [uJ]",energy_cons);

	return energy_cons;
}

void TEGRA_CPUPowerManager::ProfileEnergyConsumption()
{
	bu::IoFs::ExitCode_t io_result;
	logger->Debug("ProfileEnergyConsumption: [CPU] started for device");
	
	int32_t power1, power2;
	while (is_sampling){

		logger->Debug("ProfileEnergyConsumption: [CPU] sampling ");


		io_result = bu::IoFs::ReadIntValueFrom<int32_t>(BBQUE_TEGRA_CPU_POWER_PATH,power1,1);
		if(io_result != bu::IoFs::ExitCode_t::OK){
			logger->Error("ProfileEnergyConsumption: [CPU] error in power reading #1");
			return;
		}
	

		std::this_thread::sleep_for(std::chrono::milliseconds(BBQUE_TEGRA_T_MS));

		io_result = bu::IoFs::ReadIntValueFrom<int32_t>(BBQUE_TEGRA_CPU_POWER_PATH,power2,1);
		if(io_result != bu::IoFs::ExitCode_t::OK){
			logger->Error("ProfileEnergyConsumption: [CPU] error in power reading #2");
			power2 = power1;
		}

		logger->Debug("ProfileEnergyConsumption: [CPU] p1=%lu p2=%lu [mW]", power1, power2);

		// The energy additional contribution is given by the area of the trapezium
		energy += ((power1 + power2) * BBQUE_TEGRA_T_MS) / 2;

	}

	logger->Debug("ProfileEnergyConsumption: [CPU] terminated");
}

} // namespace bbque

