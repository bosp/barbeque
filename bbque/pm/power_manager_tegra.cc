/*
 * Copyright (C) 2016  Politecnico di Milano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <dlfcn.h>
#include <fstream>
#include <algorithm>


#include "bbque/pm/power_manager_tegra.h"
#include "bbque/utils/iofs.h"
#include "bbque/utils/string_utils.h"



namespace br = bbque::res;
namespace bu = bbque::utils;

namespace bbque {


TEGRAPowerManager & TEGRAPowerManager::GetInstance()
{
	static TEGRAPowerManager instance;
	return instance;
}

TEGRAPowerManager::TEGRAPowerManager()
{	
	is_sampling=false;
	energy=0;
	logger->Info("TEGRAPowerManager initialization...");
	// Retrieve information about the GPU of the system
	LoadDevicesInfo();
	
}

void TEGRAPowerManager::LoadDevicesInfo()
{
	bu::IoFs::ExitCode_t io_result;

	//available freqs separated by one space 
	std::string freqs_string;
	io_result = bu::IoFs::ReadValueFrom(BBQUE_TEGRA_GPU_AVAILABLE_FREQS_PATH,freqs_string);

	if(io_result != bu::IoFs::ExitCode_t::OK){
		logger->Error("GetAvailableFrequencies: Cannot get GPU available frequencies");
		return;
	}

	std::list<std::string> freqs_split;
	bu::SplitString(freqs_string,freqs_split," ");
	//discard last SplitString generated element
    freqs_split.pop_back();

	for( auto s : freqs_split){
		available_freqs.push_back(std::stoi(s)/1000);
	}

	initialized = true;
	logger->Info("TEGRAPowerManager initialized");
	
}

TEGRAPowerManager::~TEGRAPowerManager()
{
	is_sampling=false;
	energy_thread.join();
}


PowerManager::PMResult
TEGRAPowerManager::GetLoad(br::ResourcePathPtr_t const & rp, uint32_t & perc)
{
	(void) rp;
	bu::IoFs::ExitCode_t io_result;

	io_result = bu::IoFs::ReadIntValueFrom<uint32_t>(BBQUE_TEGRA_GPU_LOAD_PATH,perc,1);

	if(io_result != bu::IoFs::ExitCode_t::OK){
		logger->Error("GetLoad: Cannot get GPU load");
		return PMResult::ERR_SENSORS_ERROR;
	}

	logger->Debug("GetLoad: [GPU] load: %d", perc);

	return PMResult::OK;
}

PowerManager::PMResult
TEGRAPowerManager::GetTemperature(br::ResourcePathPtr_t const & rp,
				   uint32_t & celsius)
{
	(void) rp;
	bu::IoFs::ExitCode_t io_result;

	io_result = bu::IoFs::ReadIntValueFrom<uint32_t>(BBQUE_TEGRA_GPU_TEMP_PATH,celsius,1);

	if(io_result != bu::IoFs::ExitCode_t::OK){
		logger->Error("GetTemperature: Cannot get GPU temperature");
		return PMResult::ERR_SENSORS_ERROR;
	}

	celsius = celsius / 1000;

	logger->Debug("GetTemperature: [GPU] temperature: %d", celsius);

	return PMResult::OK;
	
}

/* Clock frequency */

PowerManager::PMResult
TEGRAPowerManager::GetAvailableFrequencies(br::ResourcePathPtr_t const & rp,
					    std::vector<uint32_t> & freqs)
{
	(void) rp;
	if(!initialized)
	{
		logger->Error("GetAvailableFrequencies: Cannot get available GPU frequencies");
		return PMResult::ERR_NOT_INITIALIZED;
	}

	freqs = available_freqs;

	for(auto freq:freqs){
		logger->Info("GetClockFrequencyInfo: [GPU] supported GPU frequency=%d kHz",freq/1000);
	}

	return PMResult::OK;
}

PowerManager::PMResult
TEGRAPowerManager::GetClockFrequency(br::ResourcePathPtr_t const & rp,
				      uint32_t & khz)
{
	(void) rp;
	bu::IoFs::ExitCode_t io_result;

	io_result = bu::IoFs::ReadIntValueFrom<uint32_t>(BBQUE_TEGRA_GPU_CURR_FREQ_PATH,khz,1);

	if(io_result != bu::IoFs::ExitCode_t::OK){
		logger->Error("GetClockFrequency: Cannot get GPU frequency");
		return PMResult::ERR_SENSORS_ERROR;
	}
	
	//freq is stored in Hz
	khz = khz/1000;
	logger->Debug("GetClockFrequency: GPU clock: %d kHz", khz);

	return PMResult::OK;
}

PowerManager::PMResult
TEGRAPowerManager::SetClockFrequency(br::ResourcePathPtr_t const & rp,
				      uint32_t khz)
{
	(void) rp;
	bu::IoFs::ExitCode_t io_result;

	if(!initialized)
	{
		logger->Error("SetClockFrequency: Cannot set GPU frequency");
		return PMResult::ERR_NOT_INITIALIZED;
	}

	if(std::find(available_freqs.begin(),available_freqs.end(),khz) == available_freqs.end())
	{
		logger->Error("SetClockFrequency: GPU frequency not supported");
		return PMResult::ERR_API_INVALID_VALUE;
	}

	khz = khz * 1000;

	io_result = bu::IoFs::WriteValueTo<uint32_t>(BBQUE_TEGRA_GPU_SET_MIN_FREQ_PATH,khz);
	if(io_result != bu::IoFs::ExitCode_t::OK){
			logger->Error("SetClockFrequency: Cannot set GPU frequency");
			return PMResult::ERR_SENSORS_ERROR;
	}
	io_result = bu::IoFs::WriteValueTo<uint32_t>(BBQUE_TEGRA_GPU_SET_MAX_FREQ_PATH,khz);
	if(io_result != bu::IoFs::ExitCode_t::OK){
			logger->Error("SetClockFrequency: Cannot set GPU frequency");
			return PMResult::ERR_SENSORS_ERROR;
	}
	
	logger->Debug("SetClockFrequency: [GPU] clock set at frequency=%d kHz", khz);

	return PMResult::OK;
}

PowerManager::PMResult
TEGRAPowerManager::GetClockFrequencyInfo(br::ResourcePathPtr_t const & rp,
					  uint32_t & khz_min,
					  uint32_t & khz_max,
					  uint32_t & khz_step)
{
	(void) rp;
	if(!initialized)
	{
		logger->Error("SetClockFrequency: Cannot get GPU frequency info");
		return PMResult::ERR_NOT_INITIALIZED;
	}

	khz_min = available_freqs[0];
	khz_max = available_freqs[available_freqs.size()-1];
	//TODO: step???
	khz_step = 0;

	return PMResult::OK;
}

/* Fan */

PowerManager::PMResult
TEGRAPowerManager::GetFanSpeed(br::ResourcePathPtr_t const & rp,
				FanSpeedType fs_type,	uint32_t & value)
{
	(void) rp;
	bu::IoFs::ExitCode_t io_result;

	// Fan speed type
	if (fs_type == FanSpeedType::PERCENT){

		io_result = bu::IoFs::ReadIntValueFrom<uint32_t>(BBQUE_TEGRA_GPU_FAN_PWM_PATH,value,1);
		if(io_result != bu::IoFs::ExitCode_t::OK){
			logger->Error("GetTemperature: Cannot get fan pwm");
			return PMResult::ERR_SENSORS_ERROR;
		}

		value = (value/255) * 100;

	}
	
	else if (fs_type == FanSpeedType::RPM){
		
		io_result = bu::IoFs::ReadIntValueFrom<uint32_t>(BBQUE_TEGRA_GPU_FAN_RPM_PATH,value,1);
		if(io_result != bu::IoFs::ExitCode_t::OK){
			logger->Error("GetTemperature: Cannot get fan rpm");
			return PMResult::ERR_SENSORS_ERROR;
		}
	}
	

	return PMResult::OK;
}

/* Power */

PowerManager::PMResult
TEGRAPowerManager::GetPowerUsage(br::ResourcePathPtr_t const & rp,
				  uint32_t & mwatt)
{
	(void) rp;
	bu::IoFs::ExitCode_t io_result;

	io_result = bu::IoFs::ReadIntValueFrom<uint32_t>(BBQUE_TEGRA_GPU_POWER_PATH,mwatt,1);

	if(io_result != bu::IoFs::ExitCode_t::OK){
		logger->Error("GetPowerUsage: Cannot get GPU power");
		return PMResult::ERR_SENSORS_ERROR;
	}
	
	logger->Debug("GetPowerUsage: [CPU] power usage value=%d mW [+/-5%c]", mwatt,'%');
	
	return PMResult::OK;
}


PowerManager::PMResult
TEGRAPowerManager::GetPowerState(br::ResourcePathPtr_t const & rp,
				  uint32_t & state)
{
	(void) rp;
	bu::IoFs::ExitCode_t io_result;
	
	std::string curr_mode;
	io_result = bu::IoFs::ReadValueFrom(BBQUE_TEGRA_GPU_PWSTATE_PATH,curr_mode);

	if(io_result != bu::IoFs::ExitCode_t::OK){
		logger->Error("GetPowerState: Cannot get GPU power state");
		return PMResult::ERR_SENSORS_ERROR;
	}

	//format of the line "pmode:000x"
	std::list<std::string> curr_mode_split;
	bu::SplitString(curr_mode,curr_mode_split,":");
	
	//discard last SplitString generated element
    curr_mode_split.pop_back();
	state = (uint32_t) std::stoi(curr_mode_split.back());
	
	return PMResult::OK;
}

/* States */


#define TEGRA_GPU_PSTATE_MAX    0
#define TEGRA_GPU_PSTATE_MIN    7

PowerManager::PMResult
TEGRAPowerManager::GetPowerStatesInfo(br::ResourcePathPtr_t const & rp,
				       uint32_t & min, uint32_t & max, int & step)
{
	(void) rp;
	min  = TEGRA_GPU_PSTATE_MIN;
	max  = TEGRA_GPU_PSTATE_MAX;
	step = 1;

	return PMResult::OK;
}

PowerManager::PMResult
TEGRAPowerManager::GetPerformanceState(br::ResourcePathPtr_t const & rp,
					uint32_t & state)
{
	(void) rp;
	bu::IoFs::ExitCode_t io_result;
	
	std::string curr_mode;
	io_result = bu::IoFs::ReadValueFrom(BBQUE_TEGRA_GPU_PWSTATE_PATH,curr_mode);

	if(io_result != bu::IoFs::ExitCode_t::OK){
		logger->Error("GetPerformanceState: Cannot get GPU performance state");
		return PMResult::ERR_SENSORS_ERROR;
	}

	//format of the line "pmode:000x"
	std::list<std::string> curr_mode_split;
	bu::SplitString(curr_mode,curr_mode_split,":");
	
	//discard last SplitString generated element
    curr_mode_split.pop_back();
	state = (uint32_t) std::stoi(curr_mode_split.back());

	logger->Debug("GetPerformanceState: valid interval [%d-%d]+{32}:", TEGRA_GPU_PSTATE_MAX, TEGRA_GPU_PSTATE_MIN);
	logger->Debug("GetPerformanceState:\t *) %d for Maximum Performance", TEGRA_GPU_PSTATE_MAX);
	logger->Debug("GetPerformanceState:\t *) %d for Minimum Performance", TEGRA_GPU_PSTATE_MIN);
	logger->Debug("GetPerformanceState:\t [CPU] PerformanceState: %u ", state);
	
	return PMResult::OK;
}

PowerManager::PMResult
TEGRAPowerManager::GetPerformanceStatesCount(br::ResourcePathPtr_t const & rp,
					      uint32_t & count)
{
	(void) rp;

	count = TEGRA_GPU_PSTATE_MIN - TEGRA_GPU_PSTATE_MAX;

	return PMResult::OK;
}

int64_t TEGRAPowerManager::StartEnergyMonitor(br::ResourcePathPtr_t const & rp)
{
	(void) rp;

	logger->Warn("StartEnergyMonitor: [GPU] StartingEnergyMonitor %d",int(is_sampling));

	if (is_sampling) {
		logger->Warn("StartEnergyMonitor: [GPU] device already started");
		return -2;
	}
	is_sampling = true;

	energy_thread = std::thread(&TEGRAPowerManager::ProfileEnergyConsumption,this);

	logger->Debug("ProfileEnergyConsumption: [GPU] started");

	return 0;
}

uint64_t TEGRAPowerManager::StopEnergyMonitor(br::ResourcePathPtr_t const & rp)
{
	(void) rp;
	if (!is_sampling) {
		logger->Warn("StopEnergyMonitor: [GPU] energy sampling not started");
		return 0;
	}
	is_sampling = false;

	uint64_t energy_cons;
	
	logger->Debug("StopEnergyMonitor:  [GPU] waiting for the profiler termination...");
	energy_thread.join();
	energy_cons = energy;
	

	// Reset energy value reading for the next sampling
	energy = 0;
	logger->Info("StopEnergyMonitor: [GPU] consumption=%llu [uJ]",energy_cons);

	return energy_cons;
}

void TEGRAPowerManager::ProfileEnergyConsumption()
{
	bu::IoFs::ExitCode_t io_result;
	logger->Debug("ProfileEnergyConsumption: [GPU] started for device");
	
	int32_t power1, power2;
	while (is_sampling){

		logger->Debug("ProfileEnergyConsumption: [GPU] sampling ");


		io_result = bu::IoFs::ReadIntValueFrom<int32_t>(BBQUE_TEGRA_GPU_POWER_PATH,power1,1);
		if(io_result != bu::IoFs::ExitCode_t::OK){
			logger->Error("ProfileEnergyConsumption: [GPU] error in power reading #1");
			return;
		}
	

		std::this_thread::sleep_for(std::chrono::milliseconds(BBQUE_TEGRA_T_MS));

		io_result = bu::IoFs::ReadIntValueFrom<int32_t>(BBQUE_TEGRA_GPU_POWER_PATH,power2,1);
		if(io_result != bu::IoFs::ExitCode_t::OK){
			logger->Error("ProfileEnergyConsumption: [GPU] error in power reading #2");
			power2 = power1;
		}

		logger->Debug("ProfileEnergyConsumption: [GPU] p1=%lu p2=%lu [mW]", power1, power2);

		// The energy additional contribution is given by the area of the trapezium
		energy += ((power1 + power2) * BBQUE_TEGRA_T_MS) / 2;

	}

	logger->Debug("ProfileEnergyConsumption: [GPU] terminated");
}

} // namespace bbque

