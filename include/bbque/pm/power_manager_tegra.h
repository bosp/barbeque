/*
 * Copyright (C) 2016  Politecnico di Milano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BBQUE_POWER_MANAGER_TEGRA_H_
#define BBQUE_POWER_MANAGER_TEGRA_H_


#include <thread>

#include "bbque/pm/power_manager.h"
#include "bbque/res/resource_type.h"
#include "bbque/res/resource_path.h"


#define BBQUE_TEGRA_T_MS  1000

#define BBQUE_TEGRA_GPU_LOAD_PATH				"/sys/devices/gpu.0/load"
#define BBQUE_TEGRA_GPU_CURR_FREQ_PATH			"/sys/devices/gpu.0/devfreq/17000000.gv11b/cur_freq"
#define BBQUE_TEGRA_GPU_AVAILABLE_FREQS_PATH	"/sys/devices/gpu.0/devfreq/17000000.gv11b/available_frequencies"
#define BBQUE_TEGRA_GPU_SET_MIN_FREQ_PATH		"/sys/devices/gpu.0/devfreq/17000000.gv11b/min_freq"
#define BBQUE_TEGRA_GPU_SET_MAX_FREQ_PATH		"/sys/devices/gpu.0/devfreq/17000000.gv11b/max_freq"


#define BBQUE_TEGRA_GPU_POWER_PATH			"/sys/bus/i2c/devices/1-0040/iio_device/in_power0_input"
#define BBQUE_TEGRA_GPU_TEMP_PATH			"/sys/devices/virtual/thermal/thermal_zone1/temp"
#define BBQUE_TEGRA_GPU_PWSTATE_PATH		"/var/lib/nvpmodel/status"

#define BBQUE_TEGRA_GPU_FAN_PWM_PATH		"/sys/devices/pwm-fan/cur_pwm"
#define BBQUE_TEGRA_GPU_FAN_RPM_PATH		"/sys/devices/pwm-fan/rpm_measured"



namespace br = bbque::res;

namespace bbque {

/**
 * @class TEGRAPowerManager
 *
 * @brief Provide power management related API for TEGRA GPU devices,
 * by extending @ref PowerManager class.
 */
class TEGRAPowerManager : public PowerManager
{
public:

	TEGRAPowerManager();

	~TEGRAPowerManager();

	static TEGRAPowerManager & GetInstance();


	PMResult GetLoad(br::ResourcePathPtr_t const & rp, uint32_t & perc);


	PMResult GetTemperature(br::ResourcePathPtr_t const & rp, uint32_t & celsius);


	PMResult GetClockFrequency(br::ResourcePathPtr_t const & rp, uint32_t & khz);


	PMResult SetClockFrequency(br::ResourcePathPtr_t const & rp, uint32_t khz);


	PMResult GetClockFrequencyInfo(br::ResourcePathPtr_t const & rp,
				uint32_t & khz_min, uint32_t & khz_max,
				uint32_t & khz_step);


	PMResult GetAvailableFrequencies(br::ResourcePathPtr_t const & rp,
					std::vector<uint32_t> & freqs);


	PMResult GetFanSpeed(br::ResourcePathPtr_t const & rp, FanSpeedType fs_type,
			uint32_t & value);


	PMResult GetPowerUsage(br::ResourcePathPtr_t const & rp, uint32_t & mwatt);


	PMResult GetPowerState(br::ResourcePathPtr_t const & rp, uint32_t & state);


	PMResult GetPowerStatesInfo(br::ResourcePathPtr_t const & rp, uint32_t & min,
				uint32_t & max, int & step);


	PMResult GetPerformanceState(br::ResourcePathPtr_t const & rp,
				uint32_t & state);

	PMResult GetPerformanceStatesCount(br::ResourcePathPtr_t const & rp,
					uint32_t & count);

	int64_t StartEnergyMonitor(br::ResourcePathPtr_t const & rp);

	uint64_t StopEnergyMonitor(br::ResourcePathPtr_t const & rp);

private:

	bool initialized = false;

	std::vector<uint32_t> available_freqs;
	
	uint64_t energy;

	/*** device energy monitor thread */
	std::thread energy_thread;

	/*** device energy monitor thread status */
	std::atomic<bool>  is_sampling;

	/**
	 * @brief Load devices information
	 */
	void LoadDevicesInfo();

	
	/**
	 * @brief Sample the power consumption values in time intervals of
	 * length BBQUE_TEGRA_T_MS (milliseconds).
	 * It terminates once the @see is_sampling is set to false.
	 *
	 */
	void ProfileEnergyConsumption();


};

}

#endif // BBQUE_POWER_MANAGER_TEGRA_H_

