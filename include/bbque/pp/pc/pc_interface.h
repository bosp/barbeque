/*
 * Copyright (C) 2020  Politecnico di Milano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
 
#ifndef BBQUE_PC_INTERFACE_H_
#define BBQUE_PC_INTERFACE_H_

#include <cassert>
#include <map>
#include <memory>
#include <mutex>

#include "performance_counter.h"
#include "bbque/utils/worker.h"

namespace bbque {

namespace pp {

namespace br = bbque::res;
namespace bu = bbque::utils;


class PCInterface {

public:

    /** Returns the PerformanceCounter associated to the resource and with the type specified. The
     *  PerformanceCounter is put in the list of monitored counters (if not already present and
     *  regularly updated by the PlatformProxy.
     *
     * @note This function is thread-safe
     * @note The function has non-constant complexity, consider to cache the result if possible.
     */
    std::shared_ptr<PerformanceCounter> GetPC(br::ResourcePtr_t resource, PCType_t pc_type) noexcept {
        assert(resource);
        const auto path = resource->Path()->ToString();

        std::lock_guard<std::mutex> lk(this->mx_list_pcs);

        if (list_pcs.find(path) != list_pcs.end()) {
            auto pc_in_res = list_pcs[path].find(pc_type);
            if (pc_in_res != list_pcs[path].end()) {
                return pc_in_res->second;
            }
        }

        // Not found create a new one
        const auto human_name = path + "[" + convert_pc_type_to_str(pc_type) + "]";
        return list_pcs[path][pc_type] = std::make_shared<PerformanceCounter>(pc_type, human_name);
    }

    virtual void UpdatePCs() noexcept {
        std::lock_guard<std::mutex> lk(this->mx_list_pcs);
        for (auto &pcs_in_res : list_pcs) {
            std::string res_path = pcs_in_res.first;
            for (auto &pc : pcs_in_res.second) {
                UpdatePC(res_path, pc.second);
            }
        }
    }
    
protected:
    std::mutex mx_list_pcs;
    std::map<std::string, std::map<PCType_t, std::shared_ptr<PerformanceCounter>>> list_pcs;
   
    virtual void UpdatePC(const std::string &resource_path, std::shared_ptr<PerformanceCounter> pc) noexcept = 0;
};


}

}


#endif

