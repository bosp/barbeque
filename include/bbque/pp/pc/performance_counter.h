/*
 * Copyright (C) 2020  Politecnico di Milano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
 
#ifndef BBQUE_PERFORMANCE_COUNTER_H_
#define BBQUE_PERFORMANCE_COUNTER_H_

#ifndef CONFIG_BBQUE_PERFC_ENABLED
    #error "CONFIG_BBQUE_PERFC_ENABLED must be enabled to use performance counters"
#endif

#include <chrono>
#include <shared_mutex>
#include <string>

namespace bbque {

typedef enum class PCType_t {
    CACHE_L1I_MISS,
    CACHE_L1I_HIT,
    CACHE_L2_MISS,
    CACHE_L2_RQSTS,
    CACHE_L3_MISS,
    CACHE_L3_RQSTS,
    
    UOPS_DISPATCH_P0,
    UOPS_DISPATCH_P1,
    UOPS_DISPATCH_P2,
    UOPS_DISPATCH_P3,
    UOPS_DISPATCH_P4,
    UOPS_DISPATCH_P5,
    UOPS_DISPATCH_P6,
    UOPS_DISPATCH_P7,

    UOPS_RETIRED,
    UOPS_ISSUED_ANY,
    
    CYCLES,
    
    CAS_READ,
    CAS_WRITE
} PCType_t;

inline std::string convert_pc_type_to_str(PCType_t pc_type) {
    switch(pc_type) {
        case PCType_t::CACHE_L1I_MISS: return "CACHE L1I MISS";
        case PCType_t::CACHE_L1I_HIT:  return "CACHE L1I HIT";
        case PCType_t::CACHE_L2_MISS:  return "CACHE L2 MISS";
        case PCType_t::CACHE_L2_RQSTS: return "CACHE L2 REQUESTS";
        case PCType_t::CACHE_L3_MISS:  return "CACHE L3 MISS";
        case PCType_t::CACHE_L3_RQSTS: return "CACHE L3 REQUESTS";

        case PCType_t::UOPS_DISPATCH_P0: return "UOPS DISPATCHED PORT 0";
        case PCType_t::UOPS_DISPATCH_P1: return "UOPS DISPATCHED PORT 1";
        case PCType_t::UOPS_DISPATCH_P2: return "UOPS DISPATCHED PORT 2";
        case PCType_t::UOPS_DISPATCH_P3: return "UOPS DISPATCHED PORT 3";
        case PCType_t::UOPS_DISPATCH_P4: return "UOPS DISPATCHED PORT 4";
        case PCType_t::UOPS_DISPATCH_P5: return "UOPS DISPATCHED PORT 5";
        case PCType_t::UOPS_DISPATCH_P6: return "UOPS DISPATCHED PORT 6";
        case PCType_t::UOPS_DISPATCH_P7: return "UOPS DISPATCHED PORT 7";

        case PCType_t::UOPS_RETIRED:     return "UOPS RETIRED";
        case PCType_t::UOPS_ISSUED_ANY:  return "UOPS ISSUED ANY";

        case PCType_t::CYCLES:           return "CPU CYCLES";

        case PCType_t::CAS_READ:         return "CAS READ";
        case PCType_t::CAS_WRITE:        return "CAS WRITE";

        default: return "UNKNOWN";
    }
}

typedef long long PCValue_t;

#if __cplusplus >= 201703L
    typedef std::shared_mutex rw_mutex_t;
#else
    // C++14 has only timed mutex
    typedef std::shared_timed_mutex rw_mutex_t;
#endif

class PerformanceCounter {

public:

    /**
	 * Constructor
	 * @param type What the performance counter represents. This argument is immutable.
	 * @param human_name A string with a human-readable string for the PC. This argument is immutable.
	 */
    PerformanceCounter(PCType_t type, const std::string &human_name) noexcept 
                      : type(type), human_name(human_name) {
    
    }

    /**
	 * Returns the (immutable) type of the performance counter.
	 * @seealso PCType_t
	 */
    PCType_t Type() const noexcept {
        return this->type;
    }

    /**
	 * Returns the (immutable) human-readable name of the performance counter.
	 */
    std::string HumanName() const noexcept {
        return this->human_name;
    }

    /**
	 * Returns the current value for the performance counter.
	 * @note The function is thread-safe
	 */
    PCValue_t Get() const noexcept {
        std::shared_lock<rw_mutex_t> lk(mx_sync);
        return this->value;
    }
    
    /**
	 * Returns the last time point when the PC has been updated with the Set() function.
	 * @note The function is thread-safe
	 */
    std::chrono::time_point<std::chrono::steady_clock> GetLastUpdate() const noexcept {
        std::shared_lock<rw_mutex_t> lk(mx_sync);
        return this->last_update;
    }
    
    /**
	 * Set a new value for the performance counter. The last updated time is automatically updated.
	 * @note The function is thread-safe
	 */
    void Set(PCValue_t new_value) noexcept {
        std::unique_lock<rw_mutex_t> lk(mx_sync);
        this->value = new_value;
        this->last_update = std::chrono::steady_clock::now();
    }

private:

    /**
	 * Enumeration with the type of performance counter refers to.
	 */
    const PCType_t type;

    /**
	 * Human-readable string for this performance counter.
	 */
    const std::string human_name;

    /**
	 * The current value of performance counter.
	 */
    PCValue_t value;

    /**
	 * The last time this performance counter value has been updated.
	 */
    std::chrono::time_point<std::chrono::steady_clock> last_update;

    /**
	 * A mutex protecting both value and last_update
	 */
    mutable rw_mutex_t mx_sync;

};

}


#endif

