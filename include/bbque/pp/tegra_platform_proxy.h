#ifndef BBQUE_LINUX_TEGRAPROXY_H_
#define BBQUE_LINUX_TEGRAPROXY_H_

#include <map>
#include <memory>
#include <string>
#include <vector>



#include "bbque/config.h"
#include "bbque/platform_proxy.h"
#include "bbque/app/application.h"
#include "bbque/pm/power_manager.h"
#include "bbque/pm/power_manager_tegra.h"

#define BBQUE_PP_TEGRA_PLATFORM_ID ".tegra"
#define BBQUE_PP_TEGRA_HARDWARE_ID "cuda"

#define BBQUE_TEGRA_GPU_PATH "sys.gpu.pe"

namespace ba = bbque::app;
namespace br = bbque::res;
namespace bu = bbque::utils;

namespace bbque {
namespace pp {

class TEGRAPlatformProxy : public PlatformProxy
{
public:

	static TEGRAPlatformProxy * GetInstance();

	/**
	 * @brief Destructor
	 */
	virtual ~TEGRAPlatformProxy();

	/**
	 * @brief Platform specific resource setup interface.
	 */
	ExitCode_t Setup(SchedPtr_t papp) override;

	/**
	 * @brief Load NVML device data function
	 */
	ExitCode_t LoadPlatformData() override final;

	/**
	 * @brief OpenCL specific resources refresh function
	 */
	ExitCode_t Refresh() override;
	/**
	 * @brief OpenCL specific resources release function
	 */
	ExitCode_t Release(SchedPtr_t papp) override;

	/**
	 * @brief OpenCL specific resource claiming function
	 */
	ExitCode_t ReclaimResources(SchedPtr_t papp) override;

	/**
	 * @brief TEGRA resource assignment mapping
	 */
	ExitCode_t MapResources(SchedPtr_t papp,
				ResourceAssignmentMapPtr_t pres,
				bool excl = true);

	/**
	 * nvml specific termination
	 */
	void Exit() override;

protected:
/*** Configuration manager instance */
	ConfigurationManager & cm;

	/*** Command manager instance */
	CommandManager & cmm;
#ifdef CONFIG_BBQUE_PM

	/*** Power Manager instance */
	PowerManager & pm;

	void PrintPowerInfo(br::ResourcePathPtr_t r_path_ptr) const;

	void PrintDevicesPowerInfo() const;

#endif // CONFIG_BBQUE_PM
	/*** The logger used by the module */
	std::unique_ptr<bu::Logger> logger;

	/*** Constructor */
	TEGRAPlatformProxy();

	ExitCode_t RegisterDevices();

	uint16_t local_sys_id;
};

} // namespace pp
} // namespace bbque

#endif
