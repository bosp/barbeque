/*
 * Copyright (C) 2019  Politecnico di Milano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BBQUE_PROCESS_H_
#define BBQUE_PROCESS_H_
#include <fstream>
#include <thread>
#include <sys/stat.h>
#include <cstring>
#include "bbque/app/schedulable.h"
#include "bbque/utils/logging/logger.h"
#include "bbque/resource_accounter.h"

namespace bbque {

class ProcessManager;

namespace app {

    
struct RuntimeProfilingProc_t
{
	/** CPU usage profiling carried
	 * out by the scheduling policy */
	struct
	{
		int curr = 0;
		int prev = 0;
	} cpu_usage;

};
/**
 * @class Process
 * @brief This class is defined to instantiate descriptors of generic
 * processors (i.e. not AEM-integrated applications)
 */
class Process : public Schedulable
{
	friend class bbque::ProcessManager;

public:

	/**
	 * @struct SchedRequest_t
	 * @brief The set of resources required to schedule the process
	 */
	class ScheduleRequest
	{
	public:

		ScheduleRequest() :
		    cpu_cores(0), acc_cores(0), memory_mb(0) { }
		uint32_t cpu_cores;
		uint32_t acc_cores;
		uint32_t gpu_units;
		uint32_t memory_mb;
	};

	using ScheduleRequestPtr_t = std::shared_ptr<ScheduleRequest>;

	/**
	 * @brief Constructor
	 * @param _pid the process id
	 * @param _prio the priority (optional)
	 */
	Process(std::string const & _name,
		AppPid_t _pid,
		AppPrio_t _prio = 0,
		Schedulable::State_t _state = READY,
		Schedulable::SyncState_t _sync = SYNC_NONE);

	/**
	 * @brief Destructor
	 */
	virtual ~Process() { 
        cpu_monitor_thread.join();
    }

	/**
	 * @brief Set the scheduling request of resources
	 * @param sched_req a ScheduleRequest_t object
	 */
	void SetScheduleRequestInfo(ScheduleRequestPtr_t sched_req)
	{
		this->sched_req = sched_req;
	}

	/**
	 * @brief Get the scheduling request of resources
	 * @param sched_req a ScheduleRequest_t object
	 */
	ScheduleRequestPtr_t const & GetScheduleRequestInfo()
	{
		return this->sched_req;
	}
	
    	// ---------------------- Profiling management ---------------------- //

	/**
	 * @brief Get runtime profiling information for this application
	 */
	RuntimeProfilingProc_t GetRuntimeProfile()
	{
		std::unique_lock<std::mutex> rtp_lock(rt_prof_mtx);
		return rt_prof;
	}

	/**
	 * @brief Save the profiling data collected at runtime 
	 */
	void UpdateRuntimeProfile(int cpu_usage)
	{
		std::unique_lock<std::mutex> rtp_lock(rt_prof_mtx);
		rt_prof.cpu_usage.prev = rt_prof.cpu_usage.curr;
		rt_prof.cpu_usage.curr = cpu_usage;
    }

private:
    
    void MonitorCPUUsage();
    uint32_t GetTotalCPUTime(int nr_proc_elements);
    uint32_t GetProcessCPUTime();

	std::unique_ptr<bbque::utils::Logger> logger;
    RuntimeProfilingProc_t rt_prof;
    std::thread cpu_monitor_thread;

	std::mutex rt_prof_mtx;

	/** Request of resources for scheduling */
	ScheduleRequestPtr_t sched_req;

};


} // namespace app

} // namespace bbque

#endif // BBQUE_PROCESS_H_

