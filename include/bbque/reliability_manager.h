/*
 * Copyright (C) 2019  Politecnico di Milano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BBQUE_RELIABILITY_MANAGER_H_
#define BBQUE_RELIABILITY_MANAGER_H_

#include <sys/types.h>
#include <sys/stat.h>
#include <ftw.h>
#include <fstream>
#include <map>
#include <unistd.h>

#include "bbque/config.h"
#include "bbque/application_manager.h"
#include "bbque/command_manager.h"
#include "bbque/process_manager.h"
#include "bbque/platform_manager.h"
#include "bbque/resource_accounter.h"
#include "bbque/res/resources.h"
#include "bbque/utils/logging/logger.h"
#include "bbque/utils/timer.h"
#include "bbque/utils/worker.h"

#ifdef CONFIG_BBQUE_LIBHWREL
	#include "libhwrel.h"
#endif


/** Do not accept shorter checkpoint period length */
#define BBQUE_MIN_CHECKPOINT_PERIOD_MS   10000
#define DEFAULT_EXPECTED_CHECKPOINT_TIME 1000 //ms

using namespace bbque::utils;

namespace bbque {

/**
 * @class ReliabilityManager
 * @brief The class is responsible of monitoring the health status
 * of the hardware resources and managing the checkpoint/restore
 * of the applications.
 */


class ReliabilityManager : utils::Worker, CommandHandler
{
public:
	enum class ExitCode_t
	{
		OK = 0, /// Successful call
		ERR_RSRC_MISSING, /// Not valid resource specified
		ERR_UNKNOWN /// A not specified error code
	};

	static ReliabilityManager & GetInstance();

	virtual ~ReliabilityManager();

	/**
	 * @brief Notify (to the manager) that an application
	 * has required a checkpoint
	 */
	void NotifyCheckpointRequest();
	void StoreNrIdleProcessingElements(int64_t nr_idle_pe)
	{
		nr_idle_proc_elements = nr_idle_pe;
	}

	int64_t GetNrIdleProcessingElements()
	{
		return nr_idle_proc_elements;
	}

	/**
	 * @brief Register the resources to monitor for collecting run-time
	 * reliability information
	 *
	 * @param rp Resource path of the resource(s)
	 * @return ERR_RSRC_MISSING if the resource path does not
	 * reference any resource, OK otherwise
	 */
	ExitCode_t Register(br::ResourcePathPtr_t rp);

	ExitCode_t Register(const std::string & rp_str);

protected:

	/**
	 * @brief Monitor the health status of the HW resources
	 */
	void Task();

private:

	/**
	 * @struct ResourceHandler
	 */
	struct ResourceHandler
	{
		br::ResourcePathPtr_t path;
		br::ResourcePtr_t resource_ptr;
	};

	CommandManager & cm;

	ResourceAccounter & ra;

	ApplicationManager & am;

#ifdef CONFIG_BBQUE_LINUX_PROC_MANAGER

	ProcessManager & prm;
#endif

	PlatformManager & plm;

	std::unique_ptr<utils::Logger> logger;

	std::string checkpoint_appinfo_dir = BBQUE_CHECKPOINT_APPINFO_PATH "/";

	std::vector<ResourceHandler> monitored_resources;

#ifdef CONFIG_BBQUE_PERIODIC_CHECKPOINT

	/**
	 * @brief Perform periodical checkpoints of the managed applications
	 * and processes
	 */
	void PeriodicCheckpointTask();
    
#elif defined CONFIG_BBQUE_DYNAMIC_CHECKPOINT
    
	/**
	 * @brief Dynamically schedules checkpoints of the managed applications
	 * and processes according to a specified overhead constraint.
	 */
    
	void DynamicCheckpointTask();
#endif

#ifdef CONFIG_BBQUE_CHECKPOINT_SCHEDULER
	Timer chk_timer;

	std::thread chk_thread;

	unsigned int chk_period_len = BBQUE_CHECKPOINT_PERIOD_LENGTH; // ms
#endif

	/**
	 * @brief Constructor
	 */
	ReliabilityManager();

	/**
	 * @brief Notify the detection of a HW resource fault (or a
	 * "high" probability of that) to applications that may be
	 * affected.
	 */
	void NotifyFaultDetection(res::ResourcePtr_t rsrc);

	/**
	 * @brief Trigger the restore of a previously check-pointed
	 * application
	 */
	void Restore();

	/**
	 * @see CommandHandler
	 */
	int CommandsCb(int argc, char * argv[]);

	/**
	 * @brief Handler for resource performance degradation notification
	 *
	 * @param argc The command line arguments
	 * @param argv The command line arguments vector: {resource, value} pairs
	 *
	 * @return 0 if success, a positive integer value otherwise
	 */
	int ResourceDegradationHandler(int argc, char * argv[]);

	/**
	 * @brief Simualate the occurrence of a fault on a resource
	 * @param resource_path (string) resource path
	 */
	void SimulateFault(std::string const & resource_path);

	/**
	 * @brief Force the freezing of a given process/application
	 * @param pid Process id
	 */
	void Freeze(app::AppPid_t pid);

	/**
	 * @brief Thaw a previously frozen application or process
	 * @param pid Process id
	 */
	void Thaw(app::AppPid_t pid);

	/**
	 * @brief Perform the checkpoint of an application or process
	 * @param pid Process id
	 */
	void Dump(app::AppPid_t pid);

	/**
	 * @brief Perform the checkpoint of an application or process
	 * @param psched pointer to Schedulable object
	 */
	void Dump(app::SchedPtr_t psched);

	/**
	 * @brief Restore a checkpointed application or process
	 * @param pid Process id
	 * @param exe_name Executable name
	 */
	void Restore(app::AppPid_t pid, std::string exe_name);

#ifdef CONFIG_BBQUE_DYNAMIC_CHECKPOINT

	double ComputeCheckpointOverhead(app::SchedPtr_t psched);
#endif


#ifdef CONFIG_BBQUE_LIBHWREL
	/**
	 * @brief Update the reliability information of any registered resource
	 * @note This function is thread safe
	 * @note This function is periodically called by Task()
	 */
	void UpdateReliabilityInfo();
	
	std::unique_ptr<libhwrel::HWReliabilityMonitor> hwrel;

	/** Libhwrel state for the current resource */
	std::map<br::ResourcePath, libhwrel::reliability_state_t> hwrel_state;

#endif


	int64_t nr_idle_proc_elements;

};

}

#endif // BBQUE_RELIABILITY_MANAGER_H_
