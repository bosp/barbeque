/*
 * Copyright (C) 2021  Politecnico di Milano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BBQUE_ENERGY_PROFILER_H
#define BBQUE_ENERGY_PROFILER_H

#include <cstdint>
#include <list>
#include <map>

#include "bbque/plugins/scheduler_policy.h"

namespace bbque {


using SchedEntity = plugins::SchedulerPolicyIF::SchedEntity_t;

class EnergyProfiler
{
public:

	using EpcContainerIterator = std::list<SchedEntity>::iterator;

	static EnergyProfiler & GetInstance()
	{
		static EnergyProfiler instance;
		return instance;
	}

	virtual ~EnergyProfiler()
	{
		this->epc_values.clear();
		this->sched_entities.clear();
	}

	EnergyProfiler & operator=(EnergyProfiler &) = delete;

	/**
	 * @brief Given a <Application, WorkingMode> pair put the profiled
	 * Energy-per-Cycle value in the sorted data structure
	 *
	 * @param papp Application pointer
	 * @param pawm WorkingMode pointer
	 */
	void PushUpdate(app::AppPtr_t papp, app::AwmPtr_t pawm)
	{
		SchedEntity new_se(papp, pawm);
		auto sched_it = sched_entities.find(new_se);
		if (sched_it == sched_entities.end()) {
			epc_values.push_front(new_se);
			sched_entities[new_se] = epc_values.begin();
		}
		epc_values.sort(SchedEntity::CompareEpcMean);
	}

	/**
	 * @brief Remove all the scheduling entities related to a given application
	 *
	 * @param papp Application pointer
	 */
	void RemoveSchedEntities(app::AppPtr_t papp)
	{
		for (auto it = epc_values.begin(); it != epc_values.end(); ++it) {
			auto it_to_del = it;
			if (it->papp == papp) {
				it++;
				sched_entities.erase(*it_to_del);
				epc_values.erase(it_to_del);
			}
		}
	}

	/**
	 * @brief Get the list of SchedEntity sorted by EpC value
	 *
	 * @return A list container of SchedEntity objects
	 */
	std::list<SchedEntity> & GetSchedEntityList()
	{
		return this->epc_values;
	}

private:

	EnergyProfiler() { }

	std::map<SchedEntity, EpcContainerIterator> sched_entities;

	std::list<SchedEntity> epc_values;

};

} // namespace bbque

#endif /* BBQUE_ENERGY_PROFILER_H */

